z≈ﬁˇ‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰‰```
TODOs: In priority/chronological order

Introduction [Anna + Ankita] Status: Ready for review
Write first draft
Ankita, Clare, and others to provide input
Nat - tying into OpenTEAM context
Landscape Review [Ankita] Status: Finish
Import content 
Import content from the design lit review section
Collabathon Convener Reflections Status: TBD
TBD
Perspectives from Producers & TAPS [Eliza] Status: Ready for review (Anna/Clare/others)
Jump to section 
Lead: Eliza
Contributing: Eliza, Katy, Clare, Anna
Reviewier: Maddie, Anna
Perspectives from Ag Technologists [Tibet]
Lead: Tibet
Contributing: Steve, Tibet
Reviewer: Vic, Ben
UX Design Patterns [Hana] Status: Ready for review
Identify writing “lead”
Outline section in detail
Reviewer: Ben, Anna
ADW Design Recommendations [Clare]
Identify writing “lead”
Outline section in detail
Review: Tibet, Steve, Clare, Ben (if needed)
General/Summary Guidance & Recommendations for Ag Tech (Conclusions)
jump to section 
Reviewer: Nat, Ankita, Anna, Kara
Next Steps
Contributing: Steve, Ankita, Nat
```

**Document Outline**


[0. META	4](#0meta4)
[1. Introduction	8](#1introduction8)
[2. Trust & Technology in Agriculture: Research Literature	11](#2trusttechnologyinagricultureresearchliterature11)
[2.1 What is Trust?	12](#21whatistrust12)
[2.2 Dimensions of Trust in Technology	14](#22dimensionsoftrustintechnology14)
[2.3. The State of Trust in Ag Tech	18](#23thestateoftrustinagtech18)
[2.4. Lessons from UX/UI Design Practice	22](#24lessonsfromuxuidesignpractice22)
[3. Collabathon Convener Reflections	27](#3collabathonconvenerreflections27)
[3.1 Exploring our own Data Stores to Conceptualize Trust	27](#31exploringourowndatastorestoconceptualizetrust27)
[3.3 Asynchronous Convener Reflections re: Antecedents for Trust	29](#33asynchronousconvenerreflectionsreantecedentsfortrust29)
[4. Perspectives from Producers & Technical Assistant Providers	31](#4perspectivesfromproducerstechnicalassistantproviders31)
[4.1 Goals	31](#41goals31)
[4.2 Interview Process	32](#42interviewprocess32)
[4.3 Who We Talked To	32](#43whowetalkedto32)
[4.4 Biases and Limitations	33](#44biasesandlimitations33)
[4.5 Producers: Learnings	34](#45producerslearnings34)
[4.6 Technical Assistance Providers (TAPs): Learnings	37](#46technicalassistanceproviderstapslearnings37)
[4.7 Challenges Related to Technical Tools	41](#47challengesrelatedtotechnicaltools41)
[Section 4 Appendices	43](#section4appendices43)
[5. Perspectives from Ag Technologists	47](#5perspectivesfromagtechnologists47)
[5.1 Surveying the landscape	47](#51surveyingthelandscape47)
[5.2 Interview process	50](#52interviewprocess50)
[5.3 Learnings	52](#53learnings52)
[5.4 Summary and the implications for an ADW	58](#54summaryandtheimplicationsforanadw58)
[5.5 Unanswered Questions	59](#55unansweredquestions59)
[5.6 Challenges for a standalone Ag Data Wallet	59](#56challengesforastandaloneagdatawallet59)
[6. UX Design Patterns	60](#6uxdesignpatterns60)
[6.1. OVERVIEW	60](#61overview60)
[6.2. UX PATTERNS:	62](#62uxpatterns62)
[7. ADW Design Recommendations	81](#7adwdesignrecommendations81)
[Suggested User Interface Elements	81](#suggesteduserinterfaceelements81)
[System Architecture Considerations	89](#systemarchitectureconsiderations89)
[8. Summary Guidance & Next Steps	92](#8summaryguidancenextsteps92)
[8.1 Principles	92](#81principles92)
[8.2 Next steps	96](#82nextsteps96)
[– Deprecated - 8. PRE-Ankita: Summary Guidance & Recommendations for Ag Tech (Conclusions)	99](#deprecated8preankitasummaryguidancerecommendationsforagtechconclusions99)
[Data	99](#data99)
[Trust	100](#trust100)
[UI/UX Design	101](#uiuxdesign101)
[Open Source / Open Data	102](#opensourceopendata102)
[9. – Deprecated - Next Steps [suggest delete, see new sec 8]	102](#9deprecatednextstepssuggestdeleteseenewsec8102)
[9. References	105](#9references105)
# 

# 0. META

**Authors****, ADW Convening Team**

| Group | People | About |
|---|---|---|
| OpenTEAM | Anna Lynton,
Eliza Baker-Wacks | OpenTEAM, or Open Technology Ecosystem for Agricultural Management, is a farmer-driven, collaborative community of food system stakeholders (farmers, ranchers, scientists, researchers, engineers, farm service providers, programmers and food companies) who are committed to improving soil health, expanding regenerative agriculture, and advancing agriculture’s ability to become a solution to climate change. |
| Tech Matters | Steve Francis, Hana Lee, 
Katy McKinney-Bock  | A non-profit open source developer which creates software solutions for communities whose needs are not being met by commercial software products.  Tech Matters creates in a co-design process and seeks input from potential users around the world.  They bring their methods, experiences, and technical and design expertise to this collabathon. |
| Terran Collective / Hylo | Clare Brodeur, Tibet Sprague | Terran is a non-profit collective, working to amplify cooperation among people working to regenerate our communities and our planet. We are technologists and community organizers building systems and tools for more cooperative coordination. Our main project is Hylo - the prosocial coordination platform for purpose driven groups. We are also passionate about bioregional organizing and the role the food systems play in healing our local ecological and cultural landscapes. |
| Agricultural Informatics Lab @ Purdue University | Ankita Raturi,
Laila Dodhy,
Autumn Denny | A university-based informatics research lab working across food, agriculture, ecology, and technology in service of resilient food futures. We build a range of products, from farmer-facing decision support tools and community-oriented local food system toolkits to developer-facing APIs that serve a range of agricultural data. We conduct human-centered design research, translate our findings into usable and useful software prototypes, think critically about the role of technology in local and global food systems, and build real tools for real users.  |

**Authors****, ADW Convening Team**

**[OpenTEAM](https://openteam.community/)**

*Anna Lynton, Eliza Baker-Wacks*

OpenTEAM, or Open Technology Ecosystem for Agricultural Management, is a farmer-driven, collaborative community of food system stakeholders (farmers, ranchers, scientists, researchers, engineers, farm service providers, programmers and food companies) who are committed to improving soil health, expanding regenerative agriculture, and advancing agriculture’s ability to become a solution to climate change.

**[Tech Matters](https://techmatters.org/)**

*Steve Francis, Hana Lee, Katy McKinney-Bock*

A non-profit open source developer which creates software solutions for communities whose needs are not being met by commercial software products.  Tech Matters creates in a co-design process and seeks input from potential users around the world.  They bring their methods, experiences, and technical and design expertise to this collabathon.

**[Terran Collective / Hylo](https://www.terran.io/)**

*Clare Brodeur, Tibet Sprague*

Terran is a non-profit collective, working to amplify cooperation among people working to regenerate our communities and our planet. We are technologists and community organizers building systems and tools for more cooperative coordination. Our main project is Hylo - the prosocial coordination platform for purpose driven groups. We are also passionate about bioregional organizing and the role the food systems play in healing our local ecological and cultural landscapes.

**[Agricultural Informatics Lab @ Purdue University](https://aginformaticslab.org/)**

*Ankita Raturi, Laila Dodhy, Autumn Denny*

A university-based informatics research lab working across food, agriculture, ecology, and technology in service of resilient food futures. We build a range of products, from farmer-facing decision support tools and community-oriented local food system toolkits to developer-facing APIs that serve a range of agricultural data. We conduct human-centered design research, translate our findings into usable and useful software prototypes, think critically about the role of technology in local and global food systems, and build real tools for real users.

**Disclaimer**

This report is a snapshot of our assumptions, ideas, and understanding of trust, as part of the work for the OpenTEAM Ag Data Wallet project. This report is not intended to be a comprehensive review of literature, but instead captures our current process, open questions, and needs, as well as the resources we gathered towards an effort to bridge the gap between trust theory and practice, with emphasis on agricultural technologies.


**Glossary**

| Abbreviation | Term and Synonyms | Description |
|---|---|---|
| ADW | Ag data wallet; 
Agricultural data wallet | OpenTEAM term for a conceptual digital storage system for all information collected during or required for producer operations. |
| Ag Tech | Agricultural technologies | Refers to digital technologies designed for use in the agricultural sector. |
| Ag Data | Agricultural data | Refers to data collected, used, or otherwise managed by agricultural stakeholders. |
| Collabathon | Collaboration marathon | Collabathons are a sustained collaboration effort with short sprints in service of long range shared goals implemnented by the OpenTEAM community to acomplish goals in key work streams. Each Collabathon session has a defined goal, outcome, and proposed output shaped by a community co-hosts. |
| Convening Team | Authors, Conveners | Term used to refer to the authors and lead contributors to this report and process. |
| Data Sovereignty | Although sometimes used to refer to the preservation of national interests in data, we use the term to identify the rights of individuals or groups to control the use of data about them or their environment.  |  |
| Producer | Any person who operates a farm or is directly involved in agricultural production. |  |
| GDPR | General Data Protection Regulation | A European Union law that sets out requirements for how organizations can collect, store, and manage personal data. |
|  |  |  |
| TAP | Technical Assistance Provider | Individuals who offer specialized services to producers with the goal of assisting in and improving conditions for a producer. |
| UX | User experience | UX/UI is often used to encompass both experiential and interaction components of user-centered design. |
| UI | User interface |  |
|  |  |  |
|  |  |  |
| FAIR principles | Refers to research data management and stewardship principles: Findable, Accessible, Interoperable, Reusable (Wilkinson et al., 2016). |  |
| CARE principles | Refers to Indigenous data governance principles: Collective Benefit, Responsibility, Authority to Control, Ethics (Carroll et al., 2020). |  |
| TRUST principles | Refers to principles for digital repositories: Transparency, Responsibility, User focus, Sustainability, Technology (Lin et al., 2020). |  |


# 1. Introduction

Agriculture entails a variety of knowledge work activities as people grapple with the intermingled management of data about farms, people, land, and natural resources. As such, there are many agricultural data stakeholders, from people that directly produce and use ag data such as land stewards (e.g., farmers) and their advisors (e.g., service providers, extension agents) to people that review and communicate ag data such as certifiers, marketing groups, and government entities. Each of these data stakeholders has different goals, desires, and needs with respect to data management. **There is, in theory, alignment across ag data stakeholder groups with respect to the need for interoperable, streamlined, and responsible ag data management and sharing, as well as consensus that the status quo must be improved to reduce the cognitive overhead of managing information.**

**This collabathon was prompted with a vision for an “Agricultural Data Wallet” (ADW): a digital concept that enables farmers, ranchers, and land stewards to securely store, access, and manage their data while exercising full control over who can see and use it.** The hypothesis underlying this vision is that by collaboratively designing and implementing the ADW,  agricultural data management will be streamlined - reducing administrative burdens, and increasing privacy and control over data. This, in turn, will lead to improved ecological outcomes, greater data sovereignty, and increased economic opportunities for land stewards.

To pursue this vision, OpenTEAM convened a group of experienced organizations, Terran Collective, Tech Matters, and Purdue’s Agricultural Informatics Lab, i.e., the “convening team”, to engage in a year-long collaborative effort, i.e., a “collabathon”. **Our collabathon goal was to take a more human-centered approach to the design of an ADW, focusing on identifying User Interface and User Experience (UX/UI) design considerations, contextualized in opportunities across the technology landscape, and motivated by real ag data stakeholder needs.**

Over the last year, we have have researched, interviewed, debated, and refined our thinking around (a) how to conceptualize trust in Ag Tech, (b) how different ag data stakeholders think about trust, data, and technology in an agricultural context, and (c) what design patterns might enable digitally-mediated trust in Ag Tech.*** ***We brought together diverse stakeholders, including producers, technologists, designers, researchers, and government agencies, to identify key data-sharing challenges and collaboratively design user experiences (UX) that meet the agricultural community's needs. The focus was on exploring system architectures and UX design patterns that are intuitive, functional, and adaptable across platforms (e.g., mobile, desktop). These patterns are intended to provide a familiar and seamless experience for users, ensuring that the ADW becomes a trusted tool for producers.

The following materials document the outcomes of the 2024 ADW UX/UI Collabathon.

Specifically, we…

- **Developed a landscape review** that explored the state of research and practice on concepts related to trust, sovereignty, data sharing, privacy, and related topics in human-computer interaction, in the context of the agricultural community

- **Interviewed technical subject matter experts** on the topic of agricultural data management, including coalitions focused on ag data education, policy, and interoperability

- **Interviewed producers and Technical Assistance Providers (TAPs)** on common challenges encountered with agricultural data technologies

- **Hosted community sessions** to share findings and get feedback along the way. 

This resulted in:

- The development of a UX Design Patterns catalog (Section 6) to provide practical recommendations to address recurring usability issues encountered by product teams

- The development of suggested user interface elements to address the use cases that emerged from the interviews and community sessions

- General recommendations for Ag Tech under a set of key themes: Data, Trust, UX Design, and Open Source/Open Data.

These outputs capture the insights, challenges, and recommendations that emerged during the Collabathon. This collabathon is part of a greater body of work created by the OpenTEAM community and will continue to be built upon. OpenTEAM’s collaborative efforts are focused on enabling a more interoperable ag data ecosystem, built on farmer consent and trusted data transaction to create a more data-driven, farmer-benefitting, and regenerative ag sector. The outputs of this project are focused on the UI/UX components needed for trusted data storage and transaction – this is part of our area of work focused on farmer data stewardship and consent. Other projects within our community have focused on common user roles and credentials, recommendations for [Agricultural Data Use Agreements](https://openteam-agreements.community/), and the needed data  formats / conventions to allow for ease of data transfer across tools in a manner that is understandable for land stewards. Each of these components contributes to an envisioned data space in which farmers can manage their own information, or grant rights and permissions to others in their network to manage their information for the purpose and value that they would like to achieve.

In future phases of work, the OpenTEAM community will build connectivity between these previous and ongoing areas of work, including outputs of this collabathon, through the creation of reference implementations, which could be tested by land stewards in our network and will serve as well-documented technical examples which could be scaled. We anticipate near-term projects focused on prototyping user-facing consent management (data use notifications and agreements) to test both the recommended UI/UX patterns discovered in this collabathon, as well as the content of our community-designed data use agreements in existing agricultural data management tools for rapid user feedback and iteration.

# 2. Trust & Technology in Agriculture: Research Literature

The last two decades have emphasized increased digitization in agriculture, including a rapidly expanding technology ecosystem to support the many forms of agricultural knowledge work. Digital technology in agriculture (Ag Tech) includes software to: manage farm operations and supply chains; monitor and improve environmental, economic, and social outcomes; maintain records for certifications and regulatory reporting; and coordinate and collaborate among cross-sector agricultural stakeholders. While a plethora of technologies exist, there remain low technology adoption rates and a general sense of mistrust among agricultural stakeholders. Our current understanding of people working in agriculture is that there is a widespread lack of trust around collection, management, and sharing of data, as well as towards technology in general. This work  is motivated by the desire to enable a more trustworthy Ag Tech ecosystem, in particular, to increase data sovereignty in agricultural communities, as informed by a growing body of theory and practices around data sovereignty. **Our theory of change is that by adopting a more human-centered and consistent approach to technology design, we may be able to build, improve, and maintain trust among Ag Tech users, developers, and stakeholders.**

**There remains a need to bridge gaps across theory and practice. **Ag Tech development typically incorporates cybersecurity best practices as the overarching framework for enhancing trust. Technology roadmaps include focus on implementing user authentication, data management and access controls, and some privacy-preserving protocols. Popular user experience (UX) best practices, primarily from domain-agnostic technology research in both  industry and academia, tend to offer generic guidance, such as ways to improve privacy. However, there are unaddressed challenges in translating UX best practices for domain-driven design, particularly within agriculture. In research literature on trust in Ag Tech, there is significant critique of untrustworthy practices within enterprise Ag Tech, such as data reuse, with little attention to the more nuanced software landscape that has been emerging in the last decade.

**In this section, we summarize our preliminary understanding of concepts related to people, trust, data, and technology in an agricultural context. **We have synthesized our efforts to date that will inform the concurrent development of a suite of design patterns for an Agricultural Data Wallet (ADW or Ag Data Wallet). This included reviewing relevant research literature and reflections from our own professional experiences. We share preliminary findings on best practices for user-centered design, and planned efforts to dig into software solutions and technologies. This section, and corresponding database of resources, forms an initial basis for our community engagement process and the subsequent collabathon outputs: a set of interviews with technologists and a design catalog to support Ag Tech developers, that in sum provide recommendations for the human interface design of an Ag Data Wallet. 

In the planning process for the ADW collabathon, we identified a need for a shared understanding of trust and what it means to cultivate trust in agricultural research and technology. While there was an Ag Data Glossary developed by openTEAM ecosystem members ([OpenTEAM, 2023](https://openteam-agreements.community/TermsandDefinitions/)) that described terms such as “accountable”, “data governance” and “personal data” for use in the OpenTEAM data agreements, they did not provide sufficient detail for implementation within UX/UI components.** We identified a lack of shared understanding around how to define trust in the context of an Ag Data Wallet. This section contributes a method and definition of *****trust***** and *****design patterns*****, building on prior art.**

**The rest of this section is structured as follows.** In section 2.1, we describe a preliminary definition of trust and trust-framework, based on academic literature that looks at trust in data and technology. This definition was used in subsequent collabathon efforts to review the Ag Tech landscape and provide a UX catalog of trust-building design patterns. In section 2.3, we share brief literature on trust in the Ag Tech context. In section 3, we describe convener reflection activities that we conducted to form our baselines shared understanding of trust. In section [x], we briefly describe best practices to manage various dimensions of trust from UX/UI examples in e-commerce and financial technologies. In section 6, we provide a summary overview of the Ag Tech landscape, that is, software, organizations, and other information sources. In subsequent collabathon efforts, a team will interview technology subject matter experts on their perspectives on trust in Ag Tech. 

## 2.1 What is Trust?

Trust is a “willingness to be vulnerable” ([Mayer et al., 1995](https://www.jstor.org/stable/258792)*)*. It is one’s willingness to take a risk based on their belief in something. Human trust is a leap of faith: after developing some type of interpersonal relationship, we might then assess the level of risk of believing another, then choose to be vulnerable, and accept the consequence. However, trust, when defined using belief, relies on abstract notions of “goodness” or “harm”, which is often difficult to operationalize, measure, or even communicate effectively. Nevertheless, belief is a fundamental component of trust. Rousseau et al., ([1998](https://journals.aom.org/doi/abs/10.5465/AMR.1998.926617)) provides a popular definition of trust as “*a psychological state comprising the intention to accept vulnerability based on positive expectations of the intentions or behavior of another*”. Thus, trust specifically includes our belief in the predictability of others, for instance, that one can indeed be relied upon to do what they say they will do. Luhmann ([1979](https://books.google.com/books?hl=en&lr=&id=CKBRDwAAQBAJ&oi=fnd&pg=PR3&ots=9HQAxeiMeZ&sig=RaYaMHvIBB3dMidrQY6HBBIG3d0#v=onepage&q&f=false)) writes, *“a complete absence of trust would prevent [a person] from even getting up in the morning. [...] Anything and everything would be possible. Such abrupt confrontation with the complexity of the world at its most extreme is beyond human endurance.”* **Interpersonal trust is ultimately a mechanism for managing the complexity of modern social dynamics** (Luhmann, 1979): **it is a mechanism by which humans grapple with complexity. **

Gambetta ([1998](https://philarchive.org/rec/GAMTMA)) offers a summary of how humans develop **initial trust**: *“Trust (or, symmetrically, distrust) is a particular level of the subjective probability with which an agent assess that another agent or group of agents will perform a particular action, both before he can monitor such action (or independently of his capacity to ever to be able to monitor it) and in a context in which it affects his own action [...]. ****When we say we trust someone or that someone is trustworthy, we implicitly mean that the probability that he will perform an action that is beneficial or at least not detrimental to us is high enough for us to consider engaging in some form of cooperation with him. ****Correspondingly, when we say that someone is untrustworthy, we imply that that probability is low enough for us to refrain from doing so.”*

Mayer et al. (1995) conceptualize the characteristics of a trustor and trustee that can lead to trust, and subsequently trust-enabled cooperation. People that trust, or trustors, have a propensity to be more or less trusting of others based on their own experiences, personalities, culture, resulting in varying *“generalized expectation[s] about the trustworthiness of others”* (Mayer at al., 1995). This is the initial disposition of the trustor. Trustees exhibit characteristics that subsequently improve or diminish a person’s willingness to trust them, that is, their trustworthiness. Mayer et al, (1995) describe a summary set of antecedent factors that can influence trust: ability, i.e. skills or competencies in a domain; benevolence, or, believable good intentions; and integrity, or adherence to acceptable principles. **Trust factors can be summarily classified as either *****trust in motives***** and *****trust in competence***** **([Twyman et al., 2008](https://sjdm.org/journal/bb10/bb10.html)).** **Consider an example in which I seek to purchase a sustainable chair: I might trust a furniture-maker’s sustainability claims regarding the chair if (a) I am confident in their ability to produce a chair using sustainable manufacturing processes, (b) I believe that they tried to the best of their ability to produce a “sustainable” chair, that their intentions and actions are aligned, and (c) that they are following some acceptable principles or even measurable metrics by which they judge the chair as sustainable. These factors may improve my trust disposition toward the manufacturer once they pass initial muster, and further improve my propensity to trust them. These characteristics of the trustee set the scene for further cooperation. 

**Trust is a dynamic and ongoing process. **Rosseau et al. (1998), in their study of trust definitions across disciplines offer that *“[t]rust is not a behavior (e.g., cooperation), or a choice (e.g., taking a risk), but an underlying psychological condition that can cause or result from such actions.”*** **Social trust begins with a mental state, or “attitude” toward others, that is, a combination of beliefs, goals, values, and so on ([Falcone & Castelfranchi, 2001](https://doi.org/10.1007/978-94-017-3614-5_3)), where trust-relationships are subjective negotiations among trustors and trustees. Hardin ([2002](https://www.russellsage.org/publications/trust-and-trustworthiness-1)) describes trust as *“encapsulated interest”*: people trust each other via accounting for each other’s interest, even when these interests conflict or are non-overlapping. A trustor assesses the trustworthiness of a trustee via three sources of information: reflecting on historical interactions, inferred from projections about how much the relationship is valued, and inferred from the broader social network ([Schilkke et al., 2001](https://www.annualreviews.org/content/journals/10.1146/annurev-soc-082120-082850)). However, just because one person trusts an organization does not mean everyone will, and similarly just because an organization has earned the trust of some people right now, does not mean that trust is in perpetuity. Rosseau et al. (1998) describe the ebb and flow or a trust relationship where people are engaged in (re)building trust, maintaining stable trust, and managing the dissolution of trust. Operationalizing trust is thus increasingly complicated as you consider that trust often involves groups of people, whether it’s a peer group you are a part of, an organization that produces something you use, or even a more formalized or distant relationship with an institution you cooperate with, that is, institutional trust.

**Trust is expressed via reliance, whether on individuals, organizations, or artifacts, including technology, produced by others.  **It is a three-part relationship: A trusts B to do X (Hardin, 2002). However, *entity A might typically trust entity B with X within a context Y*, not otherwise. As Mayer et al. (1995) point out,* “[t]he question ‘do you trust them’, must be qualified: trust them with what?”* For instance, you might trust a person to facilitate a meeting, but not to cut your hair: both task and context matter. When the trust relationship is with a group of people, it is dependent on their collectively agreed upon rules of engagement, whether based on implicit social norms or explicitly codified, for instance, via a contract. In that sense, X can be governed by a third entity, a trust-intermediary, whether it is a person, organization, contract, or even a technology, that manages the violations and consequences of whether or not B upholds the bargain made with A. Trust can thus be afforded to various kinds of *agents*: individuals, groups of people, and even the artifacts designed by humans. We trust that a chair will not break when we sit on it because the builder was competent, the materials robust, and the human-chair interaction was not outside of its intended use cases. Falcone & Castelfranchi (2001), describe the social act as “*delegation*” or being reliant on something predicated on the trust relationship. That is, I trust the chair will not break, because I trusted the builder of the chair. **It is in this manner that we can conceptualize trust when thinking about digital technologies.**

## 2.2 Dimensions of Trust in Technology

Bodo ([2020](https://doi.org/10.1177/1461444820939922)), states that “*digital technologies shape how humans trust each other, and that in order to fulfill this task, they need to be trustworthy.” *Digital technologies mediate interpersonal relationships, institutional interactions, and knowledge work across modern society. Bodo (2020), builds on this mediation concept: “*[A]s we use digital technologies to mediate interactions that require or produce trust, these technologies turn into trust mediators”. *Thus, there is both **need and motivation to better understand socio-technical factors that influence digitally-mediated trust.**

*Consider the following scenario:*

*An unknown software developer debuts a weather app that purports to provide mid-term forecasts (weeks) at a local scale (sub-acre) with high accuracy claims. The base technology (smartphone app that utilizes global weather data in combination with user location data at minimum) seems familiar. You don’t know anything about the developer, but the functionality of the tool also seems relevant for your work. You download the app and try it out.*

*Assuming that the weather app is indeed useful and is relatively easy to use, you’ll need to figure out if you’ll keep using it. Given that it’s built by a new vendor, you may not know what their data management practices are, what their intentions with your data are, and if they will continue to stick to their current practices. In that sense, you begin to form ideas about whether or not you trust this technology based on the developers’ ability (likely demonstrated by the app performance) as well their benevolence, integrity, and predictability, which are harder for you as a user to evaluate without digging into the app functionality, developer background, and more. You start to define your level of trust in the developers and their technology.*

Technology acceptance*** ***is a popular research concept that helps us understand factors that influence user adoption of new technologies ([Marangunic & Granic, 2015](https://doi.org/10.1007/s10209-014-0348-1)). **Technology acceptance models suggest that a user’s behavioral intention to use a technology is influenced by perceived usefulness (PU) and perceived ease-of-use (PEOU) of the technology **(Davis, 1989). Individual reactions to various aspects of a new technology, including the User Experience (UX) determine their desire to use the technology, and ultimately, determine whether or not a technology is well-adopted ([Venkatesh  et al., 2003](https://www.jstor.org/stable/30036540)). Venketash et al. (2003) show that technology acceptance is influenced by:* performance expectancy*: “the degree to which an individual believes that using the system will help him or her to attain gains in job performance”*; *effort expectancy: “the degree of ease associated with the use of the system”; social influence: *"*the degree to which an individual perceives that important others believe he or she should use the system”; and the facilitating conditions: “the degree to which an individual believes that an organizational and technical infrastructure exists to support use of the system”. That is, **once a person tries a new technology, their actual use of the technology is influenced by whether or not it actually helps them achieve the **
**tasks it claims to support, and how easy it is to perform the task using the new tool, as well as their social network and the use context.** 

*Figure 1: Factors that influence technology adoption, figure from (Marangunic & Granic, 2015)*

Technology acceptance is also influenced by user characteristics such as *gender, age, computer literacy, social or cultural background*, but these do not determine technology acceptance alone. Similarly, cost-benefit and self-efficacy can lower barriers to adoption (Davis, 1989) leading to the diffusion of innovations, but do not determine technology acceptance alone. In sum, factors that influence technology acceptance among user groups can help us understand barriers to adoption of technology. Ultimately, both perceived usefulness and perceived ease of use are *“belief constructs”*, that is, a user needs to believe these to be true (Marangunic & Granic, 2013). Trust is an equally important and interconnected driver of adoption: for  instance, trust in e-commerce is built through user belief that the vendor using the e-commerce platform has nothing to gain by cheating, belief in the safety mechanisms of the technology platform itself, and by providing an easy to use user interface and a familiar user experience ([Gefen et al., 2003](https://www.jstor.org/stable/30036519?seq=1)). **Thus, developing a framework to design a user experience that enhances trust-in-technology requires us to consider usefulness and usability (PU and PEOU). **

*Figure 2: Factors that influence trust in technology, figure from (Williams & Kitchen, 2011)*

Williams & Kitchen ([2011](http://services.igi-global.com/resolvedoi/resolve.aspx?doi=10.4018/978-1-60960-575-9)), for instance, describe a summary set of factors that have been demonstrated in trust research to trust in online platforms: “*Usability and Navigability; Website design, likeability and attractiveness; familiarity (structure of the interaction / interface); interactivity; social presence; security, privacy, and returns disclosures; third-party endorsements; and peer recommendations and testimonials.” *

When considering* *trust-in-technology, we refer to the user’s willingness to believe that the technology will do what the developers claim that it will do. The user relies on the technologist 

to provide an accurate description of what a technology will and will not do, and the technologist relies on the user’s ability to understand the implications of these claims with respect to their interaction with the technology. **To build users’ trust-in-technology, the***** *****developers must first demonstrate and communicate their trustworthiness **(Gefen et al., 2003)**. **Operationalizing trust-in-technology requires negotiation among technology users (trustors) and technology developers (trustees). When developers claim that a technology can be trusted, they are both making a claim that they are trustworthy and that the user can delegate trust to the technology. In that sense, technology can also become a mediator of trust in the relationship between users and developers ([Bodó, 2020](https://journals.sagepub.com/doi/pdf/10.1177/1461444820939922)). **Thus, developers must also demonstrate and communicate the trustworthiness of the technology itself, since it both codifies the developers’ notions of trust and mediates the user-developer trust relationship.**

Technologists often describe **open source software production as a mechanism to demonstrate transparency**, since, at minimum, the software’s source code is made publicly available. Best practices in open source development include providing documentation for users and fellow developers alike, as a means to improve the readability and understandability of the source code. While taking an open source approach to technology development is a productive step toward improving transparency and accountability, among other software qualities, it alone does not suffice at fostering trust in technology. Hou & Jansen ([2023](https://link.springer.com/article/10.1007/s10664-022-10238-y)) describe: “*a knowledgeable end-user can determine whether a software package is trustworthy and appropriate for the current project based on his previous experience. Correspondingly, a person without any IT background and experience has little propensity in the software package selection*.” Furthermore, when digital technologies are designed for non-technologist users, as is common in the agri-food sector, open source can create both opportunity for user awareness as well as misunderstanding around *what *is made public: the data versus the code. Akin to questions around the provenance of food that influence consumer trust and decision making, there is a growing interest in the underlying modes of production that result in a digital technology: who funds the development? What are the governance models? How do organizational values and priorities influence the design of technology? How you demonstrate trustworthiness as a developer also depends on the types of users you are designing for. For instance, in the more enterprise-oriented technology sectors, where institutional purchasing involves a contract-and-bidding process, there are typically software quality standards and metrics used to evaluate the potential risks and benefits of adopting a technology. Hou & Jansen (2023) describe: “*standardization, processes, and models have been proposed to evaluate, select, and adopt software to address “trust erosion” from academic and industrial perspectives*.”** Intrinsic and extrinsic trust factors factors get evaluated in terms of the software product quality **(e.g., reliability, usability, maintainability, interoperability, fault-tolerance, security), **software development processes** (e.g., programming language, dependencies, versioning), and **developer or software project characteristics **(e.g., reputation, popularity, cost, documentation).

Trust is often used as a “heuristic[^1] ”, or a “decisional shortcut” ([Lewicki & Brinsfield, 2011](https://researchonline.stthomas.edu/esploro/outputs/bookChapter/Framing-Trust-Trust-as-a-Heuristic/991015164055503691)) by technology researchers and developers to frame conversations around technology acceptance, to explain the value of user-developer interactions, and even to rationalize barriers to technology adoption. Trust, when framed as a heuristic, is a *“mechanism for processing information that allows the decision maker to select some information and ignore other information as a way to make a quicker or ‘easier’ (less complex) decision” *(Lewicki & Brinsfield, 2011). Building on Lewicki & Brinsfield’s (2011) description of trust as a helpful cognitive frame, or a dynamic social construct, we can begin to consider how to reconcile what we learn about diverse and new human-computer interactions across the agri-food sector. **As people across the Ag Tech landscape continue to experience an increased cognitive demand to process the glut of Ag Data, they may use their assessment of trust in a particular technology as a shortcut to determine whether or not they should ultimately use or rely on a tool.**

## 2.3. The State of Trust in Ag Tech 

In this section, we consider the varied framings of trust in the Ag Tech literature, including the frequent considerations of trust as a decision making shortcut.

### Low trust in technology and low adoption rates across Ag Tech

To understand why many farmers lack trust in Ag Tech, it's important to grasp that they see their farm data as personal, even if privacy laws may not necessarily consider it as such ([Wiseman et al., 2019](http://www.sciencedirect.com/science/article/pii/S1573521418302616)). This brings us to the next point, privacy. Trust in technology also lies in whether users can trust it to protect their privacy which is why events such as the [Cambridge Analytica-Facebook](https://bipartisanpolicy.org/blog/cambridge-analytica-controversy/) scandal are so controversial. Wiseman et al. (2019) conducted surveys involving 1000 Australian farmers, revealing that over half of the respondents lack trust in service/technology providers, with an even larger proportion expressing discomfort at these providers profiting from their data. This phenomenon is noted in other studies and can be better expressed with the words of a farmer:

*“make some big business model and they make a good living off it and the guys that generate it all miss out. ...I’m of the thinking that**** we’re probably at the bottom of the food chain****. We’ve got something that maybe someone wants collectively and if they get it for nothing it just doesn’t feel right. (Grower 4)”* ([Jakku et al., 2019](https://www.sciencedirect.com/science/article/pii/S1573521418301842))

It is important to note that farmers are protective of their data, particularly in financial and regulatory contexts ([Slattery et al., 2021](https://h2020-demeter.eu/wp-content/uploads/2021/06/Farmer-Perspectives-on-Data-2021-1.pdf)). Therefore, privacy is crucial for establishing trust and fostering ongoing collaborations among various stakeholders ([Raturi et al., 2022](https://onlinelibrary.wiley.com/doi/abs/10.1002/agj2.20974)). 

Another factor is the limited transparency surrounding the ownership and usage of Ag Data. Farmers frequently lack a clear understanding of the contractual agreements related to farming equipment, often obscured by ‘click wraps’ where clicking ‘I agree’ implies acceptance of the software license terms (Wiseman et al., 2019). The absence of informed consent leads to cognitive dissonance between what a farmer thought they agreed to and the legal reality, exacerbating mistrust with Ag Tech. Moreover, Ag Tech is expensive but crop prices are unstable and relatively much lower which makes the farmers even more risk-averse to adopting smart farming ([Rotz et al., 2019](https://onlinelibrary.wiley.com/doi/full/10.1111/soru.12233)).

### Lack of actionable trust guidelines, best practices, and legal frameworks

Next, we will discuss the shortcomings of current frameworks in smart farming. As previously noted, farmers often perceive that as data originators they receive minimal returns in terms of profits and legal autonomy. As a result, legal frameworks, particularly those regarding contractual agreements, have to be put in place to establish trust.

*"Until the legal dimensions of the socio-technical big data discussion taking place in Smart farming is addressed, farmers will continue to have ‘mixed feelings’ towards the ways in which they are being made to interact with smart farming technologies and their providers"  *(Wiseman et al., 2019)

Copyright laws typically do not protect raw data, which is inevitably generated by Ag Tech such as yield monitors. It's worth noting that in the EU, there is a specific legal framework known as the “database right” that provides protection against the extraction and reutilization of raw data ([Directive on the Legal Protection of Databases 1996](https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=celex%3A31996L0009)). However, contractual agreements can override relevant laws even if they are *sui generis* Ag Data sharing laws, which adds more complexities (Wiseman et al., 2019). Therefore, we need frameworks that can take these factors into account. 

Figure 3: Checklist for creating a balanced contract for Ag Data, figure from ([COPA & COGECA, 2018](https://croplifeeurope.eu/wp-content/uploads/2021/03/EU_Code_of_conduct_on_agricultural_data_sharing_by_contractual_agreement_2020_ENGLISH.pdf))

*“An analysis of the proposed industry standards suggests that farmer groups are likely worried about the wrong things, specifically with regard to data use for speculation, rather than the ****dynamic consequences of data usage guidelines for future competitive market outcomes****” (**[Sykuta, 2016](https://ageconsearch.umn.edu/record/240696)**)*

Since there is no one-size-fits-all solution for the various situations that may arise, it is necessary to establish guidelines that can be adapted to each specific situation. A good example is the non-binding guideline below from the EU Code ([COPA & COGECA, 2018](https://croplifeeurope.eu/wp-content/uploads/2021/03/EU_Code_of_conduct_on_agricultural_data_sharing_by_contractual_agreement_2020_ENGLISH.pdf)). 

### Misunderstood drivers regarding willingness-to-share Ag Data

The limited adoption of Ag Tech and sharing of Ag Data is partially due to the inadequate digital infrastructure in certain regions which in turn raises concerns amongst farmers about an even larger city/country divide (Jakku et al., 2019). Another consequence of the lack of infrastructural access is increased friction in their user journey when it comes to Ag Data collection and sharing (Slattery et al., 2021). Furthermore, we need to consider the technical competencies required to adopt new technology in smart farming. Older farmers are less inclined to embrace new technologies and prefer to adhere to more traditional farming methods. Even if they buy the equipment, it is ultimately not used as they do not have the means to transform the raw Ag Data into anything meaningful–which also shows the gap between the end-users and those creating technology for them (Rotz et al., 2019). Even analyzed data gathered by Ag Tech can be found lacking in comparison to their experiential on-farm knowledge which is often intergenerational and can not simply be transferred to an algorithm ([Duncan, 2018](https://atrium.lib.uoguelph.ca/items/48af86e3-e828-40e9-80c5-d0e8840e5aa9)). 

*“We characterize this as ‘top-down’ technological development, as opposed to ‘bottom-up’ or ‘farmer-driven’ development. In the former case, technology is developed, and data is collected outside of farmer networks and are thus less likely to be directed by the needs of farmers. ****As a result, many – especially agroecological – farmers argue that technologies currently promoted to them are not effectively or accurately solving the on-farm problems that they are facing.” ***(Rotz et al., 2019)

While farmers would not hand over their yield data to other farmers, they are still willing to discuss and share their knowledge (Jakku et al., 2019). Farmers do not necessarily limit the sharing of their expertise regarding Ag Tech as they even have “hackathons” (gatherings to create needed software) to repair or even collect data from their equipment (Rotz et al., 2019). However, there is a reluctance to “openly” share data, which is worsened by the widening economic gap between small/medium farmers and large farmers.

*“...We might look at them together and talk about different farming methods and the physical, but ****you’re never going to walk away with that data****…(Input provider 4)” *(Jakku et al., 2019)

### Lack of implementation of risk assessment as a means to assess threat-levels

Open source code and data have been looked at as a solution for establishing trust mainly due to increased transparency (Raturi et al., 2022). However, there are nuances even in the storage of data. Farmers consider Ag Data to be personal which can be easily argued for because precision farming can involve GPS tracking (Wiseman et al., 2019). To keep in line with existing privacy laws such as GPDR, Personally Identifiable Information (PII) should be anonymized but that depends on how personal is being defined ([Wilgenbusch et al., 2020](https://cgspace.cgiar.org/handle/10568/108095)). This is just one example of the lack of consensus regarding how privacy should be implemented in Ag Data.

Wilgenbusch et al. (2020) developed a framework categorizing Ag Data breaches into low, medium, and high-risk levels. According to their model, if the breached data contains personally identifiable information (PII) and poses a significant risk of severe harm to individuals or institutions, it is classified as high-risk. The severity of such breaches can encompass substantial financial and operational losses, and in extreme cases, even loss of life. To give an example of a high-risk data breach, in 2017, chicken farmers sued Tyson Foods and other processors for sharing production data to suppress grower payments, highlighting concerns over inadequate data anonymization and unlawful sharing practices (Wiseman et al., 2019). The authors also mapped data management principles with both technical and non-technical standards to offer guidance on safeguarding data, considering factors such as its privacy status and varying levels of risk. Therefore, cybersecurity and anti-competitive practices have to be given more weight moving forward to create and maintain trust, especially considering the risks that data breaches can entail.

### Lack of consensus on who is responsible for what data and how

*“They [digital agriculture service providers] say the**** farmer owns the data, the farmer, legally that’s true but practically what does it mean? Almost nothing.**** A far more interesting and *
*pertinent question is what are they doing with that farm data? (Grower 2)” *(Jakku et al., 2019)

Figure 4: Strategy to Govern Open Data Ownership, figure from (Beer, 2017)

Data ownership is a serious point of contention in agriculture. Ag Data can be considered to be owned by farmers. Multiple stakeholders, such as research collaborators and entities involved in machine data collection and analysis, may also have rights depending on contractual agreements (Raturi et al., 2022). Agriculture Technology Providers (ATPs) can also be extranational entities which might mean local laws are not applicable (Wiseman et al., 2019). Data collection can even involve other stakeholders indirectly such as satellite imagery which can capture Ag Data without trespassing on private property in the traditional sense ([Beer, 2017](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3015958)). 

More complications are added when the data is processed and distributed beyond the farm. For example, cloud-based platforms utilize Internet services for data storage, management, and processing, offering enhanced accessibility and efficient data exchange. However, they also centralize access and control with service providers rather than users ([Rotz et al., 2019](https://onlinelibrary.wiley.com/doi/full/10.1111/soru.12233)). Therefore, there is a lot of uncertainty surrounding the ownership and usage of Ag Data. Other researchers have suggested strategies to resolve these issues (see Figure 4).

## 2.4. Lessons from UX/UI Design Practice

**In this section, we describe the state of trust-cultivating design patterns and practices, drawing on themes from the UX pattern research in e-commerce and fintech. **We cataloged UX/UI patterns, design systems, and UX principles across tech sectors related to trust in human-computer interactions. This includes reviewing: design pattern guides, common design recommendations to adopt, dark patterns to avoid, user interface components, and domain-specific user experience and design lessons to integrate. We paid special attention to how trust affects consumer behaviors in e-commerce and Financial technology (Fintech). We selected these fields because they have adopted technology-mediated customer interactions such as online shopping and mobile banking for several decades and have established user interaction conventions and standards.

Trust within a community is an inherently social process ([Lewis & Weigart, 1985](https://doi.org/10.2307/2578601)). Trust can be mediated through technology, but only with the input of all data stakeholders and trust-fostering practices ([Raturi & Thompson, 2021](https://acsess.onlinelibrary.wiley.com/doi/pdf/10.1002/agj2.20974)). This landscape review taught us that multiple antecedents influence customers’ trust while interacting with online services and products. Online purchase decisions can be influenced by reputation, privacy concerns, and security concerns ([Kim et al, 2008](https://doi.org/10.1016/j.dss.2007.07.001)). Perceived risks in privacy and security can undermine online banking and financial services adoption ([Vasquez & San-Jose, 2022](https://www.raco.cat/index.php/rljae/article/view/398681)). Understanding themes of trust-fostering and privacy-preserving practices from mature fields will benefit our project. 

### Privacy

Consumer privacy is the consumer’s ability to control (a) the presence of other people in the environment during a market transaction or consumption behavior, and (b) the dissemination of information related to or provided during such transactions or behaviors to those who were not present ([Goodwin, 1991](https://journals.sagepub.com/doi/abs/10.1177/074391569101000111?journalCode=ppoa)). 

Privacy concerns in electronic banking pertain to the customers' apprehensions about how their personal and financial information is collected, stored, and used by banking institutions. This involves fears about unauthorized access, misuse, or sharing of their sensitive data​​. Customer’s perceived privacy risks also negatively impact the online shopping experience. Empirical studies have found that financial services consumers are often reluctant to share personal information from the fear that their financial life will become an open book to the Internet universe ([Yousafzai et al., 2003](https://www.sciencedirect.com/science/article/abs/pii/S0166497203001305)). 

Giving users more control over their data can mitigate privacy concerns. This includes allowing users to easily access, modify, and delete their personal information, as well as providing options to opt out of data-sharing practices​​. Users' privacy concerns are a major obstacle to the widespread adoption of electronic banking. Addressing these concerns through robust privacy policies, user empowerment measures mentioned above, and advanced security measures is essential for building trust and encouraging the use of digital banking services (Yousafzai et al., 2003)

### Security

Perceived security in electronic banking refers to customers' perceptions of the degree of protection against threats such as data destruction, disclosure, modification, denial of service, fraud, waste, and abuse. Customers are concerned about threats that occur during data transmission over networks and unauthorized access to their accounts leading to potential financial loss (Yousafzai et al., 2003).

Financial institutions have adopted several technologies to secure networks and transactions. Their substantial cybersecurity efforts by facilitating encrypted transactions, installing firewalls, utilizing authentication mechanisms, and ensuring privacy seals and disclosures have an effect on institution-based trust ([Benassi, 1999](https://dl.acm.org/doi/pdf/10.1145/293411.293461); [Bhimani, 1996](https://dl.acm.org/doi/abs/10.1145/228503.228509)).  Fintech organizations need to continually enhance security measures by investing in and updating their security protocols to protect against emerging threats. 

The financial data security protocols influence customer’s confidence and trust when adopting fintech services. According to [Furnell & Karweni (1999)](https://doi.org/10.1108/10662249910297778), consumers with a greater awareness of security will be more likely to use e-commerce and fintech services. Clear communication about the security measures in place can help alleviate customer security concerns. Displaying security signs in web content also enhanced trust ([Seckler et al., 2015](https://doi.org/10.1016/j.chb.2014.11.064)). Demonstrating commitment to protecting customer information can also build trust (Yousafzai et al., 2003).

### Transparency

Transparency involves making the invisible workings of a system accessible to the user in a manner that improves their understandability. When a system exhibits transparency in design users find it easy to understand how their data is being collected, used, and shared. Subsequently, it enhances trust.

Compliance with laws such as the General Data Protection Regulation (GDPR), the California Consumer Privacy Act (CCPA), and the California Online Privacy Protection Act (CalOPPA), require businesses to have a privacy policy. Privacy policy and terms of service agreement documents provide how user data is collected, stored, and used--including if it is shared with other parties ([Kotut & McCrickard, 2022](https://doi.org/10.1145/3492842)). The privacy policy and terms agreement provide some degree of transparency in often lengthy legal text, yet accessibility of content requires further improvement such as layered privacy policies and abridged terms and conditions ([Privacy Patterns Project, 2016](https://privacypatterns.org/patterns/))

In e-commerce, successful online stores have typically crafted useful and usable shopping experiences and earned a brand reputation. The checkout process, payment system, delivery, and even the return process are clearly laid out on their websites. The user experience includes easy access to help centers and online agents for customers to find answers to their questions. While brand recognition is a key determinant of trust in e-commerce, transparency can improve trustworthiness, particularly in new and emerging businesses that have yet to establish their brand presence.

### Agency

Agency over user data in Fintech and e-commerce refers to the control and autonomy that users have over their data within these platforms. It encompasses the rights and mechanisms that allow users to understand, manage, and decide how their data is collected, used, shared, and stored (Wang et al., 2019).

GDPR transformed Europeans’ data rights and business practices across the globe. The GDPR compliance efforts of e-commerce and fintech companies have provided everyone meaningful agency over their data. Customers have control over data about their online behavior, tracking cookies settings, and legal protections over their data rights ([Aridor, 2020](https://www.nber.org/system/files/working_papers/w26900/revisions/w26900.rev0.pdf?sy=900.)). 

According to Wang et al. (2019), giving users the ability to update and delete their data is crucial for maintaining user trust and ensuring compliance with privacy regulations in e-commerce.​

Customers have voiced their concerns over their data privacy (Bestavros, 2000; Bhimani, 1996; Furnell & Karweni, 1999) and a desire for more controls instead of ticking off a checkbox in terms of services document once. A greater degree of control can lead to a greater degree of trust ([Herder, 2020](https://doi.org/10.1145/3386392.3399557)).

### Usability

Usability is a quality attribute that assesses how easy user interfaces are to use.  Nielsen  ([2012](https://www.nngroup.com/articles/usability-101-introduction-to-usability/)) elaborated usability with its 5 quality components as follows: 

1. **Learnability:** How easy is it for users to accomplish basic tasks the first time they encounter the design?

2. **Efficiency:** Once users have learned the design, how quickly can they perform tasks?

3. **Memorability:** When users return to the design after a period of not using it, how easily can they reestablish proficiency?

4. **Errors:** How many errors do users make, how severe are these errors, and how easily can they recover from the errors?

5. **Satisfaction:** How pleasant is it to use the design?

In the competitive e-commerce sector, a higher degree of usability improves the conversion rate. Users often perceive aesthetically pleasing design as design that’s more usable ([Moran, 2017](https://www.nngroup.com/articles/aesthetic-usability-effect/)). Fintech adoption is significantly influenced by the ease of use and social influence in addition to the perceived usefulness ([Singh et al., 2020](https://doi.org/10.1108/MD-09-2019-1318)). 

### Prototypicality

Webpage prototypicality is a property of web page design that describes how closely a webpage resembles the abstract webpage that the user envisions when thinking of a specific category of web pages, such as the webpages of commercial banks or higher education institutions. The shared visual features establish the prototype – the most prototypical webpage – for a particular web category or domain ([Miniukovich & Figl, 2023](https://doi.org/10.1016/j.ijhcs.2023.103103)).

Research in cognitive sciences has demonstrated that prototypicality is measurable and influential, impacting the feeling of familiarity and liking for everyday objects (Whitfield, 1983; Monin, 2003). Miniukovich’s study found a strong correlation between prototypicality and web page visual aesthetics, perceived pre-use usability, and trustworthiness. Furthermore, the direct effect of prototypicality on trustworthiness outweighed the indirect effects through aesthetics and usability (Miniukovich & Figl, 2023).

*Figure 5: Expected effects of webpage prototypicality, figure from (Miniukovich & Figl, 2023)*

# 

# 3. Collabathon Convener Reflections

In this section, we provide a summary of a set of three selected convener reflection exercises. These activities were conducted among the convening team to identify common and divergent thinking around trust and Ag Tech. They allowed us to deliberate and reflect on our initial assumptions and biases, update our shared mental model based on the lessons learned throughout the collabathon, and come to consensus on how to move forward.

## 3.1 Exploring our own Data Stores to Conceptualize Trust

In design research, *Cultural Probes *are typically used for building empathy and connection with users to understand their context. This is a generative method that is used for exploratory research, which is just another way of saying, to find inspiration and connect with people. They are most valuable for reflective design *(**[Interaction Design Foundation, 2024](https://www.interaction-design.org/literature/topics/cultural-probes)**, **[Boehner et al., 2007](https://dl.acm.org/doi/10.1145/1240624.1240789)**)*. We created a “show and tell” style reflection activity, a variation on a cultural probe, for each of us, the convening team, to [share our data stores](https://docs.google.com/document/d/1EjbnOnti5koH50LLw5YO5sNIJ4UsqxHDbXKe8hpZIzE/edit#heading=h.5qj000g2n2wd). **We sought to explore and learn how we identify, manage, and think about the importance of objects that contain information, whether physical or digital. ** The goals of this activity were (1) to create a shared understanding of how we conceptualize value, importance, trust, sharing, information, security, privacy, and related concepts, and (2) to recognize our own way of managing important data in our own environments and reflect on our individual perspectives on trust and data.

We conducted the reflection activity on our own time and shared findings during a group meeting. The activity works as follows:* begin with identifying an existing “data store”, that is a physical or digital space which holds information of importance. After reflecting on how you define your data store, the context in which it is located, and other high-level information, you “open” your data store. This may involve opening a digital folder, a physical wallet, or artifact to explore its contents, structure, organization, elicited emotions, and reflections on trust. In the last step, one of the contents within the data store is more deeply explored to map forms of data, feelings related to security, privacy, trust, and connectedness with other items, people, places, or entities. Finally, the person reflects on ways to improve their existing data store. *An example “show and tell” workspace demonstrating a data store is shown below.

![Enter image alt description](Images/8CK_Image_1.jpeg)

*Figure 5: Data Stores of Data Stores, Ankita’s Backpack*

![Enter image alt description](Images/Nnb_Image_2.png)

*Figure 6: Data Stores of Data Stores, Tibet’s Evernote*

Our reflections focused on objects that contain information. Our collective lessons on trust include:

- **Importance is contextual: **objects are of varying levels of importance, where importance varies based on context: time, place, our individual emotional state, social interactions, and environment. 

- **Trust protocols are contextual: **For instance, how we secure an object, if we keep it out in the open or keep it private, or who is allowed access to it, all depend on context: where we are, what we are doing, who we are with. 

- **Risk assessment influences trust protocols**: For instance, we use different types of security measures for objects of different levels of importance, duration and frequency of exposure to threats, and ease of accessibility by people we do and do not trust. 

- **Private by default:** Even when we value transparency or public engagement, ** **

- **Secure “enough”: **Depending on what data we are trying to secure, there is a moving target with respect to how much effort we put into securing it. It is often based on our internal conception of what is “good enough” given varying threat models and risk factors.

- **Fear, vulnerability and exposure: **digitization also increases risk in some ways, including the scale of potential exposure.

- **Collaboration: **important for one person to “run point”, and important to give everyone that needs access appropriate levels of control.

- **Potential for use: **I can use it in the future, but someone else could too. Potential for AI.

- **Minimalism vs maximalism:** how to *really *decide what is important.

## 3.3 Asynchronous Convener Reflections re: Antecedents for Trust

As another preliminary exercise in developing a shared understanding, we imagined/considered some of the conditions and situations where we believe trust may be built and experienced. For example, trust may exist between people or parties because of personal relationships, long-time interactions, or reputation. Or, when an organization or individual shows up consistently over time, taking actions that are consistent with commitments or promises, a pattern is formed that builds trust. Transparency and understanding can also increase trust, especially when proven over time that actions match words and expectations. Furthermore, having more agency in a relationship enables more trust to grow, since you know you can always change the parameters of the dynamic. In cases where there has been an action that violates or decreases trust, if there is an ability for the individual/organization to recover from failure by taking appropriate actions, such as compensation or undoing the action, trust may be recovered and repaired. We also recognized that there is a difference between trusting intent and trusting capability. There may be situations where there is trust in the process or in the relationship, but the security practices of an organization may be insufficient to realize actual protection of data that is shared.

We also considered how trust might be realized through technologies, for example, by increasing the capability of an individual or organization to meet certain expectations or commitments - around data security. Technologies can aid in transparency, by publishing information and making it accessible, as well as automating or facilitating an individuals’ agency. Technologies may help hold parties accountable, for example, in highly visible ways on social media, or facilitate informed decision-making by connecting people to further information and resources.

We also imagined ways in which technology may decrease trust. When technology feels confusing, or how it functions cannot be easily understood, or when the technology does not do what the user expects it to do, trust can decrease. Technologies can also scale bad behavior, such as data scraping or spreading false information. Though technology makes us more connected, it can also function to create distance between individuals by mediating relationships.


# 4. Perspectives from Producers & Technical Assistant Providers

| Writing Meta (remove when done) |
|---|
| Source Document(s): , https://miro.com/app/board/uXjVKy5NGpI=/
Who’s running point on this section: Eliza
Who’s contributing to this section: Eliza, Katy, Clare, Anna |

During the collabathon, we organized a set of one-on-one interviews with producers and technical assistance providers who are a part of the OpenTEAM network. We identified these groups as key stakeholders and users in developing the concept of an Ag Data Wallet. We designed a semi-structured and informal interview process to gather information about data collection, management, and storage processes, as well as challenges that they have dealt with as users of current technologies.

## 4.1 Goals

From the preliminary landscape and literature review (see section 2), we learned that there is a lack of shared understanding around how to define trust in the context of an Ag Data Wallet. We also identified gaps in general knowledge around how producers and TAPs (Technical Assistance Providers) interact with technologies and manage data/information, which we know that these communities are engaged in. 

We saw value in engaging a small number of subject matter experts during the collabathon who have experiential expertise in operating a farm, or work for an organization that is providing technical assistance to producers to help inform our design outputs. Our primary goal was to ask producers and TAPs about their attitudes and experiences around collecting, managing, and sharing farm data and using technology in agricultural applications. 

Goals:

- Engage potential users of technology products for a preliminary understanding of attitudes and challenges around data and technology

- Gain a better understanding of how producers and TAPs share data

- Gather information from key stakeholders in a trusted one-on-one setting to inform the ADW UI/UX design catalog

## 4.2 Interview Process

OpenTEAM reached out to a network of producers and TAPs to recruit interviewees. Recruiting was through the OpenTEAM community, as well as through personal connections with farms in New England and California. We reached out to research farms, non-profit farms, and farms that had engaged in work with OpenTEAM in the past. We also listed a call for interviewees in six farmer-facing publications and email newsletters. We used emails and phone calls to contact individuals. 

Over the course of 3 weeks, OpenTEAM’s Farm Network Coordinator conducted 13 one-hour interviews. Interviewees were given the option to have their interview recorded or not recorded, with the understanding that recordings would only be used by the convening team for synthesis and that quotes from the interviews would be shared with interviewees for their approval prior to use in any collabathon community meetings. 

In all interviews, the interviewer took notes during the conversation, and reorganized the notes by topic after the session. The notes were used as a primary document for interview synthesis, and any recordings that were saved were used as an additional source for synthesis.

To synthesize interviews, a team of six people made up of OpenTEAM staff and the convening team read through the interview notes and highlighted key takeaways from each interview in a collaborative document. From this document, we looked across all interviews and identified common topics that were discussed in a table, pulling quotes from the interviews as needed and linking discussions that were related. We used this synthesis process to capture key anecdotes and stories from related experiences across the interviews.

On August 15, a community meeting was held to discuss what we learned from these interviews, and to identify challenges and opportunities related to the design of an Ag Data Wallet. 

## 4.3 Who We Talked To

### Producers 

The group of producers we interviewed represented a range of operations, from small fruit and vegetable farms to large-scale diversified crop and livestock farms. Farm sizes ranged from 4.5 acres to 500 acres, with varying degrees of employee involvement, from no full-time workers to operations employing up to 200 people. Four producers were located in the Northeast, two in the Southeast, and two in California.

Most of the farms participate in diverse markets, including CSA (Community Supported Agriculture), local retailers, and wholesale distribution. Several also operate on-farm stores, while two farms engage in national-level wholesale and door-to-door shipping.

Farm operations included livestock, meat production, row crops, orchards, vegetables, fruits, cut flowers, trees, and other specialty crops. Livestock is a significant operation in many of these farms, with a few specializing in grass-fed and mixed livestock systems, while others balance crops with animal agriculture.

### Technical Assistance Providers

The group of Technical Assistance Providers (TAPs) represent a wide range of technical skills and experience levels, working with different farmer populations on various agricultural challenges. TAPs ranged in years of experience from 2 years to 13 years, working with producers on issues of climate resilience, farm viability, crop disease and pests, and conservation planning. Some worked closely with a few producers for multiple years, while others worked with over 100 new producers each year. All of the TAPs interviewed served the Northeast. TAPs were affiliated with governmental, nonprofit, and academic institutions

## 4.4 Biases and Limitations

- Interviews were recruited for and conducted mid-June to early July, limiting the ability for some producers to participate

- All producers and TAPs were recruited through the OpenTEAM community or via OpenTEAM staff's connections

- Small set of 8 producers and 5 TAPs

- Limited geographic diversity

- Synthesis of the interviews is filtered through our biases

We recognize that we recruited during a very busy time of the year for producers, as well as keeping our initial recruitment to the common community. As a result, there are some limitations to the generality of the findings, and we recognize that our own biases may be represented in the synthesis and in this report. We caution the reader against taking these observations as general conclusions at this time: we are continuing this work in the community and we are planning to widen the farmer/producer engagement as a next step.

## 4.5 Producers: Learnings

**Farm Data**

We spoke with producers about the types of data they collect and manage. The types of data we discussed can be grouped into two primary categories, with seven subcategories of data:

- Farm Management Data

- Farm Events: Animal birth and slaughter dates, pasture movements, tillage and field preparation events, cropping events, yield and harvest data

- Animal Health Data: FAMACHA score (anemia test), animal weights, medications, breeding and lineage information, medical histories

- Soil Data: Soil amendments, soil tests, bulk density, tillage and field preparation events, cover cropping, height and types of grasses and forage

- Orchard Data: Maintenance and pruning, fruit Brix scores, freeze days, sap analysis

- Farm Enterprise Data

- Financial Data: enterprise-related expenses/revenue, sales at various outlets, farm expenses, employee data (how many employees are doing a task, how long it takes them, hours they are working, etc.)

- Certification & Reporting Data: carbon monitoring, food safety data, certification protocols

- Research Data: phenology testing, wildlife tracking, soil health metrics

**Data Collection & Data Management**

*Tools: *When we asked producers about the technology tools they use to collect data, 23 tools were mentioned. Most of the producers we spoke to are collecting some data on “paper” before moving it to a technology tool, regardless of their tech tool usage. “Paper” includes physical sheets of paper, as well as iphone “notes” and whiteboard usage. All interviewed producers reported that they collect certain data on paper first prior to digitizing them (if they digitized that type of data at all), regardless of the number of digital tools they used. Most of the producers were using multiple tools to collect data (see table 4-1).

|  |
|---|
| Table 4-1 |

All but one of the producers utilized Google Sheets for some type of data management and collection. However, many also noted challenges in managing data in Microsoft Excel and Google Sheets. Successful adoption of data collection tools by farmworkers can impact farmer’s ability to choose tools. If a tool is too burdensome for a farmworker to use, data collection can be inaccurate or data can be lost.

*Certification**: *We learned that certain data collection is driven by the demands of external certification processes and that collecting and managing data is time intensive for producers. Though, it was also reported that data entry required for certifiers can provide value back to the farmer/producer. Some examples of certifications that the interviewees have are: USDA Organic, REAL Organic Project, State-run Organic Certification (Ex. Maine Organic Farmers and Gardeners, California Certified Organic), Animal Welfare Approved, Food Safety Certified (Global GAP), and others.

*Data Management:* We learned that similar types of data may exist in different places (e.g. in a business plan, with a certifier, with NRCS), and that it can be challenging to manage data in various tools and forms. Some of the considerations for managing and collecting data included: information access, simplicity of data entry, and ability to share within the team. 

*Research Data: *The research data collection methods were specific to the studies happening at the farms, and differed across studies. Sometimes producers will collect data for research, and other times researchers themselves are collecting data on the farm. 

**Data Sharing**

Producers discussed sharing data with several different groups:

- Certifiers

- Government (e.g. soil and water districts, …)

- Universities/Researchers, for research studies

- Other producers

- Vets

- Apprentices or for educational purposes

- Tech developers

- Social Media

*Openness to Data Sharing*: Several producers stated that they are open to sharing data, generally, though with some consistent exceptions around financial and price data. Producers expressed that data sharing can help with systemic understanding of our food systems. And, the producers we spoke with are sharing their data openly, through shared documents or websites, in order to help other producers, for learning and education towards new farmers and apprentices. “There’s a way to share it, and it’s in a public paper, that everybody has access to. It levels the playing field, and it’s not this one-on-one proprietary thing.”

*Mutual Benefit: *Some producers said that they would be more comfortable sharing data when there was a mutually beneficial agreement. For example, one farmer/producer told us that they openly share data with people who have vested interest in their farm, such as vets. Some producers expressed that data sharing with each other helps everyone understand the system and helps others with their farming techniques.

*Risks around Data Sharing: *The producers we spoke to did have concerns about data misuse. For example, they were concerned about potential exposure of proprietary farm information to competitors. Or, having limited control over how shared data might be analyzed or interpreted. 

*Hesitation to Data Sharing: *While producers were very open to sharing, some said that they don't share raw data. Other types of data that producers were hesitant to share were: financial data, data about payroll or that felt personal with respect to their workers. One producer shared that they would be nervous if asked to share a list of all their market and sales outlets. Some producers described concerns about sharing data that would be used for commercial purposes (that are not just commercial producers - other commercial entities). For example: “We are more cautious and calculated about what we want to share.” (When it’s not another commercial farmer - but other commercial entities). “They aren’t going to get a pile of data and we get nothing.” “They give us a widget… and after a year, they get 7 figures and an exit. I wouldn’t feel like that was an equitable exchange.” There are significant trade-offs around sharing financial information and producers indicated this as an example when they would be less likely to share data. One farmer/producer said the benefits (borrowing power for loans) were great enough that they have gone 'all in' on financial records.

## 4.6 Technical Assistance Providers (TAPs): Learnings

**Farm Data**

In speaking with these TAPs, there was a wide variation in the amount and type of data they collected. The type of data collected depended on their service area and the data that the farmer had available or that the TAP could collect themselves. Some types of data that were mentioned were soil/ecological data, pest and disease data, farm yields, farm viability data (ex. financials), and farm practice data. TAPs collect this data in order to achieve their organizational mission of assisting producers in their service area, or for research or government purposes.

**Data Collection & Data Management**

Most TAPs collect data via surveys, farm records, and on-site visits. Depending on the type of data the TAP is trying to collect, they will either collect it on their own or look to the farmer to provide it. Producers may provide data verbally, through farm records on paper or digitally, or via email. Some of the TAPs mentioned that they often will collect the data they need themselves because the farmer does not have it or it is too burdensome to ask the farmer to collect. TAPs may collect this data and enter/submit it on the producers behalf. For example, a TAP looking for data on the bulk density of farm soil could collect the soil sample, mail it to the lab, receive the results, and then share those with the farmer. Some of the mandated tools that producers must use for various programs can be burdensome or challenging to use, so TAPs may also help producers input data into these systems, or the TAP may input data on the farmer’s behalf.

A notable difference between the ways that producers and TAPs collect data is that TAPs are often using tools they did not choose. These may have been mandated by the organization they are employed by (ex. CRM systems, Microsoft or Google Workspace) or are required by the programs they are entering data into (ex. WebGrants, COMET). Many of the TAPs we spoke to expressed frustrations working with mandated tools themselves, and challenges in asking producers to use these tools as well. The table below visually demonstrates the range of tools that TAPs use both internally and externally.

|  |
|---|
| Table 4-2 |

**Data Sharing**

The TAPs we spoke with shared farm data for programmatic, research, or funding purposes. TAPs may share farm data internally for organizational improvement or with research partners. TAPs also share data in order to benefit the producers they are working with. For example, sharing farm data to help producers obtain funding, helping them plan for long-term farm viability, and giving pest/disease/soil health advice or guidance. TAPs share data by entering the data collected by them or the farmer into the tech tools that are mandated by their organization or the program. Some examples of these are WebGrants, Apricot, Salesforce, SurveyStack, and ESRI Field Maps. Before TAPs share data, they will obtain permission from the producer to share it with external entities. All of the TAPs interviewed also anonymized or aggregated all data before it was released.

**Impact of**** Trust**** on Data Sharing**

TAPs indicated that their data sharing process with producers is dependent on building trust. Some of the ways that TAPs build trust is by focusing on relationship building prior to asking producers for their more personal data. TAPs must be aware of their organization’s reputation with certain producers and how that may impact a producer’s willingness to share. Clearly explaining how data will be used and ensuring confidentiality is a critical part of this process. One TAP described a process of working with a farmer’s peers to help them feel more comfortable sharing data with the TAP.

Another insight from the TAP conversations was the importance of farm data benefiting producers. This was evident from the data collection stage to data sharing. If a farmer is being asked for data that is burdensome to collect, they could be less likely to cooperate with a TAP in the future. TAPs may emphasize the benefit of data collection and sharing with the producer, such as personalized technical support or more accurate benchmarking. TAPs will share back insights learned from the farmer’s data in cases where the data is being shared externally.

Some TAPs we spoke with also follow privacy protection and data security protocols. Most of these practices were self-created and self-managed by the TAP. One TAP organization had the practice of deleting salesforce instances and emails after a project is completed. One TAP mentioned not sharing data with a third party unless explicitly asked by the producer, ensuring that the producer is the one ultimately submitting or sharing the data externally. Some TAPs encrypt and password protect files or delete all farm data from their computer once the project is completed. 

## 4.7 Challenges Related to Technical Tools

### What We Heard From Producers:

- Complexity of tools

- Challenges in finding the perfect tool and the need for easy, streamlined systems means many producers end up using spreadsheets

- Human error, language barriers, and literacy can impact farm workers ability to use a tech tool successfully

- Some producers and farm workers find managing data on Microsoft Excel/Google Sheets challenging (even though most producers we spoke with do use spreadsheets).

- Lack of interoperability between systems.

- Similar types of data may exist in different places (business plan, with a certifier, with NRCS) Comprehensive or complete information may be stored across several different places which are not linked together. For example, land use information about a farm may have geographic information stored in NRCS, other information about each plot of land in a business plan, and even further information about each plot of land with a certifier.

- Data collection for certifiers can be time intensive and detailed. It is burdensome to find data in different places to enter into the certification form year after year.

- Concerns about data security and use by technical tools.

- Some datasets are more sensitive than others (such as financial data) and producers want to make sure of the benefits of sharing outweigh potential risks

- User agreements can be long and difficult to understand

### What We Heard From TAPs:

- Lack of interoperability between the systems/tools that they are mandated to use.

- Have to enter similar data into different tools to meet program requirements and there is no way to share data between them

- Wish it were easier for data to flow across various tools: CRMs, Google Workspace, and others

- TAP’s organization and the (target) program requirements determine the tech tools they use, these tools are not always intuitive/easy to use

- Hard to offer fast and effective solutions: When working with producers, technical issues with incompatible systems slow TAPs' work.

- Some farm records are not available for the TAP to provide effective assistance

- E.g. some producers do not keep detailed records of farm activities or farm data

- Privacy concerns around farmer’s data

- Can be unclear who has access to farmer's information within a TAP organization

- Want ways to increase data security/cybersecurity

- e.g. locking document, revoke access after staffing change

- Data access permissions are not clear in data management/sharing tools

### What we heard during the community review meeting:

On August 15, 2024 the convening team held a community review meeting to share out our findings from the interviews and get community feedback. We had 36 attendees, including interviewees, producers and TAPs we did not interview, technologists, OpenTEAM community members, etc. In this community meeting we reviewed findings around data collection, management, and sharing, as well as challenges that we heard. We then asked the community to share challenges and insights of their own, and discussed the issue of interoperability. The challenges shared in the community meeting are as follows:

**Producers:**

- Producers are hesitant to download new farm data tech that may not provide value to them

- Lack of trust that tech companies can keep farmer data secure

- Challenges surrounding Spanish-speaking farmworkers

- Potential solution: Spanish language voice to text and translator for farm tasks and observations

- Staff turnover leading to poor data collection/management

- E.g. a team member setting up a technical tool that works great, but when that team member leaves, no one else knows how to use/access

- Excel/Google Sheets basic knowledge is a barrier to effective use and basic training is a gap

- This is sometimes addressed at continued education events

- Similar types of data existing in different places - data is stored in different ways: handwritten, PDF reports from labs, on different stakeholders' portals, on producers different tech tools

- The more diversified the farm is, the harder it can be to keep data in one spot

**TAPs:**

- Outputs from proprietary tech tools can be in the incorrect format for TAP use

- Lacking an easy way to share GIS/maps with notes across platforms

- Photo use is an important way for TAPs to get information, there is not an easy way to share photos as a form of notes and link them to other data

- Lack of historical data collection by producers can make it hard for TAPs to give accurate advice/assistance

**Non Farmer/TAP challenges**:

- Many tools only work if enough other people are using them, incomplete data often isn't useful

- Some tools are expensive and the investment in learning how to use them could be too much of a tradeoff. It appears this is especially true for ESRI or other spatial tools

- Alignment between regulatory data collection and voluntary programs would support efficiency for producers and TAPs

## Section 4 Appendices

### 4A. What We Asked Producers

We developed an interview script that asked for information about the farmer/producer, the farm they currently are affiliated with, and some details about the farm, as well as including two primary topics of the interview: farm data and technology tools, and data sharing. The interviewer followed the script below, while also allowing for informal conversations on other topics that arose during the interview.

- Introduction

- OpenTEAM intro

- Purpose of these interviews

- Our goals with this collaboration

- About You and Your Farm

- Tell us about your farm:

- How long have you been farming?

- What type of operation do you have?

- What size is your operation (acres)?

- USDA defines small family farms as making “less than $350,000 gross cash farm income,” does your farm fit in this category?

- Do you have employees? If so, how many?

- What are your markets?

- Defining “Farm Data” and tools

- What types of information do you collect on your farm?

- Who collects this info?

- What do you use this information for?

- When you hear ‘Farm data’ what do you think of?

- [Prompt] What are some examples?

- What types of tools do you use on your farm to collect data (examples could include, recording on paper, Excel, Google Drive, QuickBooks, Pasture Map, AgriWebb, Farm OS, Tend, etc.)

- What are the software/tech tool names?

- Do you use these tools through a mobile app, desktop computer, or on paper?

- Do you receive assistance in using these tools from others? Technical Assistance Provider? Family member? Employees?

- What are your goals in using these tools?

- What do you like or dislike about the tools you currently use?

- Are there tools you use in the field vs. in the office?

- What are they?

- What tools do you use to manage data? (if they are different than what you use to collect)

- Can you recall a time when you felt frustrated or confused while using a data management tool for your farm? What caused these challenges?

- Have you ever encountered situations where you felt your farm data was being used in ways you didn't fully understand or agree with? Describe the situation(s). How did you address this?

- How did you decide on your current farm data management system and tools?

- Did you try out other options or tools that did not work for you?

- Why did you like or dislike them?

- Data Sharing

- For what purposes / benefits do you share your farm data?

- What are some pain points or challenges when sharing this data?

- What factors influence your decision to grant external parties access to your farm data? (ex. obligated to share contractually, trust, promised benefits, data security, hassle/overhead of sharing, etc)

- Who do you feel most comfortable sharing data with?

- What particular data do you most worry about sharing? Worry least?

- What (if any) measures do you take to ensure that your farm data remains secure and confidential when sharing it with external entities?

- [Optional?] Do you verify the credibility and trustworthiness of third-party tools or agencies before sharing your farm data with them? How? (provide an example of a third party if needed)

- How could these organizations increase your trust and willingness to share?

- What features or functionalities in your digital tools would make it easier for you to navigate and understand who has access to your farm data / for what purpose?

- Closing

- Do you have any questions for us?

- We plan to share our findings from this work towards the end of July. We will have an open session on Zoom that you are welcome to attend…

- Thank you for your time.

### 4B. What We Asked TAPs

We developed an interview script that asked for information about the technical assistance provider’s experience and relationships with producers, as well as including two primary topics of the interview: farm data and technology tools, and data sharing. The interviewer followed the script below, while also allowing for informal conversations on other topics that arose during the interview.

- Intro

- OpenTEAM intro

- Purpose of these interviews

- Our Goals with this collaboration

- About You/Farm/Org

- Tell us about your work as a Technical Assistance Provider: how long have you been working in this field? What type of services do you provide to farmers?

- How many farmers do you work with/serve per year?

- What are the average years of working relationships you have with your farmers?  How frequently do you interact with them?

- Defining “Farm Data” and data categories

- What do you think of as “Farm Data”?

- What types of data do you collect in your role? (ex. I collect sales data from farmers to help them increase their farm viability)

- Who collects this?

- If the answer is farmer: How is the farmer providing you with the data? (ex. on paper, USB drive, email, permissions through a tech tool?)

- If the answer is the TAP/oneself: why?

- What types of data management tools do you use when working with farmers (examples could include: Excel, Google Drive, QuickBooks, Farm OS, Tend, etc.)

- Do you use these tools through a mobile app, desktop, or on paper?

- What are your goals in using this method of data management?

- What do you like/dislike about the tools you currently use?

- How did you decide on this current farm data management system and tools?

- Did you choose these tools or did someone else at your organization?  Or were they mandated by some third party?

- Did you try out other options or tools that did not work for you?

- Why did you like or dislike them?

- Data Sharing

- For what purposes/benefits do farmers and ranchers share data with you?

- What are some pain points or challenges when requesting access to farm data?

- Do you receive pushback or concern when requesting specific types of data from farmers and ranchers?

- How do you go about addressing concerns from farmers and ranchers when requesting what could be considered sensitive information?

- What (if any) measures do you take to ensure that farm data that you have access to remains secure and confidential when sharing it with external entities?

- [Optional] Do you verify the credibility and trustworthiness of third-party tools or agencies before sharing data with them? How? (provide an example of a third party if needed)

- What features or functionalities would make it easier for you to navigate and understand who has access to farm data / for what purpose within the tools that you use?

- [Optional] Can you recall a time when a farmer or rancher you work with felt frustrated or confused while using a data management tool for their farm? What caused these challenges?

- Were you able to support them to address these frustrations?

- What challenges did you run into when trying to support this farmer or rancher?

- [Optional] Do you feel that the farmers and ranchers you work with understand how their farm data is being used?

- Closing

- Do you have any questions for us?

- The next step is …

- Thank you for your time.

# 

# 5. Perspectives from Ag Technologists

| Writing Meta (remove when done) |
|---|
| Source Document(s): [insert your links]
 
Who’s running point on this section: Tibet
Who’s contributing to this section: Steve, Tibet |

In parallel with our interviews of farmers and TAPs, we also carried out interviews with designers and builders of existing Ag Tech platforms and tools. In this section we describe our process for, and the learnings from, interviewing these Ag Tech providers. **The goal of these conversations was to better understand their varied approaches and attitudes towards:**

- Ag Data privacy, trust, and consent.

- Data sharing between tech tools. Current practices and desired future state.

- UI/UX learnings and best practices from their own work in Ag Tech and in managing Ag Data.

- The concept of a data wallet as a central experience for data stewardship and data sharing.

- Potential implications of a data wallet for their tech solution.

- Potential implications of this research on improving the user interface (UI) and user experience (UX) of existing Ag Tech.

## 5.1 Surveying the landscape

We started by mapping a wide range of organizations and projects working on Ag Tech, with a focus on finding a diversity of approaches to the management and use of Ag Data.** **This included large commercial farm management tools, alongside projects most likely to adopt farmer-friendly practices around trust, privacy and data management, open-source, and pro-social solutions. This resulted in a database with four main types of resource:

1. **Organizations** working on Ag Data related programs and projects. Some of these organizations produce their own software tools as well. 

2. **Software tools** and platforms that use or interact with Ag Data. 

3. **Information sources** like data standards and data use principles.

4. **Events**,** **such as conferences, where these topics are discussed. 

**Organizations****: **In our review of organizations that are thinking about or working on issues related to Ag Data use, we found a number of existing coalitions and alliances focused on improving practices around the handling of Ag Data. This includes organizations like the [Open Ag Data Alliance (OADA)](https://openag.io/), [Ag Data Coalition](https://agdatacoalition.org/), [Ag Gateway](https://aggateway.org/GetConnected/ADAPT(inter-operability).aspx), and the [Data Food Consortium](https://www.datafoodconsortium.org/en/). Some of these organizations, for example the Ag Data Coalition, are focused primarily on **education** around the value and use of Ag Data. Others are focused on influencing **policy** makers, such as the [Global Open Data For Agriculture and Nutrition (GODAN)](https://www.godan.info/). And others are focused on **interoperability** and data standards and schemas. 

**Software tools and platforms**: 

- **Farm management** tools, like commercial leader [John Deere Operations Center](https://www.deere.com/en/technology-products/precision-ag-technology/data-management/operations-center/), as well as open source platforms like [FarmOS](https://farmos.org/). 

- Tools that **consume farm data** to provide value to farmers, for example [Regen Farmers Mutual](https://www.regenfarmersmutual.com/) which helps farmers understand the value of their ecological assets and find strategies to capture that value. 

- **Back-end systems/frameworks** built to enable or support data sovereignty, e.g. [Solid](https://solidproject.org/), and Web3 solutions like [Kepler](https://www.keplr.app/) and [Holochain](https://www.holochain.org/). We did not prioritize interviews in this area, and the more technical aspects of data sovereignty, as this collabathon is focused on UI/UX. 

- Existing **digital wallets** like [MetaMask](https://metamask.io/), and the [Farm Worker Wallet OS](https://www.farmworkerwalletos.community/), from which we looked to understand UI/UX best practices.

**Information Sources****: **Our review also includes a compilation of other related resources, such as:

- Projects working on data management **best practices and principles** within tech, like [FAIR](https://www.go-fair.org/fair-principles/), [CARE](https://www.gida-global.org/care) and [TRUST](https://www.nature.com/articles/s41597-020-0486-7).   

- **Standards and schemas** related to agriculture data, such as [ISO](https://www.iso.org/home.html), [The Australian Farm Data Code](https://nff.org.au/programs/australian-farm-data-code/), [ADAPT](https://aggateway.org/GetConnected/ADAPT(inter-operability).aspx), and [AgroPortal](https://agroportal.lirmm.fr/)

- Standards and **libraries** for defining data schemas such as [JSON Schema](https://json-schema.org/), [Schema.](https://schema.org/docs/schemas.html)org and [Murmurations](https://murmurations.network/)

- Standards related to **data sovereignty** such as [Decentralized Identifiers](https://en.wikipedia.org/wiki/Decentralized_identifier) and [Verifiable Credentials](https://en.wikipedia.org/wiki/Verifiable_credentials). 

### 5.1.1 Gathering process

We gathered our resources in an Airtable, categorized by the four Resource types listed above. Additional fields included website URL, description, screenshots, estimated relevance to the ADW UI/UX Collabathon, and tags, to indicate things like whether the resource is focused on *education* or *interoperability*, whether it is *commercial* or *open source*, etc. For each of these categories we combined extensive web based research to collect the resources, alongside outreach to the wide networks connected to the convening team. We also held a community meeting to present our initial database, and gather additional resources from a range of additional people and organizations in the Ag space using a collaborative Miro board. 

### 5.1.2 Initial Assessment of the landscape

A few things stood out from an initial analysis of the resource library.

**Data sources and Data consumers: **We found some distinction between projects that primarily *generate *Ag Data, such as farm management tools, and ones that primarily *consume *Ag Data to offer services to farmers, such as COMET-planner and Open Food Network. There can be quite different ways of approaching trust, privacy and data management between these two types of projects. Of course some tools are clearly in both categories. 

**Standards & Interoperability: **There are a significant number of projects working on standards, schemas, and protocols to enable greater interoperability across Ag Tech. This includes projects with government connections like the The Australian Farm Data Code, coalitions like [Open Ag Data Alliance (OADA)](https://openag.io/), academic institutions like [OATS Center at Purdue](https://oatscenter.org/), and toolkits like [AgGateway’s ADAPT](https://adaptframework.org/). Much of this work also builds on general data standards work happening in the tech world such as JSON Schema and verifiable credentials 

**Education & Policy: **Many of the organizations we explored focus not on the development of technology but instead on educating producers or policy makers around Ag Tech, specifically around data use policies, data management, privacy, security, etc. This includes organizations like the Ag Data Coalition, Global Open Data For Agriculture and Nutrition (GODAN), and [MyData](https://mydata.org/). 

**Government involvement: **As the Agriculture space is of deep importance to societal wellbeing, there seems to be an outsized influence by government agencies in data management practices related to Ag Data and Ag Tech. There is a lot of government regulation in the space, as well as numerous government programs and subsidies that farmers interact with. 

**Impact of Web3: **Much of the conversation and experimentation happening around data sovereignty is in the web3/blockchain space, where numerous digital wallet projects already exist. This includes sovereign identity standards like Decentralized Identifiers (which are not solely a web3 technology), organizations like the Open Wallet Foundation and Startin’ Blox, data/app architectures like Solid and Holochain, and wallets like MetaMask. The Ag Tech space is only just beginning to explore what web3 has to offer, and we believe it is right to be cautious, as the user experience of interacting with blockchain and other web3 technologies is generally sub-par and not easily accessible to people with less digital literacy or time to learn new technologies. However, projects like the [Farm Workers Wallet OS](https://tac.openwallet.foundation/projects/fwos/) are showing that there are use cases for which web3 based decentralized storage and identity solutions can benefit producers. 

## 5.2 Interview process

From the above database we made an initial assessment of the relevance of each organization and tool and started outreach to schedule interviews with leaders of those projects. We conducted numerous interviews using a standard set of questions, as well as free form discussion. These conversations are recorded. At the end of each interview we asked for additional projects to include in our research.

### 5.2.1 Who we talked to

- Ag Tech researchers

- Ag Tech industry associations

- Ag Tech services/infrastructure providers

- Software providers for producer operations / farm management (via direct conversations, and in some cases users of the software)

- Software providers for farm/ranch worker welfare

- Software providers for certification processes

- Software providers for TAP/TSP/extension support

- Software providers for value added services

- Ag Data service providers

- A mix of Designers, Developers, Product Managers, and Executives

### 5.2.2 Biases and limitations

We held 17 interviews with 24 people. A number of groups we reached out to didn't respond.

Steve and Tibet were the primary interviewers, and did most of the synthesis and analysis of the interviews as well, so all of this is filtered through their biases.

### 5.2.3 Assumptions

Going into our conversations we had a few assumptions that we wanted to check:

- **Open source** projects are more likely to engender trust. We put particular focus on finding platforms and tools that share their code openly as that means the community can see exactly what the code is doing and call out anti-patterns like privacy or security issues. We also assumed that teams that care about open source are inherently more likely to care about best practices around privacy, security and data management in general. This relates to the Transparency principle described above. 

- **Non-profit** organizations/projects are more likely to follow best practices around data management than commercial platforms. We made sure to interview both commercial and non-profit organizations to check this assumption.

- That conversations around Ag Data standards and policies have not included producers' needs and perspectives at the forefront. 

### 5.2.4 What we asked

We used a standard list of questions, one for Ag Tech providers, and another for researchers and other leaders in the space. We also adapted our questions for each specific organization. Here is the standard list we started with for Ag Tech providers.

- **Tell us about your tool:**

- General overview and summary of what it does and how it works

- What is “farm data” in your tool?

- Who is using your tool?

- How many people/producers are using your tool?

- How do you market / bring in new users?

- **Data Lifecycle**

- Data Input

- How do users currently input data into the tool

- Does your tool receive farm data from other places?

- How do you integrate with these other tools/services?

- Data Storage

- How is data stored in your tool?

- Is it encrypted?

- Data Sharing & Use

- Who has access to what data in/from your tool?

- Does your tool send farm data to other places?

- What for?

- How do you integrate with these other tools/services?

- Does the data get sold or rented at all?

- Do you use the data stored in your tool in any way internally?

- What is your data sharing/use consent process?

- What is the process for farmers to revoke access or delete their data from your tool? How could this process be improved?

- **Terms of Service**

- What terms are users required to agree to regarding how your tool uses their data? How clear / concise are they?

- Do you have a privacy policy? What does it cover?

- Do you use cookies or other data tracking tools? How do they interact with Ag Data?

- **Design Influences**

- What factors have influenced your decision around the tech stack and technical architecture of your tool?

- How do you make UI/UX decisions for your tool?

- What factors have influenced how you have designed the data management UX in your tool?

- What have you seen in the world that you learn(ed) from?

- What do you like?

- What don’t you like?

- How do you get user feedback on your tool?

- Do you solicit feedback and input on your data practices?

- **Vision for Trust in Ag Tech**

- What do you see as critical to improving producer trust in technology?

- We’ve been using the phrase “Ag Data Wallet” as a stand-in for the notion of improving human-to-Ag Tech trust.... What do you think about this concept?

- What alternative language might you use to describe this idea?

- What would your ideal ADW do, in relation to your tool?

- **Screenshots: **We also gathered screenshots from some of the most relevant tools, to document and understand UI/UX approaches to data management across the ecosystem. 

## 5.3 Learnings

In our conversations, most organizations were very open and willing to share about their goals and their work, and interested in our approach. Here we have synthesized and summarized our general learnings from across the interviews, with some specific examples highlighted. **Note**: these learnings are phrased as insights about producers, but they are actually *the opinions of our interviewees, who were mostly ****not**** producers themselves.*

### 5.3.1 Trust & Privacy

Most everyone we talked to interfaced with producers in the design and marketing of their tool. They had many general observations about working with them.

Where does trust come from?

- **Earning producers' trust is hard but essential.**

- Building trust requires time, transparency, and aligned interests

- Working through trusted partners provides inherited trust

- Technical service and assistance providers are often the primary trusted partner they work with/through.

- Other farmers recommendations also hold a lot of weight

- Farmer owned/governed organizations/projects are particularly trusted

- Producers might be willing to share data in exchange for knowledge or recommendations of value to them.

- Producers are also more likely to share their data for financial incentives, The negative side of this is that money can influence their judgment.

Privacy Concerns

- Producers have a lot of concerns that influence their trust of Ag Tech:

- Their data might be used against them, in a lawsuit for example.

- Their data could be stolen somehow, and/or their competition could see it.

- They could be taken advantage of. This could happen through hidden loopholes, and/or by someone making money off their data,

- Agreements could be broken or trust violated. This could happen by mistake, or by dishonesty.

- For-profit businesses are considered generally less trustworthy.

- One main issue is that an acquisition could mean that a once trustworthy organization no longer is.

- They can be skeptical of institutions with differing politics

- E.g. universities or the government

- Working with government bodies means that a FOIA request could result in their data becoming public without their consent

- It’s not likely that someone doesn't want to share data on principle. Most often, hesitations around data-sharing arise from practical concerns  for producers.

- “I don’t want to share data” can be a proxy for…

- “I don’t want to deal with you”

- “I don’t have time to think about this”. e.g. Burdensome requests to capture data

- "What do I get out of it?"  i.e. Perceived cost/benefit analysis

- Power differentials make it so value has to be big to outweigh risk of data being used against you

- The digitization process is not capturing the things I care about

- "The platform you're using isn't the one I use for my farm management, and I don't have time for a new platform."

**Key Learning: ****All this suggests that independent non-profits are often the most trustworthy to producers. Especially if there are ****farmers as core stakeholders.****.**

*Open question: “How relevant are emerging codes of conduct in facilitating trust? Has commitment to a code of conduct affected your willingness (or ability) to do business?”*

One answer was: “they are useful for establishing shared values, less useful for actual dependable enforcement”

### 5.3.2 Data Input 

- Data input is difficult and time-consuming. It can take 100s of hours.

- There are many different data sources:

- In producer’s heads

- On paper

- General digital documents or spreadsheets

- Other tools/platforms

- Never collected / unknown

- Automating data import from other sources can be messy and cause issues, so data is often collected many times, for each tool. Some providers think it’s too complex to integrate data from other sources - "Better off starting fresh"

- **API connections to import data **can help, and is something many folks want. But it is hard to prioritize for most providers; they usually don't have the resources to build it.

- **A standalone Ag Data Wallet could help simplify and standardize data input.**

*Open question/issue: Data ownership is complex, who owns the data? The entity who owns the ground? The equipment? The guy who paid for the service? Clear a*greements are important here.

### 5.3.3 Storage

- Users don’t want to host data themselves.

- There was some debate about the benefits of on-device data vs. cloud data. On-device is more secure/private, but the cloud is more convenient/reliable. Some producers prefer one or the other more.

- All data should be **exportable & deletable.**

- Some felt that tools/services should maintain the ability to export all original "raw" data out of a system if someone decides to stop using it.

- But in this case they should make sure people know that data is always changed, losing its context and some of its value, when it is exported.

- Good security practices are generally expected, but important only to some producers. Often it becomes an issue only if there’s a breach.

- Some people care about encryption, others don’t. The ones who do are usually more technical folks, or those who have experienced privacy violations before.

- Laws/policies and **government standards** are very relevant to how data is stored, and vary by locality.

- Some producers care deeply about their info not having a chance of being revealed, so tools might **consider things like** **never entering farm metadata into a digital system, and location obfuscation.**

*Open Question*: Is *web3 (blockchains) valuable in this space? *The answer was not entirely clear to us, but it seemed less important in the short term. 

### 5.3.4 Sharing Data

- **Consent Management is at the core**. After trust, comes specific, informed consent - both parties should clearly understand the deal.

- **What **specific data is being collected or provided?

- To/**for who**?

- For what **purpose?** Does this include further **sharing **with others?

- For **how long?** (share just once, or ongoing)

- Defining what data might be connected to Personally Identifiable Information as opposed to **aggregated**, or depersonalized

- **Other conditions** / restrictions / commitments

- **Understanding the deal can be hard, **particularly because of legal jargon or obfuscating language. So a trusted advisor is often depended on to endorse a deal, for better or worse. When there is more trust in a platform/tool/org, this reduces the (perceived) need for diligence.

- **How to help people understand?**

- Humans in the loop

- Real world example use cases that are relevant to producers

- Prompt for specific scopes of data as those scopes become important.

- Before data is shared, it may need to be proofed/QA’d. Consider the UX for this.

- “Set and forget” can be helpful: once a producer makes a choice, they don’t want to think about it all the time,

- Also important to **remind people of their choices and make it easy to change them**.

- Standards could create the necessary infrastructure to support efficient and effective sharing

- A standalone ADW could be valuable by enabling farmers to only have to input data once, and giving providers a standard way to import data from many sources. Also could offer secure data storage and simplified sharing dashboard.

- **Groups (e.g. coops) add another layer of support and complexity**

- They allow for delegated deal understanding.

- They provide/require collective deal negotiation

- Members might be bound by collective decisions, or have individual opt-out rights

- **Selling data: **In most cases, just don't do it. 

- **Producers not all opposed to selling data, only opposed to not getting a cut**

- There are good use cases for selling aggregated, anonymous data.

### 5.3.5 UI/UX & Design Patterns

Alongside these general learnings we synthesized some best practices from existing tools and providers.

- **Ease of Use: **Getting the User Experience right is important for true informed consent and privacy. e.g. GDPR notifications usually don't work or help, people get annoyed by them

- Things have to be **easy** because:

- Farmers have no time

- They don’t always have internet access

- They may have low digital literacy

- Complexity reinforces mistrust, simplicity implies transparency

- UX should be **invisible** (not in the way) until it’s needed, and then **obvious**

- Use lots of help text - explain everything that is going on.

- **Mobile centric** design with offline access is often a good idea because data input from the field, with low or no connection can be important. Cold/rain/gloves all impact ability to navigate a touch screen. Using voice in creative ways or extra large buttons are two examples of ways to approach this.

- Data input and data analysis are two very different things and need different UI/UX. Input may happen in the field using a mobile app, but analysis more often at a computer.

- **Familiarity:**

- The **vocabulary** should be “farmer words” not “techie words”

- **Consistency**: use consistent labeling (terms) and ordering to maximize understanding. Things with the same functionality should be similar, e.g. form layouts, button appearance, etc.

- Use a *map-centric UI* for activities - show where they occurred

- Use a *timeline UI* for relating activities over time, e.g. field or animal history

- Lots of farms use spreadsheets and Airtable, which are super flexible. How do we play the **edge between simplicity and flexibility**?

- *But we also heard “spreadsheets do not match the way farmers think”*

- If you want to collect data from producers, try to **use a tool they’re already familiar with**. e.g. in India this is text messaging with Telegram

- If you want researchers to understand your UX, look to things like Google Drive, DropBox, etc. for cues

- **Use “roles” to help define “rights”**, e.g. what roles have the power to share which data

- Different types of people often have to access/use Ag Tech, especially TAPs

- Grouping people together and controlling sharing via group membership and group privileges eases management

- Roles and responsibilities/rights can also be assigned to API connections to other systems

- Good UX patterns help **informed consent**:

- Clarify the bargain: What is negotiable? What can the user opt in and out of?

- **Once data is shared, it’s hard to control it or take it back**, make sure people know that.

- Present a **reminder of obligations** when accessing data

- An **audit trail** to track who accessed what data, when, can be valuable.

- What happens when someone is fired or quits? Remove their access, but not their data

- **Use of AI**? Some are bullish on it; it could provide hyper relevant Ag advisory. It can integrate multiple data sources and deliver on the promise of an Ag extension.

- Lots of folks using Retrieval Augmented Generation. Curating content and receiving shared, vetted content in the correct format is difficult. Can explore a lean RAG model.

- “I think AI should only be used for 'mundane' data processing and management tasks and nothing else because it can introduce even more privacy and agency concerns that we are already facing.”

### 5.3.6 Focus on TAPs

In many cases **most work, transactions, and relationships around ag data happen through Technical Assistance Providers. **The UI/UX for TAPs can and should be different from the UI/UX for producers, because they are generally more technically experienced. Also because anyone who spends a lot of time in a tool starts to prefer efficiency over ease-of- use, if a tradeoff must be made. Finally, they are more likely to understand privacy and data issues, so they provide a layer of protection, both for farmers, and for the services. 

### 5.3.7 Tech Stacks

While not the focus of this collabathon, we did spend some time asking about the tech stacks used by various tools, and the ways that the tech infrastructure can enable or support better data management.

- **There is a big role**** of government here**: eAuth, FedRAMP, India state protocols, and even government-run servers can have a big impact on which services producers interact with, in what ways. 

- **Authentication**** and authorization**: **most people are using oAuth right now**. Scopes provide granularity around permissions and a built-in consent model that people understand.

- **Revocation is hard**. There is no standard way to remove consent, as revocation does not explicitly exist in oAuth though providers can build interfaces for it. 

- **Simple formats: **Lots of folks provide ways to import/export data using simple standard formats, like PDF and CSV.

- **Web3**: Not much adoption in Ag Tech yet. The UX of interacting with web3 is still too complex and confusing for most folks. Some providers are exploring the use of [Solid](https://solidproject.org/about) for sovereign data stores. Others are considering [Decentralized Identifiers](https://www.w3.org/TR/did-core/) for auth and [Verifiable Credentials](https://www.w3.org/TR/vc-data-model-2.0/) verification.

- **A successful Ag Data wallet likely requires agreed upon standards, protocols, & schemas for data connection and sharing**

## 5.4 Summary and the implications for an ADW 

Ag Data remains very **siloed and disconnected** in various software tools/platforms for producers and TAPs. However, it has **substantial latent value**, and it seems worth our time to try to facilitate increased trust, informed consent, and more data sharing across the ecosystem. This could give producers more stake in their Ag Data, and the ability to gain more value from it. 

There’s a real **risk of power imbalance** between producers and Ag Tech providers, leading to implicit coercion. An Ag Data Wallet would need to offer at least the same value as other service providers, alongside more data sovereignty. Or perhaps it can offer more value, with features like data portability and interoperability.

Many folks see the value in supporting & promoting more consistent and functional user experiences for data management across Ag Tech. **A consistent, high quality data management UX** would: 

- Allow producers to move data between Ag Tech tools with greater confidence.

- Increase understanding of choices and implications when sharing data, **reducing implicit coercion.**

- Reduce time spent in data management decisions and processes.

UI/UX best practices from this Collabathon could be valuable for both existing Ag Tech tools which incorporate data sharing capabilities, and a potential standalone Ag Data Wallet which could hold producer data and manage data sharing in a variety of use cases.

Conceptually, a standalone Ag Data Wallet could increase producer sovereignty and increase the likelihood of data sharing, for the benefit of all.

Ag Tech providers are** excited about the idea of receiving data input from a standalone wallet**, but less interested in delivering data to such a wallet. An Ag Data Wallet could provide them with instant integrations with many other platforms, but it was less clear what value they would get from exporting to it. 

## 5.5 Unanswered Questions

- Data ownership is complex: Who owns the data? The entity who owns the ground? The equipment? The guy who paid for the service? Clear agreements are important here.

- It can be tricky to even identify who is the person with control/permission to provide data e.g. rented land with a custom operator coordinated by a farmer. Who all has to sign off on providing access to data

- How relevant are emerging codes of conduct in facilitating trust?

- How does this project relate to the USDA? They have an outsized impact on all of this in the USA. If they suggest a standard they want to use then that takes a bunch of variables out

## 5.6 Challenges for a standalone Ag Data Wallet

- Some providers are not keen on sending their data to other Ag Data repositories, because managing data is their business model

- We are unlikely to get big companies like John Deere and Climate on board with an Ag Data Wallet, unless maybe we provide a compelling use of new tech for them?

- Trying to build standard software components for existing tools may not be very useful, because there are too many different tech stacks.


# 6. UX Design Patterns

| Writing Meta (remove when done) |
|---|
| Source Document(s): 
Who’s running point on this section: Hana
Who’s contributing to this section: Anna, Hana, Clare |

## 6.1. OVERVIEW

UX patterns are reusable solutions to common design challenges. They offer generalized frameworks that can be implemented as is, or customized to improve user experiences in digital products. In the agricultural technology (Ag Tech) context, these patterns provide practical recommendations to address recurring usability issues encountered by product teams.

The Ag Data Wallet (ADW) UI/UX Collabathon developed the Ag Tech UX Pattern Catalog to guide anyone tackling usability challenges or building new digital solutions for the agricultural sector. For this initial release, we’ve selected five key patterns that we believe will be most valuable to our primary audience: Ag Tech implementers. We envision this as a format that OpenTEAM’s community can iterate on, adding patterns as we discover or encounter them in our research and development efforts.

### 6.1.1. How to use this

- Nonlinear Navigation: Feel free to explore topics in any order. The guide is not meant to be read sequentially.

- User-Centered Design Reference: The selected UX patterns are based on widely accepted UX design principles but may need to be adapted to specific user contexts

### 6.1.2. Goals & audience

- Primary: communicate design recommendations to **Ag Tech implementers,** such as software engineers, product managers, and UI/UX designers.

- Secondary: communicate software best practices to Ag Tech end-users, such as farm managers and technical assistance providers.

### 6.1.3. Background (ADW UI/UX Collabathon)

The ADW UI/UX Collabathon operates on the belief that a human-centered, consistent approach to tech design can build trust between Ag Tech users, developers, and stakeholders. Drawing from research in sectors like e-commerce and financial technology, we identified key themes that influence technology adoption: ***privacy, security, transparency, agency, usability, usefulness, and familiarity.***

This initial Ag Tech UX catalog was informed by user research and interviews, where farmers and technical assistance providers emphasized the need for transparency and user agency throughout the data lifecycle. The patterns aim to guide the development of the ADW UX and serve as a resource for other Ag Tech implementers.

### 6.1.4. Terms

| Terms  | DEFINITIONS  |
|---|---|
| UX pattern | UX patterns, or UX design patterns, are reusable solutions to common usability problems encountered in user experience and user interface design. They serve as universal building blocks that help product teams address recurring design challenges and create a sense of familiarity for users navigating digital products or web pages. |
| Users | Refers to end-users of technical tools. E.g. farmers, TAPs, etc. |
| Data/Resource or online resources | Refers to data, dataset, or other digital resource (webpage, code, etc) |
| Data/Resource owner | A person who created, collected, and owned farm data. E.g. farmers who collected farm data for their operation.   |
| Farm data manager | A farm staff/worker who is responsible for managing data about any aspect of a farm or ranch. |

### 6.1.5. Design Pattern Presentation

Each design pattern consists of the following elements:

- Title: an illustrative name.

- Problem Statement: a summary of the common elements of the problem found across multiple Ag Tech implementations.

- Use Cases: scenarios and use cases in the agricultural technology context.

- Description: overview of the design pattern which can address the identified problem.

- Examples: screenshots from existing tech tools which implement the design pattern.

- Themes: corresponding UX themes from the collabathon’s landscape review report.

## 6.2. UX PATTERNS:


	[](#)
	[Make connected services explicit](#makeconnectedservicesexplicit)
	[Provide user-friendly data access controls](#provideuserfriendlydataaccesscontrols)
	[Manage roles and permissions](#managerolesandpermissions)
	[Export resources](#exportresources)
	[Convert resource format to meet the destination’s requirements](#convertresourceformattomeetthedestinationsrequirements)

### 6.2.1. Make connected services explicit

#### Problem Statement

Applications often make it challenging for users to track and monitor who has access to their data and how their data is flowing across a set of connected software applications. This is further complicated as terms of service evolve without users’ explicit awareness or engaged consent.

The phrase “connected services” refers to the paradigm of “integrations”, where software developers build connections among tools to enable data sharing, shared authentication, and other shared services across a suite of apps. These types of integrations can improve interoperability among software, or reduce the number of accounts a user needs to access multiple software systems. However, when implemented silently, that is, without explicit user interface components to communicate connections among services, this paradigm can also confuse users concerning which software has access to which data.

This challenge is made more difficult by the lack of a clear, centralized view within the account settings of each connected service. Users often struggle to identify which services are accessing their data through APIs (automated data connections), and the information is often scattered or buried in complex interfaces. Without an intuitive way to view and manage these connections, it is difficult to monitor, control, or revoke access to a user’s data. As a result, users remain unaware of how their data is shared, often leaving them without the power to manage or consent to ongoing data use by third parties.

##### Ag tech use cases 

- **Farmers** who adopt multiple farm data management tech tools can streamline their operations and insight gathering by setting them to share data via APIs.

- **Technical assistance providers (TAPs)** use multiple tech tools for day-to-day operations and client management. A data flow between their Customer Relationship Management (CRM) software and other related tech tools can make them more effective and transparent.

#### Description

Displaying all connected services (via API or other means) in a dedicated section of the settings interface enables users to easily monitor and manage their data-sharing activities. By providing clear details about which services have access to their data and what specific information is being shared, users gain transparency and control over their data.

Additionally, offering users the ability to revoke or modify data access directly from this list simplifies data-sharing permission management, enhancing security and boosting user empowerment.

#### Examples

1. **John Deere Operations Center**

Users can view the list of connected services (via API) from Access UI at a glance. They can view connection details by opening the item from the list.

[John Deere Operations Center - API access list in Access page]

![Enter image alt description](Images/LLt_Image_3.png)

[John Deere Operations Center- API Connection details revealed]

![Enter image alt description](Images/d8R_Image_4.png)

2. **Google Account Data & Privacy**

Users can view the list of connected services at a glance.

[Google’s connected third-party apps & services/apps list]

![Enter image alt description](Images/u89_Image_5.png)

[Google’s connected third-party apps - The connected service details view shows how it works and what data the third party can access.]

![Enter image alt description](Images/AAY_Image_6.png)

[Google’s connected third-party apps - Additional details about access and the way to revoke action]

![Enter image alt description](Images/aM4_Image_7.png)

#### Themes

* Transparency  **,  **Privacy **,    **Agency*


### 6.2.2. Provide user-friendly data access controls

#### Problem Statement

Data owners should have a simple and direct way to grant trusted individuals access to their data or resources. Requiring authentication for new platforms or sending access requests through unfamiliar channels can create barriers for recipients, making it harder to grant or receive access.

##### Ag tech use cases

Sometimes a technical assistant provider (TAP) collects farm data on behalf of a farmer. The TAP can create a private access link to a spreadsheet containing the farm data and share it with the farmer. This allows the farmer to easily verify the data without needing to navigate complex tools or authentication processes.

#### Description

Granting access to one's private data or resources can be managed in several ways. A common UX pattern involves generating a private access link (also known as an unguessable URL) and sharing it with trusted individuals who have permission to view or use the resource. This method allows for quick and convenient access without requiring formal authentication, making it simple and user-friendly. The link can be shared through existing communication channels like email or messaging apps, minimizing friction for both the data owner and recipient.

To mitigate the risk of unintended sharing, the data or resource owner can modify permissions at any time. Additional security measures, such as setting expiration dates for the link or requiring a password for access, can further enhance control and protect the resource.

#### Examples

1. **Dropbox’s private link-sharing setting**

Resource owners can create a public and/or private link to share it with others. Dropbox also offers access time constraints and password protection features.

![Enter image alt description](Images/0sC_Image_8.png)

1. **Google Sheets**

Users can grant access to their online spreadsheet from the share setting dialog and copy the link.

![Enter image alt description](Images/WcV_Image_9.png)

#### Themes

*  **Agency **,  **Security*


### 6.2.3. Manage roles and permissions

#### Problem Statement

Data owners require a structured approach to collaborate with various stakeholders, each with distinct roles and responsibilities in managing and sharing data. A data management system that provides clearly defined roles and permissions enables more effective collaboration, ensuring that tasks are carried out efficiently while preventing unauthorized actions or overstepping of boundaries. This clarity fosters trust and smoother workflows, allowing everyone to focus on their specific responsibilities within the system.

##### Ag tech use cases

Data owners can assign different roles to farm workers for data collection and management within a centralized system. By assigning specific roles, the manager can prevent unauthorized data modification while enabling broader collaboration. For instance, a farm viability adviser (a type of technical assistance provider) can be given Viewer or Commenter access to review financial data and provide tailored advice without altering the data itself.

#### Description

When sharing a file or resource with others, the data owner can assign specific roles that define how users can interact with the shared content. Common roles include:

- Viewer: Can only view the file without making any changes.

- Commenter: Can view the file, suggest changes, and add comments but cannot edit the content.

- Editor: Has full editing rights, allowing changes to be made.

- Owner: The original creator or owner, with the ability to manage access, change roles, and control permissions for other users.

The level of role granularity varies depending on the product or platform and may have context-specific role names. Data owners can revoke access or adjust roles at any time, ensuring flexibility in managing collaborations. Access and roles are typically assigned by entering the recipient’s email and selecting their permissions. Once assigned, the individual receives a notification and can begin fulfilling their designated responsibilities.

#### Examples

1. **Google Sheets  - File Share**

Data owners can share and govern data with assigned roles in Google Workspace. Limiting the edit privilege can allow the owner to share the data more broadly when needed.

![Enter image alt description](Images/3F5_Image_10.png)

**B. Terraso Story Maps**

The owner of a story map can invite others to collaborate on their story with the editor role. Invited collaborators accept the role and can add chapters, text, photos, and videos to the story. But they cannot delete the story map itself.

![Enter image alt description](Images/7Xz_Image_11.png)

#### Themes

* Transparency  **,  **Privacy **,    **Agency*


### 6.2.4. Export resources

#### Problem Statement

Data owners often need to export their data from one SaaS (Software as a Service) tool to another to meet their specific needs. When this process is difficult or unavailable, users can feel trapped within the tool, diminishing their sense of control and flexibility over their own data.

##### Ag tech use cases

Farm collaborators collect data from the field using mobile devices and the farm data manager exports the data for further processing and analysis to gain insights.

A farm data manager exports a subset of the dataset and forwards it to external stakeholders, such as financial institutions or government agencies, for reporting or compliance purposes.

#### Description

The ability to export or download one’s own data is a common user expectation. This functionality allows users to process their data using different tools, enabling greater flexibility in how they manage and analyze it. Downloading data also provides users with a local copy, which is useful for offline work or backup purposes.

**Supported file formats**

The file formats supported for export depend on the tool's context and the type of data involved. Users typically expect to export tabular data like spreadsheets as CSV files and images as JPEG files, at a minimum. Offering multiple file format options enhances the user experience by accommodating diverse use cases and technical needs.

**Permission to export/download**

For data or resources shared by multiple collaborators, the data owner or administrator should have the ability to control export/download permissions based on user roles. This ensures proper data governance and security while enabling collaborative work.

#### Examples

1. **Google Maps**

Users can export custom geospatial data (points, polygons, etc.) as KML/KMZ or CSV.

![Enter image alt description](Images/EFo_Image_12.png)

1. **Google Sheets**

Users have 6 options to export the spreadsheet in Google Sheets. Google apps don’t differentiate between export and download functionalities.

![Enter image alt description](Images/ZyW_Image_13.png)

1. **Microsoft 365 Excel**

** **Users can export their Excel spreadsheet as CSV or PDF.  

![Enter image alt description](Images/kwm_Image_14.png)

1. **Miro: Frame**

Users can export the selected frame as PDF, JPG image, or CSV file from the virtual whiteboard SaaS tool.

![Enter image alt description](Images/KwK_Image_15.png)

#### Themes

* Transparency  **,  **Privacy **,    **Agency*

### 6.2.5. Convert resource format to meet the destination’s requirements

#### Problem Statement

When sending data or files to other software, users often face the cumbersome task of manually verifying and adjusting the file format to match the requirements.

Mismatched file formats often result in data transfer failures, causing delays and additional work for users.

##### Ag tech use cases

Farmers need a simple way to forward their data to external parties, such as certifiers, in the correct format for each recipient. Automating the packaging and formatting of data for repeated submissions significantly reduces the effort involved in applying for certifications and ensures successful transfers without manual intervention.

#### Description

When sending data to external entities, ensuring it is in the correct format is essential for successful transfers. Users need a way to package and send their data in the appropriate format for third-party requirements. By automatically setting the outgoing data format to match the destination's specifications, the system removes the need for users to manually adjust settings for recurring or ongoing transfers. This automation simplifies the process, reduces errors, and enhances efficiency, allowing users to dedicate less time to data formatting.

#### Examples

1. **Apple Photo**

"Automatic" image file format option is designed to choose the best file format when transferring or sharing photos, based on the compatibility and capabilities of the receiving device or platform.

[Apple Photo - Picture format can be determined by the destination ]

![Enter image alt description](Images/pij_Image_16.png)

**B. TurboTax**

TurboTax stores entered personal, financial, and tax information in a proprietary format (e.g. “.tax2023”), but as befits its intended purpose, it includes the ability to generate a PDF file that formats the tax information and associated calculations in a form acceptable to federal and state tax authorities (File -> Save to PDF…). It also provides the ability to deliver tax information to many tax authorities electronically, sometimes for an extra fee (File -> Electronic Filing -> File Electronically). Lastly, it provides the ability to generate a special file for sending to product help agents which keeps the original file structure but anonymizes all personal information (Online -> Send Tax File to Agent).

NOTE: Although TurboTax makes data sharing explicit, and has a privacy policy that appropriately limits their use of your personal data, it’s worth noting that TurboTax does *not* provide user data export in a portable or standard form, *deliberately* attempting to lock users into a pattern of annual fees and purchases. Indeed, it has been [reported ](https://www.propublica.org/article/inside-turbotax-20-year-fight-to-stop-americans-from-filing-their-taxes-for-free)that the company has actively lobbied against tax simplification which would remove the need for its product entirely.

[TurboTax - Saving proprietary format to PDF to match the tax authorities' specifications]

![Enter image alt description](Images/1ls_Image_17.png)

[TurboTax - Filing tax electronically from stored tax information]

![Enter image alt description](Images/wY0_Image_18.png)

[TurboTax - Generating a special file to send agents]

![Enter image alt description](Images/k0N_Image_19.png)

#### Themes

* Transparency  **,    **Agency*


# 7. ADW Design Recommendations

## Suggested User Interface Elements

This section presents a series of suggested user interface (UI) components designed to address the use cases that emerged from the interviews and community sessions with agricultural producers and technical assistance providers (discussed in part 4), focusing on their specific needs and challenges in managing and accessing agricultural data. Selected UI elements are accompanied by wireframes that visualize the solutions. By grounding design recommendations in real-world input, we aim to create a tool that fosters more effective and streamlined data management for agricultural stakeholders.

### Data Input UI

This element addresses how agricultural data enters the Ag Data Wallet. Based on the interviews and sessions with producers and TAPs, a useful ADW would be able to take in data directly, import and parse a spreadsheet, or connect to another ag tech tool and import data.

Functionality might include:

- Simple direct data entry via voice recording, text entry, image capture, or location capture with accessible buttons/controls

- Voice to text functionality

- Spanish translation (and other relevant languages)

- Offline mode to allow operation and store data on device until there is an internet connection

- Ability to categorize and sort these data points into standard formats or existing datasets

- Spreadsheet import:

- Import templates that allow producers to record data in a common format that aligns with a shared schema

- Manual import column mapping to align an existing spreadsheet with the shared schema

- AI-powered or algorithmic data parsing to find patterns in the data and map it to a shared schema

- A library of services that can be connected through an authorization flow to search for other ag tech tools, authenticate in a tool, and import specific datasets from that tool.

- Ability to turn on ongoing syncing of data to keep the ADW up to date as data changes in the source

![Enter image alt description](Images/ocG_Image_20.png)

**Figure 7.1 Data Input UI example**

#### Use Cases

1. As a farmer/farmworker/TAP collecting data in the field, I need easier and more reliable ways to record that data, so that I can save time and collect more detailed data.

2. As a farmer primarily using spreadsheets for farm data, I want to upload my spreadsheet into a tech tool and have the data parsed and presented to me in an easy to view/search/share format, so that:

1. I can use it to make farm management decisions.

2. I can access data-based opportunities (certification, grants) without having to use an additional tech tool that I do not like, and can continue using my spreadsheet system which works well for our on-farm data collection.

3. As a farmer using multiple tech tools to store/analyze different types of farm data (PastureMap and FarmOS for example), I would like to aggregate and manage all my data in one place so that I can access opportunities by sharing this data.

### Data Management UI

Once the data has entered the ADW, producers need a way to view and manage it. The Data Management UI would show a list of datasets, with the ability to drill down into each one.

Functionality includes:

- Ability to view & search imported data

- Preview of data in each dataset

- Ability to open up and view a complete data set

- Ability to edit, add, and delete data

- Meta-data such as who imported or added the data set, date-time, and provenance

- Ability to export the data in various formats

![Enter image alt description](Images/SCu_Image_21.png)

**Figure 7.2 Data Management UI example**

#### Use Cases

- Same as above.

### Data Sharing UI

The Data Sharing interface is the place where producers and other authorized users would initiate the sharing of specific datasets with another group, or platform, such as a TAP organization, certifier, government agency, or digital farm tool. In this interaction, it is important to clearly and completely illustrate the terms of sharing with the party in question so that the producer can make an informed decision.

Specific functionality may include:

- Search bar for producers to find the party they want to share with.

- Inbound data sharing requests to approve or deny, with context, for example if a TAP wants to share the producer’s data with a new third-party.

- When a producer is searching for the party they want to share with, the potential connection should specify:

- The terms of sharing with that party, including whether sharing can later be revoked, and shared data deleted

- Individuals and/or organizations that will have access to the data

- What recipients will do with the data

- Data management practices / certifications

- How the producer will benefit

- Additional entities that will have access to it (for example, if sharing with "USDA" which specific agencies will have access?)

- Ability to select which datasets will be shared, and to see a preview of the data. The preview may be in a table format or a map format, or other formats as appropriate.

- Ideally there is an option to select a subset of a dataset to share, such as specific columns from a spreadsheet

- For each dataset, the ability to specify whether it can be shared with additional third parties

- Ability to set an expiration date after which the data will no longer be available unless permission is renewed

- Ability to set whether data is shared ongoing, or just one time

- Nice to haves for this UI:

- AI-powered summary of Terms of Use for sharing data with each service/partner that flags potentially controversial things.

- When sharing data for an application, it would be helpful for the system to identify any required data that are missing from the wallet’s datasets

- Tools for sharing data into aggregate data pools and data coops anonymously

![Enter image alt description](Images/Bas_Image_22.png)

**Figure 7.3 Data Sharing UI example**

#### Use Cases

1. As a farmer looking for technical assistance and funding opportunities, I want to search for TAPs, orgs, businesses, and govt agencies to **find the right one to share with and then be able to share my data with them easily**.

2. As a farmer deciding whether to share my data, I need to **understand the terms of the agreement (the fine print) easily**, so that I can give informed consent without having to spend too much time reading legalese.

3. As a farmer or TAP preparing to share data, I would like to **select the specific datasets to share and see a preview **of the data, and edit for quality assurance, so that I can ensure that only relevant data is shared.

4. As a farmer deciding whether to share my data,** I want clear information on who will have access and what it will be used for**, so that I can decide if it is worth the time & costs of sharing my data.

### Data Access UI

The Data Access interface expresses a list of parties and services that have been granted access to data held in the ADW. It is important for the producer to see at a glance where their data has been shared, by whom, and for what purposes, and if necessary, to revoke any access that is no longer needed.

Functionality includes:

- A list of parties with whom ag data has been shared.

1. For each one, show which dataset(s) are being shared, what are the basic terms / what the user is getting out of it, and option to revoke or request to revoke

2. An audit log showing who shared/accessed what data and when

3. Periodic reminders to check if users still want to be sharing data with the same person/service

4. For each external party using the farmer's data, provide a button to revoke permission or request to remove the data.

5. A list of previous form submissions and sharing actions, with the ability to view / export the data / report that was shared, in multiple formats such as CSV, PDF, etc

6. For situations where data has been shared widely, it may be helpful to break this list into sections based on categories or permissions (services that can update the data, vs services that can only read the data). Disconnected services could be in a collapsed section that is visually deprioritized but available when needed.

![Enter image alt description](Images/1IX_Image_23.png)
 \
**Figure 7.4 Data Access UI example**

#### Use Cases

1. As a farmer who has already shared my data, I need a** list of who has access, which datasets they have, and what they are using it for**, so that I can be aware of who is accessing my data and understand whether I want to keep sharing or stop.

2. As a farmer who has already shared my data, I would like to be able to **revoke my data** from a specific partner, so I can remove it from places that I no longer want it.

### Team Permissions UI

The final interface we recommend is a Team Permissions page where the producer holding the wallet can authorize trusted individuals to manage and share data in the wallet on their behalf.

- A place where admins can invite other users (TAPs etc) and authorize them to input, manage, or share their data

- Ability to change roles / revoke access from authorized users

- Specify which datasets each user/role has access to

#### Use Cases

1. As a farmer, I want to **authorize a user (TAP) to input data, manage data, and share data on my behalf, **so that:

1. I do not have to use a tech tool that I do not understand.

2. I do not have to spend time doing data management because I am very busy.

## System Architecture Considerations

The functionality of an ag data wallet, data storage and intermediation (data input & data output: which data, to whom), can reside in a standalone system or be incorporated into many different Ag Tech tools.  In fact, it would be a rare Ag Tech tool that provides neither data input nor data output.  Figure 7.5 shows some of the different system connections that could be supported by ADW functions.

The OpenTEAM API Switchboard project provides a way for multiple systems to interact by creating a single API connection (to the Switchboard).  If adopted, it substantially reduces the burden on any particular application (including the Ag Data Wallet) to code to a myriad of different system APIs.

Note that API connections between systems require agreement not only on the programmatic access methods, but also on the type of data to flow over the API *and the meaning of that data*.  Regardless of open and well-documented APIs, all providers benefit from data and metadata schema standardization.

Manual input and output functionality (except direct data entry) also relies on data format standardization.  Automated format identification (with conversion, if needed) can be augmented with a manual UX for mapping incoming and outgoing data to arbitrary formats.

The figure attempts to show that the Ag Data Wallet functions could be standalone or could be directly incorporated with other general purpose Ag Tech.

![Enter image alt description](Images/7li_Image_24.png)

**Figure 7.5 **** (****[image source](https://docs.google.com/presentation/d/1uKXXM1RR8VBMa5u_hA5kNkrY5VguR2_dMtBP818icHw/edit?usp=sharing)****)**


# 8. Summary Guidance & Next Steps

## 8.1 Principles

In this section, we build upon all of the above work to propose a set of principles for improving human-data interactions, with downstream effects on usability, transparency, and trust. We hope these themes are of utility to those engaged in the design and development of Ag Tech.

### **1. Be human-centered:** understand the diversity of data practices among your current and intended users. 

To truly understand the needs and challenges within Ag Tech, it's crucial to go beyond assumptions and engage directly with the individuals who use these tools daily. Farmers, agronomists, technical assistance providers, and other stakeholders each have unique perspectives and workflows that need to be considered. By actively involving users in the design process and conducting thorough research, you can gain valuable insights into their data practices, pain points, and desired outcomes. This understanding will enable you to develop solutions that are not only effective but also user-friendly and tailored to their specific needs. Remember, technology should empower users, not burden them with unnecessary complexity or irrelevant features. Furthermore, consider how your current or planned user research can contribute to our collective understanding around concrete ag data management challenges, barriers to adoption of domain-specific technologies, and the perceived value and usability of Ag Tech. This principle is a derivation of a broader first principle of human-centered design [cite Norman], but we encourage thinking specifically about agriculture as knowledge work that relies on identifying shared conceptual models among diverse user groups. 

Key questions to consider:

- How can you actively involve users in the design and development process?

- What research methods can you employ to gain a deeper understanding of their data practices?

- How can you ensure that your solutions are user-friendly and tailored to their specific needs?

*Are you able to identify critical use cases that are valuable enough to motivate increased utility and usefulness of Ag Tech?*

### **2. Speak the languages of your users: **improve the match between your technology and both natural languages and domain-specific vocabularies of your users. 

Effective communication is key to building trust and fostering understanding. In the context of Ag Tech, this means using language that is clear, concise, and accessible to all users, regardless of their background or expertise. Avoid technical jargon and complex terminology that may alienate or confuse non-technical users. Instead, strive to use plain language and familiar terms that resonate with your target audience. Additionally, be mindful of the diverse linguistic and cultural backgrounds of your users. Providing multi-lingual support and incorporating culturally relevant content can significantly enhance the user experience and promote inclusivity. Remember, technology should facilitate communication and collaboration, not create barriers due to language or cultural differences.

*Key questions to consider:*

- How can you simplify and clarify the language used in your technology?

- What steps can you take to ensure that your solutions are accessible to users with varying levels of literacy and technical expertise?

- How can you incorporate multi-lingual support and culturally relevant content?

### **3. Design for real life: **prioritize features based on the context of tech use.

People use Ag Tech in very different contexts: Farmers may track animal weights during the move between pastures. Conservation specialists may collect soil samples in the field and type the location of the sample on their phone. A certifier may sit with a farmer at a kitchen table to review the past year of records. It's essential to consider these diverse contexts when designing and developing solutions. A mobile app that works seamlessly in the field may not be suitable for desktop use, and a tool designed for large-scale operations may be overwhelming for small-scale farmers. This principle builds on situated cognition theory [cite suchman] that can be used to describe how the social, physical, and infrastructural context influences how, if at all, a particular technology is used, including in agriculture. That said, two common trends in Ag Tech use indicate the need for (a) responsive design, since people use varying devices, often older phones and tablets, and (b) design for low- or no-network connectivity via making data/or features available offline, to support usage in the fields. 3) Consider alternate user input methods such as voice for when hands are not available. a *Are you able to identify the technology and use context to determine priority features?*

*Key questions to consider:*

- *How can you ensure that your solutions are adaptable to different environments and scenarios?*

- *What features and functionalities are most important for different user groups and contexts?*

- *How can you prioritize user needs and challenges in the design process?*

### **4. Improve user agency:** provide data stakeholders with agency, visibility, and control over their data.

Producers and TAPs have expressed a clear need for greater agency and transparency in how their data is accessed and used. It's crucial to design solutions that give users clear visibility into who has access to their data, for what purpose, and under what terms. This includes providing intuitive controls for granting, revoking, and modifying data access permissions. Additionally, users should be informed about the potential implications of data sharing, including the possibility of data being copied, aggregated, or used in ways they may not have anticipated. By empowering users with knowledge and control, you can foster trust and confidence in your technology. Remember, data should be a tool for empowerment, not a source of anxiety or uncertainty.

Key questions:

- How can you provide users with clear and concise information about data access and usage?

- What tools and controls can you offer to empower users to manage their data sharing preferences?

- How can you educate users about the potential implications of data sharing, including the limitations of data deletion and revocation?

- What types of control does the user have over their data? Is it clear who owns, has access to, or is using, which data?

- In some cases, data provided cannot be retracted. Recognize, and help your users understand when sharing data whether it is irrevocable or not., that once data has been shared, it is not always possible to fully eradicate.

- Sharing in any semi-public way allows others to make copies and share further regardless of deletion of the source, or the policies of the app/platform

- Data included in aggregated data sets can rarely be disaggregated, even if the source data is eliminated

- Data used to train modern AI systems is inherently incorporated into those systems and cannot be removed

- Standard backup practices of every technology platform and technology supplier lead to a surprisingly large number of copies in unexpected places


### **5. Reduce data friction:** create seamless data collection and management workflows.

Producers and TAPs face significant challenges in efficiently collecting and managing farm data. These challenges are exacerbated by the diversity of farming practices, collaborative workflows, and the need for digitization. To improve stakeholder outcomes, Ag Tech solutions should prioritize seamless data collection and management workflows. This includes:

- Understanding on-farm data collection: It would benefit the Ag Tech community to work towards a shared community-wide understanding of the needs and challenges of on-farm or in-field data collection, as well as how digitization processes may motivate or influence the adoption (or not) of specific technologies or tools.

- Leveraging technology to reduce friction: Implement technologies such as speech-to-text, image-to-text, and language translation to simplify data entry and improve accessibility for diverse users.

- Prioritizing early-stage data lifecycle: Focus on optimizing the initial stages of data collection and management, as this is where many pain points and inefficiencies occur.

- Considering the collaborative context: Design solutions that support collaboration among farmers, farmworkers, and TAPs, enabling seamless data sharing and communication.

By addressing these key areas, AgTech solutions can significantly reduce data friction, empowering producers and TAPs to collect, manage, and utilize farm data more effectively. This will ultimately lead to improved decision-making, enhanced productivity, and greater overall success in agricultural operations.

Key questions to consider:

- How can you streamline and simplify the on-farm data collection process?

- What technologies and tools can you leverage to reduce data friction and improve accessibility?

- How can you design solutions that support collaboration and data sharing among stakeholders?

- How does digitization improve stakeholder outcomes?

- Farmers and their practices are diverse, they often work in a collaborative context with people on and off the farm.

It would benefit the Ag Tech community to work towards a shared community-wide understanding of the needs and challenges of on-farm or in-field data collection, as well as how digitization processes may motivate or influence the adoption (or not) of specific technologies or tools.

Implementation strategies may consider user-data interactions that prioritize speech-to-text, image-to-text, and language translation technologies may relieve barriers to digitization.

For example, speech-to-text, image-to-text, and language translation technologies may relieve barriers to digitization. We encourage ongoing user research on early stages of the data lifecycle for a better understanding of how technology can address data collection challenges.

### **6. Don’t be (too) original: **implement consistent and familiar interaction paradigms in technology.

Producers and TAPs are often juggling multiple tools and platforms, and the cognitive load of learning new interaction paradigms for each one can be overwhelming. By prioritizing consistency and familiarity in your design, you can reduce friction and improve the overall user experience. This means using established design patterns from existing tools and platforms, standard terminology, and intuitive navigation structures whenever possible. While innovation is important, it should never come at the expense of usability. Remember, the goal is to empower users to accomplish their tasks efficiently and effectively, not to impress them with flashy but unfamiliar features. Example: A confirmation dialog to delete a data element should have the same text and button placement each time. However, if the user chooses to delete all their data, the confirmation dialog should be visually distinct, highlighting the significance of the action.

Key considerations:

- How can you leverage established design patterns and conventions to create a familiar and intuitive user experience?

- What steps can you take to ensure consistency in terminology, navigation, and visual elements across your platform?

- How can you balance innovation with usability, ensuring that new features are easy to learn and use?

### Example: A confirmation dialog to delete a data element should have the same text and button placement each time. However, if the user chooses to delete all their data, the confirmation dialog should be visually distinct, highlighting the significance of the action.

- Users do not want to learn new paradigms in order to interact with your tech.  *Occasionally* you will need to do something novel, but most design problems have been solved (better) before. 

- Repeat patterns in similar situations and break patterns in different ones. \
A confirmation dialog to delete a data element (if needed at all) should have the same text and the “yes” button in the same place each time.  If the user has elected to delete *all* their data, then the confirmation dialog should look fundamentally different, drawing the user’s attention to the fact that they may have mistakenly selected a different function.

### **7. Actively engage in standards development: **participate and/or take leadership in data standardization efforts across sectors and disciplines.

- Contribute to domain-specific data standards themselves

- Whose responsibility is it to knit together all the data standards?

There are many standards across sectors and disciplines that have the potential to improve interoperability and the overhead of Ag Data management. Invested participation from Ag Tech developers, users, and other stakeholders, is critical to ensure that such standards are designed to honor producer sovereignty, transparency, and reciprocity.

- Example: Without standards, the data requirements for certification are variable and create lots of barriers for farmer data collection collecting all that data.

- There was a real gap for people: e.g. CcCertifiers askwere asking for all kinds of data, which drives a backwards process whereby specific data is collected only for the specific needs of that certifier.  was going 'backwards' to drive data collection only for that purpose (with some recognition that it could also provide value to the farmer to have that data for themselves - but rare). It would be much easier on producerfarmers if they knew they only had to collect a standard set of data, which could be shipped out to various certifiers/reports/etc. Efforts to bring together the ecosystem of certifiers to work on aCan certifiers come together to work on a "what is needed for everyone" set of standards would be beneficial to all stakeholders.? Who is going to require that?

Examples of community-led data standards development in Ag Tech include work by [Ag Data Transparent](https://www.agdatatransparent.com/) and [Ag Gateway](https://aggateway.atlassian.net/wiki/spaces/AG/overview?mode=global), where Ag Gateway’s [MODUS Agricultural Lab Test Data Standard](https://aggateway.atlassian.net/wiki/spaces/MOD/overview) serves as an exemplar of an exemplary user-friendly and implementable standard. Increased and active collaboration of common Ag Tech standards to govern data in Ag Tech can improve software qualities such as interoperability, data portability, and application portability. However, the language and guidance around how to measure and improve such software qualities has historically been standardized by general technology organizations such as the ISO, IEC, and IEEE, e.g., ISO/IEC 19941:2017 standard provides shared terminology on cloud computing interoperability and portability. 

Example: Without standards, the data requirements for certification are variable and create lots of barriers for farmer data collection collecting all that data. Certifiers ask for all kinds of data, which drives a backwards process whereby specific data is collected only for the specific needs of that certifier. It would be much easier on producers if they knew they only had to collect a standard set of data, which could be shipped out to various certifiers. Efforts to bring together the ecosystem of certifiers to work a set of standards would be beneficial to all stakeholders.

Key questions to consider:

- How can you contribute to the Ag Tech ecosystem committing to the use of existing standards more?

- How can you contribute to the development of new standards where necessary?

Whose responsibility is it to drive development and adoption of standards?

### **8****. Make data machine-translatable: **identify and develop shared utilities for automatable data flows, from data schemas to common APIs.

The future of AgTech hinges on the ability to seamlessly exchange data between different tools and platforms. Producers and TAPs currently face significant challenges due to the lack of standardized data formats and the difficulty of manually transferring information. By actively participating in the development and adoption of shared data schemas and common APIs, AgTech developers can unlock the potential for greater interoperability and significantly reduce data friction. This will not only streamline workflows and improve efficiency but also empower users to leverage their data more effectively. Embracing standardization and open data exchange mechanisms is not just a technical consideration; it's a strategic imperative for fostering innovation, collaboration, and ultimately, a more connected and user-centric AgTech ecosystem.

Key Considerations:

- How can AgTech stakeholders collaborate to establish and promote shared data schemas and APIs?

- What are the key barriers to data interoperability in the current AgTech landscape?

- How can we ensure that data exchange mechanisms are designed with security, privacy, and user control in mind?

- In some ways, the ADW hinges on the success of efforts in the Ag Tech community to establish common data schemas so that ag data is structured consistently across tools. If shared schemas are developed and adopted by Ag Tech tools in the OpenTEAM ecosystem, overall utility and ease for producers greatly improves by unlocking the possibility of greater interoperability and interchange between tools.

- We all benefit from reducing data friction.

When developing new technology solutions, look to existing data schemas and standards.  This is an easy way to leverage community expertise.  If you find holes, participate and contribute your insights.  Although there is  easy commercial justification for importing data from other systems, exporting data in standard formats builds users' value, and reducing lock-in creates user loyalty.

- Open APIs allow anyone to add value to your system. \
If standard data is good, being able to transfer it automatically is better.  Providing secure APIs builds additional user value and supports a broader ecosystem which is beneficial for every participant.

### **9. Make policies human-readable:** improve understandability, consistency, and accessibility in data policies. 

Starting with Terms of Service, License Agreements, and Privacy Policies, the legal language which governs the relationship between tech developers and their users needs to be clear, well-organized, and must not attempt to hide undesirable deal elements or reserve unneeded rights. Ideally, we move toward shared policy structures and standardized documents, with clear rationale for deviations, to reduce time, money, and effort on behalf of all parties - technology providers AND technology users.

Key questions to consider:

- How can you simplify and clarify the language used in your policies, avoiding legal jargon and complex terminology, to ensure that your policies are accessible to users with varying levels of literacy and legal expertise?

- In what ways can you involve users in the development and review of your data policies, ensuring their voices are heard and their concerns are addressed?

- How can you contribute to industry-wide efforts to standardize data policies and promote greater transparency and consistency across the AgTech ecosystem?

### **10. Responsibly prioritize open knowledge: **consider how to communicate your use of and contribution to open knowledge, including open source software and open datasets.

Those in the OpenTEAM community are generally predisposed to creating open source technology and seeking to support and leverage open source assets.  *Even if your situation seems to prevent working in the open*, look for pieces of your technology that do not provide a critical competitive advantage to test open development within your organization.  Contribute to open source projects which provide you with critical functionality.

As you collect data with your users, consider whether that data can be anonymized and distributed safely for the benefit of broader research efforts and to advance our understanding of agricultural practices and their impact on productivity and ecosystems.

Key questions to consider:

- What aspects of your technology or data could be shared openly without compromising your competitive advantage?

- How can you contribute to existing open source projects that align with your goals and values?

- What steps can you take to anonymize and share data responsibly, while protecting user privacy and ensuring data integrity?

- How can you clearly communicate your commitment to open knowledge and the benefits it brings to the broader community?

## 8.2 Next steps

### How might we create an actionable, domain-driven, trust framework to improve ag data stakeholder outcomes?

While there are many conceptual theories around trust and how it works, there is minimal tactical guidance on how to operationalize these ideas via domain-driven design. Research literature covers best practices, at a generic or high-level, regarding privacy, security and other subcomponents of trust, but this report (a) does not provide a comprehensive synthetic review, though we provide some foundations to build on. Like many before us, we argue that here is a need for actionable guidelines on how to operationalize trust. However, we add that the need for empirically derived patterns that allow for traceability from our understanding of user perspectives to implementable design blueprints. Missing, is a coherent trust framework that allows designers and technology stakeholders to have a common conceptual model for how, for instance, improving “benevolence” can impact higher level trust outcomes, and subsequently impact the usability and usefulness of data-driven Ag Technologies.

[insert exact next step - user research study, properly stratified etc etc.]

[questions for the activity]

- How do you currently think about trust in ag tech?

- How do you learn about what your users need to be able to value, use, and ultimately trust your technology with their data?

- Are there any activities on your product/project/organization roadmap to answer these questions?

### How might we implement standardized data management practices that encompass Ag Data stakeholder perspectives on data sovereignty?

how do ag data stakeholders conceptualize data ownership, sovereignty, trust? what do these actually mean and how should the “system image” capture it. Cite norman re: conceptual modeling mismatch in design.

- MODUS gold star

[insert exact next step - inventory of data standards landscape.]

### How might we characterize diverse yet prototypical digital toolboxes among Ag Tech users?

what are prototypicial digital toolboxes and ui/ux among them? How can we use the understanding of “real” digital footprints to understand stakeholder value oriented use cases. To answer, for example, what is familiar? What does “private” look like and for whom? cite rotz, prokopy, flachs re: spreadsheets and dst uptake/barriers to adoption


[insert exact next step - data collection but better]

### How might we create a common design pattern catalog that evolves with practices?

what is an inclusive and equitable process for community-driven co-creation of a design pattern catalog? Is it enough? Cite dark/fair patterns, the gang of four’s design patterns book, and, a sampling of material-design-like catalogs

[exact next step - build out a more comprehensive design pattern catalog]

### How might we select appropriate utility technologies to implement trust-enhancing features in Ag Tech?

What are specific recommendations on where and how different technologies can fit into individual ag tech data-sharing roadmaps? E.g., there are x approaches to solving an encryption problem the one you pick is dependent on your tech stack, but the software quality you strive for should guide your selection process. Where do these types of selection processes come from? What is the role of open source, especially in context of conflation in ag with open access. Cite the software/security/sustainability paper, SWEBOK/ieee, and the data value chain paper

- Building with and through already trusted entities brings a lot of benefits. This can mean having farmers as owners or core stakeholders of the business, building with and for TAPs, and/or structuring your project as an independent non-profit instead of a commercial or government affiliated organization.

[insert exact next step - utility technology etc.]

### [closing] What does an Ag Data Wallet look like to you now?

There are a plethora of practical open-source technologies designed to support responsible data management, and there are many communities across sectors that have theorized actionable ways to implement trust-enhancing technology paradigms. One of our conclusions is that there is a need to more rigorously map the spectrum of ag data use cases to allow for the creation of tangible and actionable design patterns that could be integrated across the Ag Tech landscape. But, we do not propose *“reinventing the wheel”* as do we caution against *“building sandcastles in the sky”.* 

**open ADW implementation question vis a vis the cathedral vs bazaar**

reference to brook’s essential difficulties language to frame

- **Do we want to still want to build a standalone ADW? **a set of design patterns? or something else?

# – Deprecated - 8. PRE-Ankita: Summary Guidance & Recommendations for Ag Tech (Conclusions)

| Writing Meta (remove when done) |
|---|
|  |
| Source Document(s): previous synthesis
Who’s running point on this section: Steve. Anna + Ankita next
Who’s contributing to this section: Tibet, Steve, Clare (and everyone!) |

**~~I’m working in a draft here and will port back since I didn’t want to disrupt the existing content just yet: ~~****~~[https://docs.google.com/document/d/1C_xgQLScu2ffNbjJzUBupofAjV4HWlESK6QQaW-zOq4/edit](https://docs.google.com/document/d/1C_xgQLScu2ffNbjJzUBupofAjV4HWlESK6QQaW-zOq4/edit)~~****~~ ~~**

Although this collabathon has focused on issues surrounding the idea of an Ag Data Wallet, and particularly how a consistent user experience for data manipulation and management can improve usability, increase transparency, and enhance trust, the collabathon process also revealed some general themes of interest and utility to anyone developing technology for the agricultural sector.

## Data

- **~~Data Collection:~~****~~ ~~**~~It would benefit producers and the Ag Tech community to work towards a shared community-wide understanding of the needs and challenges of on-farm or in-field data collection, as well as how digitization processes may motivate or influence the adoption (or not) of specific technologies or tools. For example, speech-to-text, image-to-text, and language translation technologies may relieve barriers to digitization. We encourage ongoing user research on early stages of the data lifecycle for a better understanding of how technology can address data collection challenges.~~

- **~~Data Standards: ~~**~~It would benefit producers and the Ag Tech community if we could collaboratively develop a common set of standards to govern how Ag Tech companies handle ag data - for example, in ways that honor producer sovereignty, transparency, and reciprocity. ~~~~The ~~~~[Ag Data Transparent](https://www.agdatatransparent.com/)~~~~ seal is an example of community-led data standards in Ag Tech.~~

- **~~Shared Schemas: ~~**~~In some ways, the ADW hinges on the success of efforts in the Ag Tech community to establish common data schemas so that ag data is structured consistently across tools. If shared schemas are developed and adopted by Ag Tech tools in the OpenTEAM ecosystem, overall utility and ease for producers greatly improves by unlocking the possibility of greater interoperability and interchange between tools.~~

- **~~Interchange~~**~~: We all benefit from reducing data friction. ~~

~~When developing new technology solutions, look to existing data schemas and standards.  This is an easy way to leverage community expertise.  If you find holes, participate and contribute your insights.  Although there is  easy commercial justification for importing data from other systems, e~~~~xporting data in standard formats~~~~ builds ~~~~users' value, and~~~~ reducing lock-in creates user loyalty.~~

- **~~Interoperability~~**~~:~~~~ Open APIs allow anyone to add value to your system.~~~~ \
If standard data is good, being able to transfer it automatically is better.  Providing secure APIs builds additional user value and supports a broader ecosystem which is ~~~~beneficial~~~~ ~~~~for~~~~ every participant.~~

- **~~Irrevocability~~**~~: In some cases, data provided cannot be retracted. HRecognize, and help your users understand when sharing data whether it is irrevocable or not., that once data has been shared, it is not always possible to fully eradicate. ~~~~ ~~

- Sharing in any semi-public way allows others to make copies and share further regardless of deletion of the source, or the policies of the app/platform

- Data included in aggregated data sets can rarely be disaggregated, even if the source data is eliminated

- Data used to train modern AI systems is inherently incorporated into those systems and cannot be removed

- Standard backup practices of every technology platform and technology supplier lead to a surprisingly large number of copies in unexpected places

- **~~In summary~~**~~:~~

- All data should always be exportable and deletable.

- Remind people of their choices and make it easy to change them.

- Ensure Terms of Use are human-readable by providing summaries or bullet points to accompany legalese.

- What someone is fired or quits, remove their access, but not their data

- Adopt common data standards and schemas when available.

- ~~Ag Tech tools should open up their APIs for greater interoperability, ~~~~including participating in the OpenTEAM API Switchboard as that project matures.~~

## Trust

~~There can be no long-term ~~~~ag tech winner ~~~~that hasn’t earned the trust of its users.~~

- **~~Transparency ~~****~~(honesty)~~**~~: Policies must be clear. \
Starting with Terms of Service, License Agreements, and Privacy Policies, the legal language which governs the relationship between tech developers and their users needs to be clear, well-organized, and must not attempt to hide undesirable deal elements or reserve unneeded rights.  This is ~~*~~not~~*~~ the inclination of most lawyers, so they must be actively managed to create trustworthy relationships.~~

- **~~Consistency~~**~~: A path to transparency. \
Everyone would benefit from more consistent policies.  Copying the structure and content of respected or standard documents saves time and money.  Calling out and providing the reasons for any deviations allows rapid assessment of an organization’s policies.  ~~~~OpenTEAM has created a draft Data Hosting and Storage Agreement to encourage further work in this area (see below)~~

- **~~The structures of trust: ~~**~~Building with and through already trusted entities brings a lot of benefits. This can mean having farmers as owners or core stakeholders of the business, building with and for TAPs, and/or structuring your project as an independent non-profit instead of a commercial or government affiliated organization.~~

There are a variety of valuable existing resources which can be utilized:

- ~~OpenTEAM’s ~~~~[Agricultural Data Use Documents](https://openteam-agreements.community/)~~

- ~~AgGateway’s ~~~~[Data Privacy and Use](https://s3.amazonaws.com/aggateway_public/AgGatewayWeb/WorkingGroups/Committees/DataPrivacySecurityCommittee/2017-03-31%20Data%20Privacy%20and%20Use%20White%20Paper%20-%201.2.pdf)~~~~ white paper~~

- ~~Tech Matters’ ~~~~[Better Deal for Data](https://bd4d.org/)~~~~ project~~

## ~~UI/~~~~UX Design~~

~~This collabathon has produced some very specific results and recommendations regarding UI/UX design for an Ag Data Wallet.  At the same time, we discovered and reviewed a wide range of ~~*~~general~~*~~ guidance for creating great user experiences.  Please see ~~~~Trust & Technology in Agriculture: Research Literature~~~~ and ~~~~UX Design Patterns, Landscape Report~~~~ for specific references and detail, but to summarize two key elements:~~

- ~~Familiarity~~~~: Don’t be original. \
Users do not want to learn new paradigms in order to interact with your tech.  ~~*~~Occasionally~~*~~ you will need to do something novel, but most design problems have been solved (better) before. ~~~~Use farmer language. ~~

- ~~Consistency: Repeat patterns in similar situations and break patterns in different ones. \
A confirmation dialog to delete a data element (if needed at all) should have the same text and the “yes” button in the same place each time.  If the user has elected to delete ~~*~~all~~*~~ their data, then the confirmation dialog should look fundamentally different, drawing the user’s attention to the fact that they may have mistakenly selected a different function.~~

- ~~Mobile centric~~~~ design is key for many use cases, as many farm applications are used in the field.~~~~ ~~

## Open Source / Open Data

~~Those in the OpenTEAM community are generally predisposed to creating open source technology and seeking to support and leverage open source assets.  ~~*~~Even if your situation seems to prevent working in the open~~*~~, look for pieces of your technology ~~~~that~~~~ do not provide a critical competitive advantage to test open development within your organization.  Contribute to open source projects which provide you with critical functionality.~~

~~As you collect data with your users, consider whether that data can be anonymized and distributed safely for the ~~~~benefit of broader research efforts~~~~ and to advance our understanding of agricultural practices and their impact on productivity and ecosystems.~~

# 9. – Deprecated - Next Steps [suggest delete, see new sec 8]

| Writing Meta (remove when done) |
|---|
| Source Document(s): 
Who’s running point on this section: 
Who’s contributing to this section: Steve, Ankita, Nat |


**How might we create an actionable, domain-driven, trust framework to improve ag data stakeholder outcomes?**

While there are many conceptual theories around trust and how it works, there is minimal tactical guidance on how to operationalize these ideas via domain-driven design. Research literature covers best practices, at a generic or high-level, regarding privacy, security and other subcomponents of trust, but this report (a) does not provide a comprehensive synthetic review, though we provide some foundations to build on. Like many before us, we argue that  here is a need for actionable guidelines on how to operationalize trust. However, we add that the need for empirically derived patterns that allow for traceability from our understanding of user perspectives to implementable design blueprints. Missing, is a coherent trust framework that allows designers and technology stakeholders to have a common conceptual model for how, for instance, improving “benevolence” can impact higher level trust outcomes, and subsequently impact the usability and usefulness of data-driven Ag Technologies.

[exact next step - user research study, properly stratified etc etc.]

- How do you currently think about trust in ag tech?

- How do you learn about what your users need to be able to value, use, and ultimately trust your technology with their data?

- Are there any activities on your product/project/organization roadmap to answer these questions?

**How might we implement standardized data management practices that encompass Ag Data stakeholder perspectives on data sovereignty?**

how do ag data stakeholders conceptualize data ownership, sovereignty, trust? what do these actually mean and how should the “system image” capture it. Cite norman re: conceptual modeling mismatch in design.

- MODUS gold star

[exact next step - inventory of data standards landscape.]

**How might we characterize diverse yet prototypical digital toolboxes among Ag Tech users?**

~~ what are prototypicial digital toolboxes and ui/ux among them? ~~~~How can we use the understanding of “real” digital footprints to understand stakeholder value oriented use cases. To answer, for example, what is familiar? What does “private” look like and for whom? cite rotz, prokopy, flachs re: spreadsheets and dst uptake/barriers to adoption~~


[exact next step - data collection but better]

**How might we create a common design pattern catalog that evolves with practices?**

**Is that valuable?**

~~what is an inclusive and equitable process for community-driven co-creation of a design pattern catalog? ~~~~Is it enough? Cite dark/fair patterns, the gang of four’s design patterns book, and, a sampling of material-design-like catalogs~~

[exact next step - design pattern catalog]

**How might we select appropriate utility technologies to implement trust-enhancing features in Ag Tech?**

~~What are specific recommendations on where and how different technologies can fit into individual ag tech data-sharing roadmaps? ~~~~E.g., there are x approaches to solving an encryption problem the one you pick is dependent on your tech stack, but the software quality you strive for should guide your selection process. Where do these types of selection processes come from? What is the role of open source, especially in context of conflation in ag with open access. Cite the software/security/sustainability paper, SWEBOK/ieee, and the data value chain paper~~

[exact next step - utility technology etc.]

**[closing]**

**What does an Ag Data Wallet look like to you now?**

~~There are a plethora of practical open-source technologies designed to support responsible data management, and there are many communities across sectors that have theorized actionable ways to implement trust-enhancing technology paradigms. One of our conclusions is that there is a need to more rigorously map the spectrum of ag data use cases to allow for the creation of tangible and actionable design patterns that could be integrated across the Ag Tech landscape. But, we do not propose ~~*~~“reinventing the wheel”~~*~~ as do we caution against ~~*~~“building sandcastles in the sky”.~~*~~ ~~

**open ADW implementation question vis a vis the cathedral vs bazaar**

reference to brook’s essential difficulties language to frame

- **~~Do we want to still want to build a standalone ADW? ~~**~~Does a set of design patterns? or something else?~~

**[call to action: our individual roadmaps -> collaborative roadmap]**

Pointer to activity? Keep in touch for [list of conveners and their next related actions, if any]

~~Many o~~~~pen questions~~~~ remain. We share a selection of these to provoke next steps among OpenTEAM community members.~~

- What is privacy in Ag Tech? Privacy in social interaction differs from privacy settings in GUI.

- To what extent can Ag Data be considered to be personal data and therefore warrant protection given by other privacy frameworks such as GDPR?

- Are the main concerns surrounding Ag Tech privacy only about financial benefits?

- Privacy & control: the dominant practices of communicating & obtaining informed consent for privacy policy have multiple usability problems

- What do we say about it here?

- Is exploring/envisioning better ways to handle it out of scope?

- Is there a way to reduce complexity which arises due to many stakeholders which can also be extranational?

- How can we realistically increase transparency?

- How do we define relationships among sense of security, data security, and cybersecurity? Perhaps we should consider existing data security principles and mapping onto them, especially the technical aspects.

- What did we learn from people about their values and attitudes on this topic?

- How do these principles interact with concepts related to trust?

- What is a “signifier” or “virtue signalling” vs an actual trust implementation

- Are open-source practices/projects enough to establish trust?

- How can we avoid the shortcomings of other principles and frameworks which do not become implemented?

- Who owns the data? The entity who owns the ground? The equipment? The guy who paid for the service?

- How relevant are emerging codes of conduct in facilitating trust?

- How do we play with the USDA? They have an outsized impact on all of this. If they suggest a standard they want to use then that takes a bunch of variables out.

# 9. References

Bhimani, A. (1996). Securing the commercial Internet. *Commun. ACM*, *39*(6), 29–35.[ ](https://doi.org/10.1145/228503.228509)[https://doi.org/10.1145/228503.228509](https://doi.org/10.1145/228503.228509)

Bodó, B. (2020). Mediated trust: A theoretical framework to address the trustworthiness of technological trust mediators. *New Media & Society*, 1461444820939922.[ ](https://doi.org/10.1177/1461444820939922)[https://doi.org/10.1177/1461444820939922](https://doi.org/10.1177/1461444820939922)

Boehner, K., Vertesi, J., Sengers, P., & Dourish, P. (2007). How HCI interprets the probes. *Proceedings of the SIGCHI Conference on Human Factors in Computing Systems*, 1077–1086.[ ](https://doi.org/10.1145/1240624.1240789)[https://doi.org/10.1145/1240624.1240789](https://doi.org/10.1145/1240624.1240789)

Carroll, S. R., Garba, I., Figueroa-Rodríguez, O. L., Holbrook, J., Lovett, R., Materechera, S., Parsons, M., Raseroka, K., Rodriguez-Lonebear, D., Rowe, R., Sara, R., Walker, J. D., Anderson, J., & Hudson, M. (2020). The CARE Principles for Indigenous Data Governance. *Data Science Journal*, *19*, 43.[ ](https://doi.org/10.5334/dsj-2020-043)[https://doi.org/10.5334/dsj-2020-043](https://doi.org/10.5334/dsj-2020-043)

Cogeca, C. (2018). *EU Code of conduct on agricultural data sharing by contractual agreement* (p. 11) [Guidelines].[ ](https://www.copa-cogeca.eu/img/user/files/EU%20CODE/EU_Code_2018_web_version.pdf)[https://www.copa-cogeca.eu/img/user/files/EU%20CODE/EU_Code_2018_web_version.pdf](https://www.copa-cogeca.eu/img/user/files/EU%20CODE/EU_Code_2018_web_version.pdf)

Davis, F. D. (1989). Perceived Usefulness, Perceived Ease of Use, and User Acceptance of Information Technology. *MIS Quarterly*, *13*(3), 319–340.[ ](https://doi.org/10.2307/249008)[https://doi.org/10.2307/249008](https://doi.org/10.2307/249008)

de Beer, J. (2017). *Ownership of Open Data: Governance Options for Agriculture and Nutrition* (p. 13). Wallingford: Global Open Data for Agricultural and Nutrition | University of Ottawa.[ ](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3015958)[https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3015958](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3015958)

Duncan, E. (2018). *An Exploration of how the Relationship between Farmers and Retailers influences Precision Agriculture Adoption* [University of Guelph].[ ](http://hdl.handle.net/10214/13546)[http://hdl.handle.net/10214/13546](http://hdl.handle.net/10214/13546)

Falcone, R., & Castelfranchi, C. (2001). Social Trust: A Cognitive Approach. In C. Castelfranchi & Y.-H. Tan (Eds.), *Trust and Deception in Virtual Societies* (pp. 55–90). Springer Netherlands.[ ](https://doi.org/10.1007/978-94-017-3614-5_3)[https://doi.org/10.1007/978-94-017-3614-5_3](https://doi.org/10.1007/978-94-017-3614-5_3)

Fleming, A., Jakku, E., Lim-Camacho, L., Taylor, B., & Thorburn, P. (2018). Is big data for big farming or for everyone? Perceptions in the Australian grains industry. *Agronomy for Sustainable Development*, *38*(3), 24.[ ](https://doi.org/10.1007/s13593-018-0501-y)[https://doi.org/10.1007/s13593-018-0501-y](https://doi.org/10.1007/s13593-018-0501-y)

Furnell, S. M., & Karweni, T. (1999). Security implications of electronic commerce: A survey of consumers and businesses. *Internet Research*, *9*(5), 372–382.[ ](https://doi.org/10.1108/10662249910297778)[https://doi.org/10.1108/10662249910297778](https://doi.org/10.1108/10662249910297778)

Gambetta, D. (Ed.). (1988). *Trust. Making and Breaking Cooperative Relations*. Basil Blackwell Ltd.

Gefen, D., Karahanna, E., & Straub, D. W. (2003). Trust and TAM in Online Shopping: An Integrated Model. *MIS Quarterly*, *27*(1), 51–90.[ ](https://doi.org/10.2307/30036519)[https://doi.org/10.2307/30036519](https://doi.org/10.2307/30036519)

Goodwin, C. (1991). Privacy: Recognition of a Consumer Right. *Journal of Public Policy & Marketing*, *10*(1), 149–166.[ ](https://doi.org/10.1177/074391569101000111)[https://doi.org/10.1177/074391569101000111](https://doi.org/10.1177/074391569101000111)

Herder, E., & van Maaren, O. (2020). Privacy Dashboards: The Impact of the Type of Personal Data and User Control on Trust and Perceived Risk. *Adjunct Publication of the 28th ACM Conference on User Modeling, Adaptation and Personalization*, 169–174.[ ](https://doi.org/10.1145/3386392.3399557)[https://doi.org/10.1145/3386392.3399557](https://doi.org/10.1145/3386392.3399557)

Hou, F., & Jansen, S. (2022). A systematic literature review on trust in the software ecosystem. *Empirical Software Engineering*, *28*(1), 8.[ ](https://doi.org/10.1007/s10664-022-10238-y)[https://doi.org/10.1007/s10664-022-10238-y](https://doi.org/10.1007/s10664-022-10238-y)

Jakku, E., Taylor, B., Fleming, A., Mason, C., Fielke, S., Sounness, C., & Thorburn, P. (2019). “If they don’t tell us what they do with it, why would we trust them?” Trust, transparency and benefit-sharing in Smart Farming. *NJAS - Wageningen Journal of Life Sciences*, *90–91*, 13.[ ](https://doi.org/10.1016/j.njas.2018.11.002)[https://doi.org/10.1016/j.njas.2018.11.002](https://doi.org/10.1016/j.njas.2018.11.002)

Jakob, N. N. (n.d.). *Usability 101: Introduction to Usability*. Nielsen Norman Group. Retrieved July 15, 2024, from[ ](https://www.nngroup.com/articles/usability-101-introduction-to-usability/)[https://www.nngroup.com/articles/usability-101-introduction-to-usability/](https://www.nngroup.com/articles/usability-101-introduction-to-usability/)

Kim, D. J., Ferrin, D. L., & Rao, H. R. (2008). A trust-based consumer decision-making model in electronic commerce: The role of trust, perceived risk, and their antecedents. *Decision Support Systems*, *44*(2), 544–564.[ ](https://doi.org/10.1016/j.dss.2007.07.001)[https://doi.org/10.1016/j.dss.2007.07.001](https://doi.org/10.1016/j.dss.2007.07.001)

Lewis, J. D., & Weigert, A. (1985). Trust as a Social Reality. *Social Forces*, *63*(4), 967–985.[ ](https://doi.org/10.1093/sf/63.4.967)[https://doi.org/10.1093/sf/63.4.967](https://doi.org/10.1093/sf/63.4.967)

Lin, D., Crabtree, J., Dillo, I., Downs, R. R., Edmunds, R., Giaretta, D., De Giusti, M., L’Hours, H., Hugo, W., Jenkyns, R., Khodiyar, V., Martone, M. E., Mokrane, M., Navale, V., Petters, J., Sierman, B., Sokolova, D. V., Stockhause, M., & Westbrook, J. (2020). The TRUST Principles for digital repositories. *Scientific Data*, *7*(1), 144.[ ](https://doi.org/10.1038/s41597-020-0486-7)[https://doi.org/10.1038/s41597-020-0486-7](https://doi.org/10.1038/s41597-020-0486-7)

Luhmann, N. (2018). *Trust and Power*. John Wiley & Sons.

Marangunić, N., & Granić, A. (2015). Technology acceptance model: A literature review from 1986 to 2013. *Universal Access in the Information Society*, *14*(1), 81–95.[ ](https://doi.org/10.1007/s10209-014-0348-1)[https://doi.org/10.1007/s10209-014-0348-1](https://doi.org/10.1007/s10209-014-0348-1)

Mayer, R. C., Davis, J. H., & Schoorman, F. D. (1995). An Integrative Model of Organizational Trust. *The Academy of Management Review*, *20*(3), 709–734.[ ](https://doi.org/10.2307/258792)[https://doi.org/10.2307/258792](https://doi.org/10.2307/258792)

Moran, K. (n.d.). *The Aesthetic-Usability Effect*. Nielsen Norman Group. Retrieved July 15, 2024, from[ ](https://www.nngroup.com/articles/aesthetic-usability-effect/)[https://www.nngroup.com/articles/aesthetic-usability-effect/](https://www.nngroup.com/articles/aesthetic-usability-effect/)

OpenTEAM Working Group Team. (2023, July). *OpenTEAM Ag Data Glossary*.[ ](https://openteam-agreements.community/TermsandDefinitions/)[https://openteam-agreements.community/TermsandDefinitions/](https://openteam-agreements.community/TermsandDefinitions/)

Raturi, A., Thompson, J. J., Ackroyd, V., Chase, C. A., Davis, B. W., Myers, R., Poncet, A., Ramos-Giraldo, P., Reberg-Horton, C., Rejesus, R., Robertson, A., Ruark, M. D., Seehaver-Eagen, S., & Mirsky, S. (2022a). Cultivating trust in technology-mediated sustainable agricultural research. *Agronomy Journal*, *114*(5), 2669–2680.[ ](https://doi.org/10.1002/agj2.20974)[https://doi.org/10.1002/agj2.20974](https://doi.org/10.1002/agj2.20974)

Raturi, A., Thompson, J. J., Ackroyd, V., Chase, C. A., Davis, B. W., Myers, R., Poncet, A., Ramos-Giraldo, P., Reberg-Horton, C., Rejesus, R., Robertson, A., Ruark, M. D., Seehaver-Eagen, S., & Mirsky, S. (2022b). Cultivating trust in technology-mediated sustainable agricultural research. *Agronomy Journal*, *114*(5), 2669–2680.[ ](https://doi.org/10.1002/agj2.20974)[https://doi.org/10.1002/agj2.20974](https://doi.org/10.1002/agj2.20974)

Rotz, S., Duncan, E., Small, M., Botschner, J., Dara, R., Mosby, I., Reed, M., & Fraser, E. D. G. (2019). The Politics of Digital Agricultural Technologies: A Preliminary Review. *Sociologia Ruralis*, *59*(2), 203–229.[ ](https://doi.org/10.1111/soru.12233)[https://doi.org/10.1111/soru.12233](https://doi.org/10.1111/soru.12233)

Rousseau, D. M., Sitkin, S. B., Burt, R. S., & Camerer, C. (1998). Not so Different after All: A Cross-Discipline View of Trust. *The Academy of Management Review*, *23*(3), 393–404.[ ](https://doi.org/10.5465/amr.1998.926617)[https://doi.org/10.5465/amr.1998.926617](https://doi.org/10.5465/amr.1998.926617)

Russel Hardin. (2002). *Trust and Trustworthiness*. Russell Sage Foundation.[ ](https://www.russellsage.org/publications/trust-and-trustworthiness-1)[https://www.russellsage.org/publications/trust-and-trustworthiness-1](https://www.russellsage.org/publications/trust-and-trustworthiness-1)

Singh, S., Sahni, M. M., & Kovid, R. K. (2020). What drives FinTech adoption? A multi-method evaluation using an adapted technology acceptance model. *Management Decision*, *58*(8), 1675–1697.[ ](https://doi.org/10.1108/MD-09-2019-1318)[https://doi.org/10.1108/MD-09-2019-1318](https://doi.org/10.1108/MD-09-2019-1318)

Slattery, D., Rayburn, K., & Slay, C. M. (2021). *Farmer Perspectives on Data*. Trust in Food, a Farm Journal Initiative & The Sustainability Consortium.[ ](https://www.trustinfood.com/wp-content/uploads/2020/05/Farmer-Data-Perspectives-Research_final.pdf)[https://www.trustinfood.com/wp-content/uploads/2020/05/Farmer-Data-Perspectives-Research_final.pdf](https://www.trustinfood.com/wp-content/uploads/2020/05/Farmer-Data-Perspectives-Research_final.pdf)

Sykuta, M. E. (2016). Big Data in Agriculture: Property Rights, Privacy and Competition in Ag Data Services. *International Food and Agribusiness Management Review*, *19*(A), 18.[ ](https://doi.org/10.22004/ag.econ.240696)[https://doi.org/10.22004/ag.econ.240696](https://doi.org/10.22004/ag.econ.240696)

Twyman, M., Harvey, N., & Harries, C. (2008). Trust in motives, trust in competence: Separate factors determining the effectiveness of risk communication. *Judgement and Decision Making*, *3*(1), 111–120.

Vasquez, O., & San-Jose, L. (2022). Ethics in Fintech through users’ confidence: Determinants that affect trust. *Ramon Llull Journal of Applied Ethics*, *13*, Article 13.[ ](https://doi.org/10.34810/rljaev1n13id398681)[https://doi.org/10.34810/rljaev1n13id398681](https://doi.org/10.34810/rljaev1n13id398681)

Venkatesh, V., Morris, M. G., Davis, G. B., & Davis, F. D. (2003). User Acceptance of Information Technology: Toward a Unified View. *MIS Quarterly*, *27*(3), 425–478.[ ](https://doi.org/10.2307/30036540)[https://doi.org/10.2307/30036540](https://doi.org/10.2307/30036540)

*What are Cultural Probes? — Updated 2024*. (n.d.). The Interaction Design Foundation. Retrieved July 12, 2024, from[ ](https://www.interaction-design.org/literature/topics/cultural-probes)[https://www.interaction-design.org/literature/topics/cultural-probes](https://www.interaction-design.org/literature/topics/cultural-probes)

Wilgenbusch, J., Lynch, B., Hospodarsky, N., & Pardey, P. (2020). *Dealing with data privacy and security to support agricultural R&D: Technical practices and operating procedures for responsible agroinformatics data management.* [Report]. CGIAR Big Data Platform.[ ](https://cgspace.cgiar.org/handle/10568/108095)[https://cgspace.cgiar.org/handle/10568/108095](https://cgspace.cgiar.org/handle/10568/108095)

Wilkinson, M. D., Dumontier, M., Aalbersberg, Ij. J., Appleton, G., Axton, M., Baak, A., Blomberg, N., Boiten, J.-W., da Silva Santos, L. B., Bourne, P. E., Bouwman, J., Brookes, A. J., Clark, T., Crosas, M., Dillo, I., Dumon, O., Edmunds, S., Evelo, C. T., Finkers, R., … Mons, B. (2016). The FAIR Guiding Principles for scientific data management and stewardship. *Scientific Data*, *3*(1), 160018.[ ](https://doi.org/10.1038/sdata.2016.18)[https://doi.org/10.1038/sdata.2016.18](https://doi.org/10.1038/sdata.2016.18)

Williams, R., & Kitchen, P. J. (1 C.E., January 1). *Involvement, Elaboration and the Sources of Online Trust* (involvement-elaboration-sources-online-trust) [Chapter]. Https://Services.Igi-Global.Com/Resolvedoi/Resolve.Aspx?Doi=10.4018/978-1-60960-575-9.Ch001; IGI Global.[ ](https://www.igi-global.com/gateway/chapter/www.igi-global.com/gateway/chapter/54130)[https://www.igi-global.com/gateway/chapter/www.igi-global.com/gateway/chapter/54130](https://www.igi-global.com/gateway/chapter/www.igi-global.com/gateway/chapter/54130)

Wiseman, L., Sanderson, J., Zhang, A., & Jakku, E. (2019). Farmers and their data: An examination of farmers’ reluctance to share their data through the lens of the laws impacting smart farming. *NJAS - Wageningen Journal of Life Sciences*, *90–91*, 10.[ ](https://doi.org/10.1016/j.njas.2019.04.007)[https://doi.org/10.1016/j.njas.2019.04.007](https://doi.org/10.1016/j.njas.2019.04.007)

Gitlab Layout:

Tabs:

- Home page: Intro section, table of contents for the rest of the sections

- Landscape review

- Perspectives from Farmers & TAPS

- Perspectives from Ag Technologists

- UX Design Patterns

- ADW Design Recommendations

- Conclusions

- Next steps

- Process - holds recordings and info from sessions we held and convener reflections if we get to those

## Notes

[^1]:  See this great explainer on what a “heuristic” is: https://thedecisionlab.com/biases/heuristics 