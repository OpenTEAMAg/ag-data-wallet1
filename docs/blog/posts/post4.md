---
date:
  created: 2024-01-18
---

# Farmer & TAP Findings Community Session

Thursday, August 15, 2024 9:00 AM to 10:00 AM PST 

<!-- more -->

## About the Session
This community session was hosted by the OpenTEAM Ag Data Wallet (ADW) collabathon conveners, where we shared learnings from our recent interviews with producers and Technical Assistance Providers (TAPs). This session was designed to provide valuable insights and foster collaborative discussions to inform the development of the Agricultural Data Wallet.

## Session ​Agenda:

### ​Process Overview (5 mins)

​We'll share our interview process and an overview of the general characteristics of the farmers and TAPs we interviewed.

###  ​Insights from Interviews (15 mins)

​Presentation of findings from the interviews.

###  ​Our Hypotheses (5 mins)

​Introducing our ideas on UX design patterns that can meet the identified needs.

###  ​UX Pattern Examples & Discussion (20 mins)

​We'll deep dive on two scenarios that emerged from the interviews and discuss potential design solutions that the ADW could offer to support producers & TAPs.

​This session is a unique opportunity to connect with fellow stakeholders, share knowledge, and contribute to the development of a tool that will support our agricultural community. We look forward to your participation and valuable insights.

Check out [the miro board](https://miro.com/app/board/uXjVKy5NGpI=/?share_link_id=507745245330) and the [session recording](https://youtu.be/Aj9xSFe_qdY?si=_HE6TLFhTx8lBBZH)

<iframe width="560" height="315" src="https://www.youtube.com/embed/Aj9xSFe_qdY?si=OuDo5UWM2et25UeO" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

# Ag Data Wallet UX Collabathon: Tech Review - Digging Deeper

##  Held on Thursday, July 18, 2024 9:00 AM to 10:00 AM PST 

In this session, we discussed discuss a few of the critical topics raised in our last Technical Subject Matter Expert Community Review:

- ​Should we “design” for producers or TAPs?

- ​How can we simplify the data-sharing decision? (what UX constructs or approaches)

​- How should we approach data ownership in real life?

- ​Can we draw bright lines around “using data against the interests of producers”?

Check out [the miro board](https://miro.com/app/board/uXjVK4LzYlo=/?share_link_id=978685647907) and the [session recording](https://www.youtube.com/watch?v=A9DGZcS5PV8)

<iframe width="560" height="315" src="https://www.youtube.com/embed/A9DGZcS5PV8?si=GWuFs4c5P2H4T8Vy" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>