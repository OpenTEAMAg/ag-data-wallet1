---
date:
  created: 2024-01-18
---

# Ag Data Wallet: From UX Patterns to UI Specifications

Thursday, September 19, 2024 9:00 AM PST 
<!-- more -->

## About the Session

During this session, we focused on the design implications emerging from our research and interviews. We examined high-level UX patterns we'd like to see ag tech tools adopt across the board to best support the sovereignty of producer and TAP users. We presented use cases and dive into specifications and user interfaces that are vital for the development of a user-centered Ag Data Wallet.

Check out [the miro board](https://miro.com/app/board/uXjVKrzwWDU=/?share_link_id=239974175447)and the [session recording](https://youtu.be/vcYx2b_i4kw?si=GM6bXpnrAHkPC4cf)

<iframe width="560" height="315" src="https://www.youtube.com/embed/vcYx2b_i4kw?si=Ktc3VvD-4xW4b2Pz" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
