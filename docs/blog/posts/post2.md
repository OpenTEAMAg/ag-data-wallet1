---
date:
  created: 2024-01-18
---

# Ag Data Wallet UX Landscape Review

Thursday, February 15, 2024 9:00 AM to 10:00 AM PST 
<!-- more -->

## What did we do 
To prepare for the upcoming Ag Data Wallet UI/UX Collabathon, we have been reviewing the state of research and practice on concepts related to trust, sovereignty, data sharing, privacy, and related topics in human-computer interaction, in the context of the agricultural community. 

Our review covers academic literature, existing technologies, and related projects, to understand the best practices and challenges in creating user interfaces and system architectures for gathering ag data, and for sovereign data stores. This effort will inform the ADW user research and co-design activities and subsequent collabathon outputs.

## About the Session

During this session we synthesized as a community what is already known about people’s attitudes toward data and technology in agriculture, and among existing technologies handling ag data, to identify strengths, challenges, and opportunities to improve. 

Check out [the miro board](https://miro.com/app/board/uXjVNxlvFlI=/?share_link_id=304668476644) from the session and the recording below! 
## Review the ADW Resource Library
 There are three ways in which we will use this library, as mentioned in the concept, and soon to be detailed in focus areas of their own:

  - To inform the ADW UX/UI user research on what farmers, technical service providers, and technologists think about with respect to trust in agtech.

  - To inform our design recommendations on how to enhance trust in agtech.

  - To provide a community resource on best practices regardign trust and technology.

<iframe class="airtable-embed" src="https://airtable.com/embed/appQB97CUa7jFYa4S/shrAWUFaVr6S5ukrY?backgroundColor=blue&layout=card&viewControls=on" frameborder="0" onmousewheel="" width="100%" height="533" style="background: transparent; border: 1px solid #ccc;"></iframe>


## Contribute to the ADW Resource Library
Please help make sure we’re not missing anything important! We invite the OpenTEAM community to make suggestions of relevant academic literature, technologies, and concurrent related efforts to ensure our landscape review is as thorough as possible as we embark on the upcoming Collabathon. Please note: what you submit will be **PUBLICLY viewable** in the library above!

<iframe class="airtable-embed" src="https://airtable.com/embed/appQB97CUa7jFYa4S/pag0IrGreVuY9q5pa/form" frameborder="0" onmousewheel="" width="100%" height="1500" style="background: transparent; border: 1px solid #ccc;"></iframe>
