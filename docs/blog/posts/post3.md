---
date:
  created: 2024-01-18
---

# Ag Data Wallet Technical Subject Matter Expert Community Review

Thursday, June 27, 2024 9:00 AM to 10:00 AM PST 
<!-- more -->

## About the Session

During this session, we reviewed the findings from our recent interviews with technologists on the topic of agricultural data management. This session is part of OpenTEAM's Agricultural Data Wallet (ADW) UI/UX collabathon, aimed at enhancing data practices and producer sovereignty in the agricultural sector. During this session, we:

- Presented an overview of our tech landscape review, covering key organizations, software tools, and information sources related to agricultural data.
- Shared insights from our conversations with various stakeholders, including coalitions focused on ag data education, policy, and interoperability.
- Discussed the range of software tools we reviewed, from farm management systems to back-end architectures supporting data sovereignty.
- Highlighted key standards and principles guiding best practices in data management, including data privacy and ownership concerns.
- Engaged in an open discussion about the implications of our findings and explore collaborative opportunities to advance the ADW initiative.

​This session was designed for technologists, agricultural data experts, producers, and anyone interested in the intersection of technology and sustainable agriculture. We look forward to your participation and contributions to this critical dialogue.

Check out [the miro board](https://miro.com/app/board/uXjVK4LzYlo=/?share_link_id=978685647907)and the [session recording](https://www.youtube.com/watch?v=9nAb3DWWoxU)

<iframe width="560" height="315" src="https://www.youtube.com/embed/9nAb3DWWoxU?si=j5yTLm7iMx9Y0k-S" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

# Ag Data Wallet UX Collabathon: Tech Review - Digging Deeper

##  Held on Thursday, July 18, 2024 9:00 AM to 10:00 AM PST 

In this session, we discussed discuss a few of the critical topics raised in our last Technical Subject Matter Expert Community Review:

- ​Should we “design” for producers or TAPs?

- ​How can we simplify the data-sharing decision? (what UX constructs or approaches)

​- How should we approach data ownership in real life?

- ​Can we draw bright lines around “using data against the interests of producers”?

Check out [the miro board](https://miro.com/app/board/uXjVK4LzYlo=/?share_link_id=978685647907) and the [session recording](https://www.youtube.com/watch?v=A9DGZcS5PV8)

<iframe width="560" height="315" src="https://www.youtube.com/embed/A9DGZcS5PV8?si=GWuFs4c5P2H4T8Vy" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>