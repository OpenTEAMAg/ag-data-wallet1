---
date:
  created: 2024-01-18
---

# Ag Data Wallet UX Collabathon Wrap Up: Trust and Technology in Agriculture

Thursday, Oct 3, 2024 9:00 AM to 10:00 AM PST 
<!-- more -->

## About the Session

In this session, we recapped this collabathon and discussed next steps. Each output was shared as well as collective learning from the conveening team. 

Check out [the miro board](https://miro.com/app/board/uXjVLab1uUs=/?share_link_id=685858071524) and the [session recording](https://youtu.be/LjOvzYkgPbI?si=Tjs4YbJK_bplmpxv)

<iframe width="560" height="315" src="https://www.youtube.com/embed/LjOvzYkgPbI?si=qPZ35WJb0dsBA_ax" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>