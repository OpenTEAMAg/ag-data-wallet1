# 7. ADW Design Recommendations

## Suggested User Interface Elements

This section presents a series of suggested user interface (UI) components designed to address the use cases that emerged from the interviews and community sessions with agricultural producers and technical assistance providers (discussed in part 4), focusing on their specific needs and challenges in managing and accessing agricultural data. Selected UI elements are accompanied by wireframes that visualize the solutions. By grounding design recommendations in real-world input, we aim to create a tool that fosters more effective and streamlined data management for agricultural stakeholders.

### Data Input UI

This element addresses how agricultural data enters the Ag Data Wallet. Based on the interviews and sessions with producers and TAPs, a useful ADW would be able to take in data directly, import and parse a spreadsheet, or connect to another ag tech tool and import data.

Functionality might include:

- Simple direct data entry via voice recording, text entry, image capture, or location capture with accessible buttons/controls

- Voice to text functionality

- Spanish translation (and other relevant languages)

- Offline mode to allow operation and store data on device until there is an internet connection

- Ability to categorize and sort these data points into standard formats or existing datasets

- Spreadsheet import:

- Import templates that allow producers to record data in a common format that aligns with a shared schema

- Manual import column mapping to align an existing spreadsheet with the shared schema

- AI-powered or algorithmic data parsing to find patterns in the data and map it to a shared schema

- A library of services that can be connected through an authorization flow to search for other ag tech tools, authenticate in a tool, and import specific datasets from that tool.

- Ability to turn on ongoing syncing of data to keep the ADW up to date as data changes in the source

![Enter image alt description](img/ocG_Image_20.png)

**Figure 7.1 Data Input UI example**

#### Use Cases

1. As a farmer/farmworker/TAP collecting data in the field, I need easier and more reliable ways to record that data, so that I can save time and collect more detailed data.

2. As a farmer primarily using spreadsheets for farm data, I want to upload my spreadsheet into a tech tool and have the data parsed and presented to me in an easy to view/search/share format, so that:

1. I can use it to make farm management decisions.

2. I can access data-based opportunities (certification, grants) without having to use an additional tech tool that I do not like, and can continue using my spreadsheet system which works well for our on-farm data collection.

3. As a farmer using multiple tech tools to store/analyze different types of farm data (PastureMap and FarmOS for example), I would like to aggregate and manage all my data in one place so that I can access opportunities by sharing this data.

### Data Management UI

Once the data has entered the ADW, producers need a way to view and manage it. The Data Management UI would show a list of datasets, with the ability to drill down into each one.

Functionality includes:

- Ability to view & search imported data

- Preview of data in each dataset

- Ability to open up and view a complete data set

- Ability to edit, add, and delete data

- Meta-data such as who imported or added the data set, date-time, and provenance

- Ability to export the data in various formats

![Enter image alt description](img/SCu_Image_21.png)

**Figure 7.2 Data Management UI example**

#### Use Cases

- Same as above.

### Data Sharing UI

The Data Sharing interface is the place where producers and other authorized users would initiate the sharing of specific datasets with another group, or platform, such as a TAP organization, certifier, government agency, or digital farm tool. In this interaction, it is important to clearly and completely illustrate the terms of sharing with the party in question so that the producer can make an informed decision.

Specific functionality may include:

- Search bar for producers to find the party they want to share with.

- Inbound data sharing requests to approve or deny, with context, for example if a TAP wants to share the producer’s data with a new third-party.

- When a producer is searching for the party they want to share with, the potential connection should specify:

- The terms of sharing with that party, including whether sharing can later be revoked, and shared data deleted

- Individuals and/or organizations that will have access to the data

- What recipients will do with the data

- Data management practices / certifications

- How the producer will benefit

- Additional entities that will have access to it (for example, if sharing with "USDA" which specific agencies will have access?)

- Ability to select which datasets will be shared, and to see a preview of the data. The preview may be in a table format or a map format, or other formats as appropriate.

- Ideally there is an option to select a subset of a dataset to share, such as specific columns from a spreadsheet

- For each dataset, the ability to specify whether it can be shared with additional third parties

- Ability to set an expiration date after which the data will no longer be available unless permission is renewed

- Ability to set whether data is shared ongoing, or just one time

- Nice to haves for this UI:

- AI-powered summary of Terms of Use for sharing data with each service/partner that flags potentially controversial things.

- When sharing data for an application, it would be helpful for the system to identify any required data that are missing from the wallet’s datasets

- Tools for sharing data into aggregate data pools and data coops anonymously

![Enter image alt description](img/Bas_Image_22.png)

**Figure 7.3 Data Sharing UI example**

#### Use Cases

1. As a farmer looking for technical assistance and funding opportunities, I want to search for TAPs, orgs, businesses, and govt agencies to **find the right one to share with and then be able to share my data with them easily**.

2. As a farmer deciding whether to share my data, I need to **understand the terms of the agreement (the fine print) easily**, so that I can give informed consent without having to spend too much time reading legalese.

3. As a farmer or TAP preparing to share data, I would like to **select the specific datasets to share and see a preview **of the data, and edit for quality assurance, so that I can ensure that only relevant data is shared.

4. As a farmer deciding whether to share my data,** I want clear information on who will have access and what it will be used for**, so that I can decide if it is worth the time & costs of sharing my data.

### Data Access UI

The Data Access interface expresses a list of parties and services that have been granted access to data held in the ADW. It is important for the producer to see at a glance where their data has been shared, by whom, and for what purposes, and if necessary, to revoke any access that is no longer needed.

Functionality includes:

- A list of parties with whom ag data has been shared.

1. For each one, show which dataset(s) are being shared, what are the basic terms / what the user is getting out of it, and option to revoke or request to revoke

2. An audit log showing who shared/accessed what data and when

3. Periodic reminders to check if users still want to be sharing data with the same person/service

4. For each external party using the farmer's data, provide a button to revoke permission or request to remove the data.

5. A list of previous form submissions and sharing actions, with the ability to view / export the data / report that was shared, in multiple formats such as CSV, PDF, etc

6. For situations where data has been shared widely, it may be helpful to break this list into sections based on categories or permissions (services that can update the data, vs services that can only read the data). Disconnected services could be in a collapsed section that is visually deprioritized but available when needed.

![Enter image alt description](img/1IX_Image_23.png)
 
**Figure 7.4 Data Access UI example**

#### Use Cases

1. As a farmer who has already shared my data, I need a** list of who has access, which datasets they have, and what they are using it for**, so that I can be aware of who is accessing my data and understand whether I want to keep sharing or stop.

2. As a farmer who has already shared my data, I would like to be able to **revoke my data** from a specific partner, so I can remove it from places that I no longer want it.

### Team Permissions UI

The final interface we recommend is a Team Permissions page where the producer holding the wallet can authorize trusted individuals to manage and share data in the wallet on their behalf.

- A place where admins can invite other users (TAPs etc) and authorize them to input, manage, or share their data

- Ability to change roles / revoke access from authorized users

- Specify which datasets each user/role has access to

#### Use Cases

1. As a farmer, I want to **authorize a user (TAP) to input data, manage data, and share data on my behalf, **so that:

1. I do not have to use a tech tool that I do not understand.

2. I do not have to spend time doing data management because I am very busy.

## System Architecture Considerations

The functionality of an ag data wallet, data storage and intermediation (data input & data output: which data, to whom), can reside in a standalone system or be incorporated into many different Ag Tech tools.  In fact, it would be a rare Ag Tech tool that provides neither data input nor data output.  Figure 7.5 shows some of the different system connections that could be supported by ADW functions.

The OpenTEAM API Switchboard project provides a way for multiple systems to interact by creating a single API connection (to the Switchboard).  If adopted, it substantially reduces the burden on any particular application (including the Ag Data Wallet) to code to a myriad of different system APIs.

Note that API connections between systems require agreement not only on the programmatic access methods, but also on the type of data to flow over the API *and the meaning of that data*.  Regardless of open and well-documented APIs, all providers benefit from data and metadata schema standardization.

Manual input and output functionality (except direct data entry) also relies on data format standardization.  Automated format identification (with conversion, if needed) can be augmented with a manual UX for mapping incoming and outgoing data to arbitrary formats.

The figure attempts to show that the Ag Data Wallet functions could be standalone or could be directly incorporated with other general purpose Ag Tech.

![Enter image alt description](img/7li_Image_24.png)

**Figure 7.5 **** (****[image source](https://docs.google.com/presentation/d/1uKXXM1RR8VBMa5u_hA5kNkrY5VguR2_dMtBP818icHw/edit?usp=sharing)****)**

