
# Ag Data Wallet: From UX Patterns to UI Specifications
### Thursday, September 19, 2024 9:00 AM PST 

​This session is part of OpenTEAM's Agricultural Data Wallet (ADW) UI/UX collabathon, aimed at enhancing data practices and producer sovereignty in the agricultural sector.

​We'll focus on the design implications emerging from our research and interviews. First, we'll examine high-level UX patterns we'd like to see ag tech tools adopt across the board to best support the sovereignty of producer and TAP users. Next, we'll present use cases and dive into specifications and user interfaces that are vital for the development of a user-centered Ag Data Wallet.

​This session is designed for technologists, agricultural data experts, producers, and anyone interested in the intersection of technology and sustainable agriculture. We look forward to your participation and contributions.


*Questions? Contact Clare Politano [clare@terran.io](clare@terran.io)* 
