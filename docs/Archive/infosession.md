
# Ag Data Wallet UX Collabathon Info Session 
### Held on Thursday, January 18, 2024 8:00 AM to 9:00 AM PST 

​During this info session, we shared the research design and plans for the Ag Data Wallet UX Collabathon with the OpenTEAM community, answered questions and gathered input, and encouraged participation in the Collabathon.

Check out [the miro board](https://miro.com/app/board/uXjVN6pT7M8=/?share_link_id=209943819334) from the session and the recording below!

**​Agenda:**

- ​Welcome

- ​Meet the Collabathon Team: Meet the dynamic team behind the Collabathon, learn about their backgrounds, and understand their roles in the project.

- ​Collabathon Project Plan: Dive into the detailed plans for the collabathon, including objectives, timelines, and expected benefits and outcomes.

- ​Research Design: A deeper look at the research design, highlighting key methodologies, tools, and the impact we aim to achieve.

- ​Q & A: An open forum to ask questions, share thoughts, and engage directly with the team.

- ​Next Steps / How to Register: Learn about the next steps in the process and how you can register to be a part of this exciting initiative


*Questions? Contact Clare Politano [clare@terran.io](clare@terran.io)* 

<iframe width="560" height="315" src="https://www.youtube.com/embed/bzfXixd1ZtE?si=LVouGF95h3WP4W1Z" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>