


## Agricultural Data Wallet
Our vision for an agricultural data wallet (Ag Data Wallet or ADW) is a digital place where farmers, ranchers, and land stewards can store, access and manage their data. They can also control whether, and how, others can see it. This control over what specific data is available to others, and on what terms, is a critical aspect of this vision.

Imagine a common interface where producers could:

  - aggregate and view all their important operational data
  - review and submit required data to government agencies
  - specify and share operating data with trusted advisors
  - respond to a university research request to share particular de-identified data
  - share ecological practices data to earn payments for carbon sequestration and other ecosystem services
  - see a list of everyone who has data access, and revoke permission easily

An Ag Data Wallet would provide complete autonomy over producer data, and make it easy to share chosen data with trusted partners. Implementing an ADW requires data exchange between many systems which are currently disconnected. By standardizing data formats and transfer methods, we believe that this “digital wallet” could reduce the time producers spend on paperwork, increase the value of data collected, and provide greater data security and peace of mind.

## OpenTEAM Collabathon: Ag Data Wallet User Experience

> Good design, when it’s done well, becomes invisible. 
> It’s only when it’s done poorly that we notice it
>
>  Jared Spool

Over the next several months, OpenTEAM will bring together producers, their advisors, technologists, designers, researchers, and government agencies to dig into the practical applications of an Ag Data Wallet. We want to understand how an ADW could best help solve real problems faced by the agricultural community.

The specific focus of this work is to identify the most important data-sharing problems faced by the community and design a UX (user experience) that will meet these needs. UX design is a multidisciplinary practice that merges user needs and technological possibilities to create intuitive interfaces that let farmers, ranchers, and land stewards get things done.

The intended outcome of this Collabathon is a system architecture, specifications for required functionality including how data is structured and stored, and a set of UX design patterns: recommended specific user interface (UI) elements, sequences, and terminology that would make an Ag Data Wallet easy to use and valuable. Good design patterns are familiar, highly functional, and intuitive (provide clues or cues).  Sometimes “familiar” is most important: if you rent a car, you expect the turn signal to be to the left of a “steering wheel”, and the accelerator to be to the right of the brake. There is nothing intuitive about these design decisions, but they are familiar and functional. A system architecture identifies the components and interconnectivity of the various elements that might constitute a functional Ag Data Wallet. 

## Hypothesis and Methodology
### Hypothesis
We hypothesize that collaboratively designing and implementing an Ag Data Wallet for land stewards will streamline agricultural data management, reducing administrative burdens while increasing privacy and control over data. This will lead to increased farmer income and wellbeing, and make it easier to support a variety of data-driven opportunities.  Ultimately this will benefit land stewards, governments, certifiers, technology providers, and communities with improved ecological outcomes and greater sovereignty.

### Methodology 
We have decided to use a participatory research framework[1][2] because we believe that the opinions of the most affected communities are essential. Our collaborative and iterative research and design approach will allow us to create a community-centered partnership in problem-solving. We will conduct semi-structured interviews and surveys, as well as organize research and design sessions based on our participatory design principles. Throughout the process, we will share the intermediate outcomes of our collaboration with the participants.


In the first half of 2024, we plan to undertake the following activities:

  - Convene the OpenTEAM community and review the proposed plan for the Collabathon
  - Review and discussion of good examples: what can we learn from the success cases?
  - User research: individual or small group sessions to gather information about needs
  - Review and discussion of user research results
  - Design sessions: create UX design patterns
  - Design sessions: system architecture
  - Review and discussion of designs
  - Iterate design sessions
  - Final review and publish system architecture and UX design pattern results 
(and other learnings)

Note: The UX design patterns are intended to be independent of any particular system (phone, tablet, or computer; Android, iOS, or Windows). An expected follow-on to this Collabathon is the creation of one or more reference implementations of the Ag Data Wallet UX to serve as a further example, and an RFP to build an Ag Data Wallet.
