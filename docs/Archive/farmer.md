# Farmer & TAP Findings Community Session

## Held on Thursday, August 15, 2024 9:00 AM to 10:00 AM PST 

Check out [the miro board](https://miro.com/app/board/uXjVKy5NGpI=/?share_link_id=507745245330) from the session and the recording below!

*Questions? Contact Clare Politano clare@terran.io*

## About the Session
This community session was hosted by the OpenTEAM Ag Data Wallet (ADW) collabathon conveners, where we shared learnings from our recent interviews with producers and Technical Assistance Providers (TAPs). This session was designed to provide valuable insights and foster collaborative discussions to inform the development of the Agricultural Data Wallet.

## Session ​Agenda:

### ​Process Overview (5 mins)

​We'll share our interview process and an overview of the general characteristics of the farmers and TAPs we interviewed.

###  ​Insights from Interviews (15 mins)

​Presentation of findings from the interviews.

###  ​Our Hypotheses (5 mins)

​Introducing our ideas on UX design patterns that can meet the identified needs.

###  ​UX Pattern Examples & Discussion (20 mins)

​We'll deep dive on two scenarios that emerged from the interviews and discuss potential design solutions that the ADW could offer to support producers & TAPs.

​This session is a unique opportunity to connect with fellow stakeholders, share knowledge, and contribute to the development of a tool that will support our agricultural community. We look forward to your participation and valuable insights.


<iframe width="560" height="315" src="https://www.youtube.com/embed/Aj9xSFe_qdY?si=OuDo5UWM2et25UeO" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>