# 4. Perspectives from Producers & Technical Assistant Providers

During the collabathon, we organized a set of one-on-one interviews with producers and technical assistance providers who are a part of the OpenTEAM network. We identified these groups as key stakeholders and users in developing the concept of an Ag Data Wallet. We designed a semi-structured and informal interview process to gather information about data collection, management, and storage processes, as well as challenges that they have dealt with as users of current technologies.

## 4.1 Goals

From the preliminary landscape and literature review (see section 2), we learned that there is a lack of shared understanding around how to define trust in the context of an Ag Data Wallet. We also identified gaps in general knowledge around how producers and TAPs (Technical Assistance Providers) interact with technologies and manage data/information, which we know that these communities are engaged in.

We saw value in engaging a small number of subject matter experts during the collabathon who have experiential expertise in operating a farm, or work for an organization that is providing technical assistance to producers to help inform our design outputs. Our primary goal was to ask producers and TAPs about their attitudes and experiences around collecting, managing, and sharing farm data and using technology in agricultural applications.

Goals:

-   Engage potential users of technology products for a preliminary understanding of attitudes and challenges around data and technology

-   Gain a better understanding of how producers and TAPs share data

-   Gather information from key stakeholders in a trusted one-on-one setting to inform the ADW UI/UX design catalog

## 4.2 Interview Process

OpenTEAM reached out to a network of producers and TAPs to recruit interviewees. Recruiting was through the OpenTEAM community, as well as through personal connections with farms in New England and California. We reached out to research farms, non-profit farms, and farms that had engaged in work with OpenTEAM in the past. We also listed a call for interviewees in six farmer-facing publications and email newsletters. We used emails and phone calls to contact individuals.

Over the course of 3 weeks, OpenTEAM's Farm Network Coordinator conducted 13 one-hour interviews. Interviewees were given the option to have their interview recorded or not recorded, with the understanding that recordings would only be used by the convening team for synthesis and that quotes from the interviews would be shared with interviewees for their approval prior to use in any collabathon [community meetings]{.mark}.

In all interviews, the interviewer took notes during the conversation, and reorganized the notes by topic after the session. The notes were used as a primary document for interview synthesis, and any recordings that were saved were used as an additional source for synthesis.

To synthesize interviews, a team of six people made up of OpenTEAM staff and the convening team read through the interview notes and highlighted key takeaways from each interview in a collaborative document. From this document, we looked across all interviews and identified common topics that were discussed in a table, pulling quotes from the interviews as needed and linking discussions that were related. We used this synthesis process to capture key anecdotes and stories from related experiences across the interviews.

On August 15, a community meeting was held to discuss what we learned from these interviews, and to identify challenges and opportunities related to the design of an Ag Data Wallet.

## 4.3 Who We Talked To

### Producers 

The group of producers we interviewed represented a range of operations, from small fruit and vegetable farms to large-scale diversified crop and livestock farms. Farm sizes ranged from 4.5 acres to 500 acres, with varying degrees of employee involvement, from no full-time workers to operations employing up to 200 people. Four producers were located in the Northeast, two in the Southeast, and two in California.

Most of the farms participate in diverse markets, including CSA (Community Supported Agriculture), local retailers, and wholesale distribution. Several also operate on-farm stores, while two farms engage in national-level wholesale and door-to-door shipping.

Farm operations included livestock, meat production, row crops, orchards, vegetables, fruits, cut flowers, trees, and other specialty crops. Livestock is a significant operation in many of these farms, with a few specializing in grass-fed and mixed livestock systems, while others balance crops with animal agriculture.

### Technical Assistance Providers

The group of Technical Assistance Providers (TAPs) represent a wide range of technical skills and experience levels, working with different farmer populations on various agricultural challenges. TAPs ranged in years of experience from 2 years to 13 years, working with producers on issues of climate resilience, farm viability, crop disease and pests, and conservation planning. Some worked closely with a few producers for multiple years, while others worked with over 100 new producers each year. All of the TAPs interviewed served the Northeast. TAPs were affiliated with governmental, nonprofit, and academic institutions

## 4.4 Biases and Limitations

-   Interviews were recruited for and conducted mid-June to early July, limiting the ability for some producers to participate

-   All producers and TAPs were recruited through the OpenTEAM community or via OpenTEAM staff\'s connections

-   Small set of 8 producers and 5 TAPs

-   Limited geographic diversity

-   Synthesis of the interviews is filtered through our biases

We recognize that we recruited during a very busy time of the year for producers, as well as keeping our initial recruitment to the common community. As a result, there are some limitations to the generality of the findings, and we recognize that our own biases may be represented in the synthesis and in this report. We caution the reader against taking these observations as general conclusions at this time: we are continuing this work in the community and we are planning to widen the farmer/producer engagement as a next step.

## 4.5 Producers: Learnings

**Farm Data**

We spoke with producers about the types of data they collect and manage. The types of data we discussed can be grouped into two primary categories, with seven subcategories of data:

-   Farm Management Data

    -   Farm Events: Animal birth and slaughter dates, pasture movements, tillage and field preparation events, cropping events, yield and harvest data

    -   Animal Health Data: FAMACHA score (anemia test), animal weights, medications, breeding and lineage information, medical histories

    -   Soil Data: Soil amendments, soil tests, bulk density, tillage and field preparation events, cover cropping, height and types of grasses and forage

    -   Orchard Data: Maintenance and pruning, fruit Brix scores, freeze days, sap analysis

-   Farm Enterprise Data

    -   Financial Data: enterprise-related expenses/revenue, sales at various outlets, farm expenses, employee data (how many employees are doing a task, how long it takes them, hours they are working, etc.)

    -   Certification & Reporting Data: carbon monitoring, food safety data, certification protocols

    -   Research Data: phenology testing, wildlife tracking, soil health metrics

**Data Collection & Data Management**

*Tools:* When we asked producers about the technology tools they use to collect data, 23 tools were mentioned. Most of the producers we spoke to are collecting some data on "paper" before moving it to a technology tool, regardless of their tech tool usage. "Paper" includes physical sheets of paper, as well as iphone "notes" and whiteboard usage. All interviewed producers reported that they collect certain data on paper first prior to digitizing them (if they digitized that type of data at all), regardless of the number of digital tools they used. Most of the producers were using multiple tools to collect data (see table 4-1).

![table 4-1](img/table4-1.png){ align=left }
*Table 4-1*

All but one of the producers utilized Google Sheets for some type of data management and collection. However, many also noted challenges in managing data in Microsoft Excel and Google Sheets. Successful adoption of data collection tools by farmworkers can impact farmer's ability to choose tools. If a tool is too burdensome for a farmworker to use, data collection can be inaccurate or data can be lost.

*Certification:* We learned that certain data collection is driven by the demands of external certification processes and that collecting and managing data is time intensive for producers. Though, it was also reported that data entry required for certifiers can provide value back to the farmer/producer. Some examples of certifications that the interviewees have are: USDA Organic, REAL Organic Project, State-run Organic Certification (Ex. Maine Organic Farmers and Gardeners, California Certified Organic), Animal Welfare Approved, Food Safety Certified (Global GAP), and others.

*Data Management:* We learned that similar types of data may exist in different places (e.g. in a business plan, with a certifier, with NRCS), and that it can be challenging to manage data in various tools and forms. Some of the considerations for managing and collecting data included: information access, simplicity of data entry, and ability to share within the team.

*Research Data:* The research data collection methods were specific to the studies happening at the farms, and differed across studies. Sometimes producers will collect data for research, and other times researchers themselves are collecting data on the farm.

**Data Sharing**

Producers discussed sharing data with several different groups:

-   Certifiers

-   Government (e.g. soil and water districts, ...)

-   Universities/Researchers, for research studies

-   Other producers

-   Vets

-   Apprentices or for educational purposes

-   Tech developers

-   Social Media

*Openness to Data Sharing*: Several producers stated that they are open to sharing data, generally, though with some consistent exceptions around financial and price data. Producers expressed that data sharing can help with systemic understanding of our food systems. And, the producers we spoke with are sharing their data openly, through shared documents or websites, in order to help other producers, for learning and education towards new farmers and apprentices. "There's a way to share it, and it's in a public paper, that everybody has access to. It levels the playing field, and it's not this one-on-one proprietary thing."

*Mutual Benefit:* Some producers said that they would be more comfortable sharing data when there was a mutually beneficial agreement. For example, one farmer/producer told us that they openly share data with people who have vested interest in their farm, such as vets. Some producers expressed that data sharing with each other helps everyone understand the system and helps others with their farming techniques.

*Risks around Data Sharing:* The producers we spoke to did have concerns about data misuse. For example, they were concerned about potential exposure of proprietary farm information to competitors. Or, having limited control over how shared data might be analyzed or interpreted.

*Hesitation to Data Sharing:* While producers were very open to sharing, some said that they don\'t share raw data. Other types of data that producers were hesitant to share were: financial data, data about payroll or that felt personal with respect to their workers. One producer shared that they would be nervous if asked to share a list of all their market and sales outlets. Some producers described concerns about sharing data that would be used for commercial purposes (that are not just commercial producers - other commercial entities). For example: "We are more cautious and calculated about what we want to share." (When it's not another commercial farmer - but other commercial entities). "They aren't going to get a pile of data and we get nothing." "They give us a widget... and after a year, they get 7 figures and an exit. I wouldn't feel like that was an equitable exchange." There are significant trade-offs around sharing financial information and producers indicated this as an example when they would be less likely to share data. One farmer/producer said the benefits (borrowing power for loans) were great enough that they have gone \'all in\' on financial records.

## 4.6 Technical Assistance Providers (TAPs): Learnings

**Farm Data**

In speaking with these TAPs, there was a wide variation in the amount and type of data they collected. The type of data collected depended on their service area and the data that the farmer had available or that the TAP could collect themselves. Some types of data that were mentioned were soil/ecological data, pest and disease data, farm yields, farm viability data (ex. financials), and farm practice data. TAPs collect this data in order to achieve their organizational mission of assisting producers in their service area, or for research or government purposes.

**Data Collection & Data Management**

Most TAPs collect data via surveys, farm records, and on-site visits. Depending on the type of data the TAP is trying to collect, they will either collect it on their own or look to the farmer to provide it. Producers may provide data verbally, through farm records on paper or digitally, or via email. Some of the TAPs mentioned that they often will collect the data they need themselves because the farmer does not have it or it is too burdensome to ask the farmer to collect. TAPs may collect this data and enter/submit it on the producers behalf. For example, a TAP looking for data on the bulk density of farm soil could collect the soil sample, mail it to the lab, receive the results, and then share those with the farmer. Some of the mandated tools that producers must use for various programs can be burdensome or challenging to use, so TAPs may also help producers input data into these systems, or the TAP may input data on the farmer's behalf.

A notable difference between the ways that producers and TAPs collect data is that TAPs are often using tools they did not choose. These may have been mandated by the organization they are employed by (ex. CRM systems, Microsoft or Google Workspace) or are required by the programs they are entering data into (ex. WebGrants, COMET). Many of the TAPs we spoke to expressed frustrations working with mandated tools themselves, and challenges in asking producers to use these tools as well. The table below visually demonstrates the range of tools that TAPs use both internally and externally.

![table 4-2](img/table4-2.png){ align=left }
*Table 4-1*

**Data Sharing**

The TAPs we spoke with shared farm data for programmatic, research, or funding purposes. TAPs may share farm data internally for organizational improvement or with research partners. TAPs also share data in order to benefit the producers they are working with. For example, sharing farm data to help producers obtain funding, helping them plan for long-term farm viability, and giving pest/disease/soil health advice or guidance. TAPs share data by entering the data collected by them or the farmer into the tech tools that are mandated by their organization or the program. Some examples of these are WebGrants, Apricot, Salesforce, SurveyStack, and ESRI Field Maps. Before TAPs share data, they will obtain permission from the producer to share it with external entities. All of the TAPs interviewed also anonymized or aggregated all data before it was released.

**Impact of Trust on Data Sharing**

TAPs indicated that their data sharing process with producers is dependent on building trust. Some of the ways that TAPs build trust is by focusing on relationship building prior to asking producers for their more personal data. TAPs must be aware of their organization's reputation with certain producers and how that may impact a producer's willingness to share. Clearly explaining how data will be used and ensuring confidentiality is a critical part of this process. One TAP described a process of working with a farmer's peers to help them feel more comfortable sharing data with the TAP.

Another insight from the TAP conversations was the importance of farm data benefiting producers. This was evident from the data collection stage to data sharing. If a farmer is being asked for data that is burdensome to collect, they could be less likely to cooperate with a TAP in the future. TAPs may emphasize the benefit of data collection and sharing with the producer, such as personalized technical support or more accurate benchmarking. TAPs will share back insights learned from the farmer's data in cases where the data is being shared externally.

Some TAPs we spoke with also follow privacy protection and data security protocols. Most of these practices were self-created and self-managed by the TAP. One TAP organization had the practice of deleting salesforce instances and emails after a project is completed. One TAP mentioned not sharing data with a third party unless explicitly asked by the producer, ensuring that the producer is the one ultimately submitting or sharing the data externally. Some TAPs encrypt and password protect files or delete all farm data from their computer once the project is completed.

## 4.7 Challenges Related to Technical Tools

### What We Heard From Producers:

-   Complexity of tools

    -   Challenges in finding the perfect tool and the need for easy, streamlined systems means many producers end up using spreadsheets

    -   Human error, language barriers, and literacy can impact farm workers ability to use a tech tool successfully

    -   Some producers and farm workers find managing data on Microsoft Excel/Google Sheets challenging (even though most producers we spoke with do use spreadsheets).

-   Lack of interoperability between systems.

    -   Similar types of data may exist in different places (business plan, with a certifier, with NRCS) Comprehensive or complete information may be stored across several different places which are not linked together. For example, land use information about a farm may have geographic information stored in NRCS, other information about each plot of land in a business plan, and even further information about each plot of land with a certifier.

    -   Data collection for certifiers can be time intensive and detailed. It is burdensome to find data in different places to enter into the certification form year after year.

-   Concerns about data security and use by technical tools.

    -   Some datasets are more sensitive than others (such as financial data) and producers want to make sure of the benefits of sharing outweigh potential risks

    -   User agreements can be long and difficult to understand

### What We Heard From TAPs:

-   Lack of interoperability between the systems/tools that they are mandated to use.

    -   Have to enter similar data into different tools to meet program requirements and there is no way to share data between them

    -   Wish it were easier for data to flow across various tools: CRMs, Google Workspace, and others

    -   TAP's organization and the (target) program requirements determine the tech tools they use, these tools are not always intuitive/easy to use

-   Hard to offer fast and effective solutions: When working with producers, technical issues with incompatible systems slow TAPs\' work.

    -   Some farm records are not available for the TAP to provide effective assistance

        -   E.g. some producers do not keep detailed records of farm activities or farm data

-   Privacy concerns around farmer's data

    -   Can be unclear who has access to farmer\'s information within a TAP organization

    -   Want ways to increase data security/cybersecurity

        -   e.g. locking document, revoke access after staffing change

    -   Data access permissions are not clear in data management/sharing tools

### What we heard during the community review meeting:

On August 15, 2024 the convening team held a community review meeting to share out our findings from the interviews and get community feedback. We had 36 attendees, including interviewees, producers and TAPs we did not interview, technologists, OpenTEAM community members, etc. In this community meeting we reviewed findings around data collection, management, and sharing, as well as challenges that we heard. We then asked the community to share challenges and insights of their own, and discussed the issue of interoperability. The challenges shared in the community meeting are as follows:

**Producers:**

-   Producers are hesitant to download new farm data tech that may not provide value to them

-   Lack of trust that tech companies can keep farmer data secure

-   Challenges surrounding Spanish-speaking farmworkers

    -   Potential solution: Spanish language voice to text and translator for farm tasks and observations

-   Staff turnover leading to poor data collection/management

    -   E.g. a team member setting up a technical tool that works great, but when that team member leaves, no one else knows how to use/access

-   Excel/Google Sheets basic knowledge is a barrier to effective use and basic training is a gap

    -   This is sometimes addressed at continued education events

-   Similar types of data existing in different places - data is stored in different ways: handwritten, PDF reports from labs, on different stakeholders\' portals, on producers different tech tools

-   The more diversified the farm is, the harder it can be to keep data in one spot

**TAPs:**

-   Outputs from proprietary tech tools can be in the incorrect format for TAP use

-   Lacking an easy way to share GIS/maps with notes across platforms

-   Photo use is an important way for TAPs to get information, there is not an easy way to share photos as a form of notes and link them to other data

-   Lack of historical data collection by producers can make it hard for TAPs to give accurate advice/assistance

**Non Farmer/TAP challenges**:

-   Many tools only work if enough other people are using them, incomplete data often isn\'t useful

-   Some tools are expensive and the investment in learning how to use them could be too much of a tradeoff. It appears this is especially true for ESRI or other spatial tools

-   Alignment between regulatory data collection and voluntary programs would support efficiency for producers and TAPs

## Section 4 Appendices

### 4A. What We Asked Producers

We developed an interview script that asked for information about the farmer/producer, the farm they currently are affiliated with, and some details about the farm, as well as including two primary topics of the interview: farm data and technology tools, and data sharing. The interviewer followed the script below, while also allowing for informal conversations on other topics that arose during the interview.

-   Introduction

    -   OpenTEAM intro

    -   Purpose of these interviews

    -   Our goals with this collaboration

-   About You and Your Farm

    -   Tell us about your farm:

    -   How long have you been farming?

    -   What type of operation do you have?

    -   What size is your operation (acres)?

    -   USDA defines small family farms as making "less than \$350,000 gross cash farm income," does your farm fit in this category?

    -   Do you have employees? If so, how many?

    -   What are your markets?

-   Defining "Farm Data" and tools

    -   What types of information do you collect on your farm?

        -   Who collects this info?

        -   What do you use this information for?

        -   When you hear 'Farm data' what do you think of?

            -   \[Prompt\] What are some examples?

    -   What types of tools do you use on your farm to collect data (examples could include, recording on paper, Excel, Google Drive, QuickBooks, Pasture Map, AgriWebb, Farm OS, Tend, etc.)

        -   What are the software/tech tool names?

        -   Do you use these tools through a mobile app, desktop computer, or on paper?

            -   Do you receive assistance in using these tools from others? Technical Assistance Provider? Family member? Employees?

        -   What are your goals in using these tools?

        -   What do you like or dislike about the tools you currently use?

        -   Are there tools you use in the field vs. in the office?

            -   What are they?

    -   What tools do you use to manage data? (if they are different than what you use to collect)

        -   Can you recall a time when you felt frustrated or confused while using a data management tool for your farm? What caused these challenges?

            -   Have you ever encountered situations where you felt your farm data was being used in ways you didn\'t fully understand or agree with? Describe the situation(s). How did you address this?

    -   How did you decide on your current farm data management system and tools?

        -   Did you try out other options or tools that did not work for you?

            -   Why did you like or dislike them?

-   Data Sharing

    -   For what purposes / benefits do you share your farm data?

    -   What are some pain points or challenges when sharing this data?

    -   What factors influence your decision to grant external parties access to your farm data? (ex. obligated to share contractually, trust, promised benefits, data security, hassle/overhead of sharing, etc)

        -   Who do you feel most comfortable sharing data with?

        -   What particular data do you most worry about sharing? Worry least?

    -   What (if any) measures do you take to ensure that your farm data remains secure and confidential when sharing it with external entities?

        -   \[Optional?\] Do you verify the credibility and trustworthiness of third-party tools or agencies before sharing your farm data with them? How? (provide an example of a third party if needed)

            -   How could these organizations increase your trust and willingness to share?

    -   What features or functionalities in your digital tools would make it easier for you to navigate and understand who has access to your farm data / for what purpose?

-   Closing

    -   Do you have any questions for us?

    -   We plan to share our findings from this work towards the end of July. We will have an open session on Zoom that you are welcome to attend...

    -   Thank you for your time.

### 4B. What We Asked TAPs

We developed an interview script that asked for information about the technical assistance provider's experience and relationships with producers, as well as including two primary topics of the interview: farm data and technology tools, and data sharing. The interviewer followed the script below, while also allowing for informal conversations on other topics that arose during the interview.

-   Intro

    -   OpenTEAM intro

    -   Purpose of these interviews

    -   Our Goals with this collaboration

-   About You/Farm/Org

    -   Tell us about your work as a Technical Assistance Provider: how long have you been working in this field? What type of services do you provide to farmers?

    -   How many farmers do you work with/serve per year?

    -   What are the average years of working relationships you have with your farmers? How frequently do you interact with them?

-   Defining "Farm Data" and data categories

&nbsp;

-   What do you think of as "Farm Data"?

    -   What types of data do you collect in your role? (ex. I collect sales data from farmers to help them increase their farm viability)

        -   Who collects this?

            -   If the answer is farmer: How is the farmer providing you with the data? (ex. on paper, USB drive, email, permissions through a tech tool?)

            -   If the answer is the TAP/oneself: why?

    -   What types of data management tools do you use when working with farmers (examples could include: Excel, Google Drive, QuickBooks, Farm OS, Tend, etc.)

    -   Do you use these tools through a mobile app, desktop, or on paper?

    -   What are your goals in using this method of data management?

    -   What do you like/dislike about the tools you currently use?

-   How did you decide on this current farm data management system and tools?

    -   Did you choose these tools or did someone else at your organization? Or were they mandated by some third party?

    -   Did you try out other options or tools that did not work for you?

        -   Why did you like or dislike them?

-   Data Sharing

&nbsp;

-   For what purposes/benefits do farmers and ranchers share data with you?

-   What are some pain points or challenges when requesting access to farm data?

&nbsp;

-   Do you receive pushback or concern when requesting specific types of data from farmers and ranchers?

    -   How do you go about addressing concerns from farmers and ranchers when requesting what could be considered sensitive information?

&nbsp;

-   What (if any) measures do you take to ensure that farm data that you have access to remains secure and confidential when sharing it with external entities?

    -   \[Optional\] Do you verify the credibility and trustworthiness of third-party tools or agencies before sharing data with them? How? (provide an example of a third party if needed)

-   What features or functionalities would make it easier for you to navigate and understand who has access to farm data / for what purpose within the tools that you use?

-   \[Optional\] Can you recall a time when a farmer or rancher you work with felt frustrated or confused while using a data management tool for their farm? What caused these challenges?

    -   Were you able to support them to address these frustrations?

    -   What challenges did you run into when trying to support this farmer or rancher?

-   \[Optional\] Do you feel that the farmers and ranchers you work with understand how their farm data is being used?

&nbsp;

-   Closing

    -   Do you have any questions for us?

    -   The next step is ...

    -   Thank you for your time.
