# 5. Perspectives from Ag Technologists

In parallel with our interviews of farmers and TAPs, we also carried out interviews with designers and builders of existing Ag Tech platforms and tools. In this section we describe our process for, and the learnings from, interviewing these Ag Tech providers. **The goal of these conversations was to better understand their varied approaches and attitudes towards:**

- Ag Data privacy, trust, and consent.

- Data sharing between tech tools. Current practices and desired future state.

- UI/UX learnings and best practices from their own work in Ag Tech and in managing Ag Data.

- The concept of a data wallet as a central experience for data stewardship and data sharing.

- Potential implications of a data wallet for their tech solution.

- Potential implications of this research on improving the user interface (UI) and user experience (UX) of existing Ag Tech.

## 5.1 Surveying the landscape

We started by mapping a wide range of organizations and projects working on Ag Tech, with a focus on finding a diversity of approaches to the management and use of Ag Data.** **This included large commercial farm management tools, alongside projects most likely to adopt farmer-friendly practices around trust, privacy and data management, open-source, and pro-social solutions. This resulted in a database with four main types of resource:

1. **Organizations** working on Ag Data related programs and projects. Some of these organizations produce their own software tools as well. 

2. **Software tools** and platforms that use or interact with Ag Data. 

3. **Information sources** like data standards and data use principles.

4. **Events**,** **such as conferences, where these topics are discussed. 

**Organizations****: **In our review of organizations that are thinking about or working on issues related to Ag Data use, we found a number of existing coalitions and alliances focused on improving practices around the handling of Ag Data. This includes organizations like the [Open Ag Data Alliance (OADA)](https://openag.io/), [Ag Data Coalition](https://agdatacoalition.org/), [Ag Gateway](https://aggateway.org/GetConnected/ADAPT(inter-operability).aspx), and the [Data Food Consortium](https://www.datafoodconsortium.org/en/). Some of these organizations, for example the Ag Data Coalition, are focused primarily on **education** around the value and use of Ag Data. Others are focused on influencing **policy** makers, such as the [Global Open Data For Agriculture and Nutrition (GODAN)](https://www.godan.info/). And others are focused on **interoperability** and data standards and schemas. 

**Software tools and platforms**: 

- **Farm management** tools, like commercial leader [John Deere Operations Center](https://www.deere.com/en/technology-products/precision-ag-technology/data-management/operations-center/), as well as open source platforms like [FarmOS](https://farmos.org/). 

- Tools that **consume farm data** to provide value to farmers, for example [Regen Farmers Mutual](https://www.regenfarmersmutual.com/) which helps farmers understand the value of their ecological assets and find strategies to capture that value. 

- **Back-end systems/frameworks** built to enable or support data sovereignty, e.g. [Solid](https://solidproject.org/), and Web3 solutions like [Kepler](https://www.keplr.app/) and [Holochain](https://www.holochain.org/). We did not prioritize interviews in this area, and the more technical aspects of data sovereignty, as this collabathon is focused on UI/UX. 

- Existing **digital wallets** like [MetaMask](https://metamask.io/), and the [Farm Worker Wallet OS](https://www.farmworkerwalletos.community/), from which we looked to understand UI/UX best practices.

**Information Sources****: **Our review also includes a compilation of other related resources, such as:

- Projects working on data management **best practices and principles** within tech, like [FAIR](https://www.go-fair.org/fair-principles/), [CARE](https://www.gida-global.org/care) and [TRUST](https://www.nature.com/articles/s41597-020-0486-7).   

- **Standards and schemas** related to agriculture data, such as [ISO](https://www.iso.org/home.html), [The Australian Farm Data Code](https://nff.org.au/programs/australian-farm-data-code/), [ADAPT](https://aggateway.org/GetConnected/ADAPT(inter-operability).aspx), and [AgroPortal](https://agroportal.lirmm.fr/)

- Standards and **libraries** for defining data schemas such as [JSON Schema](https://json-schema.org/), [Schema.](https://schema.org/docs/schemas.html)org and [Murmurations](https://murmurations.network/)

- Standards related to **data sovereignty** such as [Decentralized Identifiers](https://en.wikipedia.org/wiki/Decentralized_identifier) and [Verifiable Credentials](https://en.wikipedia.org/wiki/Verifiable_credentials). 

### 5.1.1 Gathering process

We gathered our resources in an Airtable, categorized by the four Resource types listed above. Additional fields included website URL, description, screenshots, estimated relevance to the ADW UI/UX Collabathon, and tags, to indicate things like whether the resource is focused on *education* or *interoperability*, whether it is *commercial* or *open source*, etc. For each of these categories we combined extensive web based research to collect the resources, alongside outreach to the wide networks connected to the convening team. We also held a community meeting to present our initial database, and gather additional resources from a range of additional people and organizations in the Ag space using a collaborative Miro board. 

### 5.1.2 Initial Assessment of the landscape

A few things stood out from an initial analysis of the resource library.

**Data sources and Data consumers: **We found some distinction between projects that primarily *generate *Ag Data, such as farm management tools, and ones that primarily *consume *Ag Data to offer services to farmers, such as COMET-planner and Open Food Network. There can be quite different ways of approaching trust, privacy and data management between these two types of projects. Of course some tools are clearly in both categories. 

**Standards & Interoperability: **There are a significant number of projects working on standards, schemas, and protocols to enable greater interoperability across Ag Tech. This includes projects with government connections like the The Australian Farm Data Code, coalitions like [Open Ag Data Alliance (OADA)](https://openag.io/), academic institutions like [OATS Center at Purdue](https://oatscenter.org/), and toolkits like [AgGateway’s ADAPT](https://adaptframework.org/). Much of this work also builds on general data standards work happening in the tech world such as JSON Schema and verifiable credentials 

**Education & Policy: **Many of the organizations we explored focus not on the development of technology but instead on educating producers or policy makers around Ag Tech, specifically around data use policies, data management, privacy, security, etc. This includes organizations like the Ag Data Coalition, Global Open Data For Agriculture and Nutrition (GODAN), and [MyData](https://mydata.org/). 

**Government involvement: **As the Agriculture space is of deep importance to societal wellbeing, there seems to be an outsized influence by government agencies in data management practices related to Ag Data and Ag Tech. There is a lot of government regulation in the space, as well as numerous government programs and subsidies that farmers interact with. 

**Impact of Web3: **Much of the conversation and experimentation happening around data sovereignty is in the web3/blockchain space, where numerous digital wallet projects already exist. This includes sovereign identity standards like Decentralized Identifiers (which are not solely a web3 technology), organizations like the Open Wallet Foundation and Startin’ Blox, data/app architectures like Solid and Holochain, and wallets like MetaMask. The Ag Tech space is only just beginning to explore what web3 has to offer, and we believe it is right to be cautious, as the user experience of interacting with blockchain and other web3 technologies is generally sub-par and not easily accessible to people with less digital literacy or time to learn new technologies. However, projects like the [Farm Workers Wallet OS](https://tac.openwallet.foundation/projects/fwos/) are showing that there are use cases for which web3 based decentralized storage and identity solutions can benefit producers. 

## 5.2 Interview process

From the above database we made an initial assessment of the relevance of each organization and tool and started outreach to schedule interviews with leaders of those projects. We conducted numerous interviews using a standard set of questions, as well as free form discussion. These conversations are recorded. At the end of each interview we asked for additional projects to include in our research.

### 5.2.1 Who we talked to

- Ag Tech researchers

- Ag Tech industry associations

- Ag Tech services/infrastructure providers

- Software providers for producer operations / farm management (via direct conversations, and in some cases users of the software)

    - Software providers for farm/ranch worker welfare

    - Software providers for certification processes

    - Software providers for TAP/TSP/extension support

    - Software providers for value added services

- Ag Data service providers

- A mix of Designers, Developers, Product Managers, and Executives

### 5.2.2 Biases and limitations

We held 17 interviews with 24 people. A number of groups we reached out to didn't respond.

Steve and Tibet were the primary interviewers, and did most of the synthesis and analysis of the interviews as well, so all of this is filtered through their biases.

### 5.2.3 Assumptions

Going into our conversations we had a few assumptions that we wanted to check:

- **Open source** projects are more likely to engender trust. We put particular focus on finding platforms and tools that share their code openly as that means the community can see exactly what the code is doing and call out anti-patterns like privacy or security issues. We also assumed that teams that care about open source are inherently more likely to care about best practices around privacy, security and data management in general. This relates to the Transparency principle described above. 

- **Non-profit** organizations/projects are more likely to follow best practices around data management than commercial platforms. We made sure to interview both commercial and non-profit organizations to check this assumption.

- That conversations around Ag Data standards and policies have not included producers' needs and perspectives at the forefront. 

### 5.2.4 What we asked

We used a standard list of questions, one for Ag Tech providers, and another for researchers and other leaders in the space. We also adapted our questions for each specific organization. Here is the standard list we started with for Ag Tech providers.

**Tell us about your tool:**

 - General overview and summary of what it does and how it works

- What is “farm data” in your tool?

- Who is using your tool?

- How many people/producers are using your tool?

- How do you market / bring in new users?

**Data Lifecycle:**

- Data Input

    - How do users currently input data into the tool

    - Does your tool receive farm data from other places?

    - How do you integrate with these other tools/services?

- Data Storage

    - How is data stored in your tool?

    - Is it encrypted?

- Data Sharing & Use

    - Who has access to what data in/from your tool?

        - Does your tool send farm data to other places?

        - What for?

    - How do you integrate with these other tools/services?

    - Does the data get sold or rented at all?

    - Do you use the data stored in your tool in any way internally?

    - What is your data sharing/use consent process?

    - What is the process for farmers to revoke access or delete their data from your tool? How could this process be improved?

**Terms of Service:**

- What terms are users required to agree to regarding how your tool uses their data? How clear / concise are they?

- Do you have a privacy policy? What does it cover?

- Do you use cookies or other data tracking tools? How do they interact with Ag Data?

**Design Influences:**

- What factors have influenced your decision around the tech stack and technical architecture of your tool?

- How do you make UI/UX decisions for your tool?

- What factors have influenced how you have designed the data management UX in your tool?

- What have you seen in the world that you learn(ed) from?

    - What do you like?

    - What don’t you like?

- How do you get user feedback on your tool?

- Do you solicit feedback and input on your data practices?

 **Vision for Trust in Ag Tech:**

- What do you see as critical to improving producer trust in technology?

- We’ve been using the phrase “Ag Data Wallet” as a stand-in for the notion of improving human-to-Ag Tech trust.... What do you think about this concept?

- What alternative language might you use to describe this idea?

- What would your ideal ADW do, in relation to your tool?

- **Screenshots: **We also gathered screenshots from some of the most relevant tools, to document and understand UI/UX approaches to data management across the ecosystem. 

## 5.3 Learnings

In our conversations, most organizations were very open and willing to share about their goals and their work, and interested in our approach. Here we have synthesized and summarized our general learnings from across the interviews, with some specific examples highlighted. **Note**: these learnings are phrased as insights about producers, but they are actually *the opinions of our interviewees, who were mostly ****not**** producers themselves.*

### 5.3.1 Trust & Privacy

Most everyone we talked to interfaced with producers in the design and marketing of their tool. They had many general observations about working with them.

Where does trust come from?
 - **Earning producers' trust is hard but essential.**

- Building trust requires time, transparency, and aligned interests

- Working through trusted partners provides inherited trust

 - Technical service and assistance providers are often the primary trusted partner they work with/through.

- Other farmers recommendations also hold a lot of weight

    - Farmer owned/governed organizations/projects are particularly trusted

- Producers might be willing to share data in exchange for knowledge or recommendations of value to them.

- Producers are also more likely to share their data for financial incentives, The negative side of this is that money can influence their judgment.

Privacy Concerns

- Producers have a lot of concerns that influence their trust of Ag Tech:

    - Their data might be used against them, in a lawsuit for example.

    - Their data could be stolen somehow, and/or their competition could see it.

    - They could be taken advantage of. This could happen through hidden loopholes, and/or by someone making money off their data,

    - Agreements could be broken or trust violated. This could happen by mistake, or by dishonesty.

    - For-profit businesses are considered generally less trustworthy.

      - One main issue is that an acquisition could mean that a once trustworthy organization no longer is.

    - They can be skeptical of institutions with differing politics

     - E.g. universities or the government

    - Working with government bodies means that a FOIA request could result in their data becoming public without their consent

- It’s not likely that someone doesn't want to share data on principle. Most often, hesitations around data-sharing arise from practical concerns  for producers.

    - “I don’t want to share data” can be a proxy for…

    - “I don’t want to deal with you”

    - “I don’t have time to think about this”. e.g. Burdensome requests to capture data

    - "What do I get out of it?"  i.e. Perceived cost/benefit analysis

    - Power differentials make it so value has to be big to outweigh risk of data being used against you

    - The digitization process is not capturing the things I care about

    - "The platform you're using isn't the one I use for my farm management, and I don't have time for a new platform."

**Key Learning: All this suggests that independent non-profits are often the most trustworthy to producers. Especially if there are **farmers as core stakeholders.**

*Open question: “How relevant are emerging codes of conduct in facilitating trust? Has commitment to a code of conduct affected your willingness (or ability) to do business?”*

One answer was: “they are useful for establishing shared values, less useful for actual dependable enforcement”

### 5.3.2 Data Input 

- Data input is difficult and time-consuming. It can take 100s of hours.

- There are many different data sources:

    - In producer’s heads

    - On paper

    - General digital documents or spreadsheets

    - Other tools/platforms

    - Never collected / unknown

- Automating data import from other sources can be messy and cause issues, so data is often collected many times, for each tool. Some providers think it’s too complex to integrate data from other sources - "Better off starting fresh"

- **API connections to import data **can help, and is something many folks want. But it is hard to prioritize for most providers; they usually don't have the resources to build it.

- **A standalone Ag Data Wallet could help simplify and standardize data input.**

*Open question/issue: Data ownership is complex, who owns the data? The entity who owns the ground? The equipment? The guy who paid for the service? Clear a*greements are important here.

### 5.3.3 Storage

- Users don’t want to host data themselves.

- There was some debate about the benefits of on-device data vs. cloud data. On-device is more secure/private, but the cloud is more convenient/reliable. Some producers prefer one or the other more.

- All data should be **exportable & deletable.**

- Some felt that tools/services should maintain the ability to export all original "raw" data out of a system if someone decides to stop using it.

    - But in this case they should make sure people know that data is always changed, losing its context and some of its value, when it is exported.

- Good security practices are generally expected, but important only to some producers. Often it becomes an issue only if there’s a breach.

- Some people care about encryption, others don’t. The ones who do are usually more technical folks, or those who have experienced privacy violations before.

- Laws/policies and **government standards** are very relevant to how data is stored, and vary by locality.

- Some producers care deeply about their info not having a chance of being revealed, so tools might **consider things like** **never entering farm metadata into a digital system, and location obfuscation.**

*Open Question*: Is *web3 (blockchains) valuable in this space? *The answer was not entirely clear to us, but it seemed less important in the short term. 

### 5.3.4 Sharing Data

- **Consent Management is at the core**. After trust, comes specific, informed consent - both parties should clearly understand the deal.

    - **What **specific data is being collected or provided?

    - To/**for who**?

    - For what **purpose?** Does this include further **sharing **with others?

    - For **how long?** (share just once, or ongoing)

    - Defining what data might be connected to Personally Identifiable Information as opposed to **aggregated**, or depersonalized

    - **Other conditions** / restrictions / commitments

- **Understanding the deal can be hard, **particularly because of legal jargon or obfuscating language. So a trusted advisor is often depended on to endorse a deal, for better or worse. When there is more trust in a platform/tool/org, this reduces the (perceived) need for diligence.

- **How to help people understand?**

    - Humans in the loop

    - Real world example use cases that are relevant to producers

    - Prompt for specific scopes of data as those scopes become important.

- Before data is shared, it may need to be proofed/QA’d. Consider the UX for this.

- “Set and forget” can be helpful: once a producer makes a choice, they don’t want to think about it all the time,

- Also important to **remind people of their choices and make it easy to change them**.

- Standards could create the necessary infrastructure to support efficient and effective sharing

- A standalone ADW could be valuable by enabling farmers to only have to input data once, and giving providers a standard way to import data from many sources. Also could offer secure data storage and simplified sharing dashboard.

- **Groups (e.g. coops) add another layer of support and complexity**

    - They allow for delegated deal understanding.

    - They provide/require collective deal negotiation

    - Members might be bound by collective decisions, or have individual opt-out rights

- **Selling data: **In most cases, just don't do it. 

- **Producers not all opposed to selling data, only opposed to not getting a cut**

    - There are good use cases for selling aggregated, anonymous data.

### 5.3.5 UI/UX & Design Patterns

Alongside these general learnings we synthesized some best practices from existing tools and providers.

- **Ease of Use: **Getting the User Experience right is important for true informed consent and privacy. e.g. GDPR notifications usually don't work or help, people get annoyed by them

- Things have to be **easy** because:

    - Farmers have no time

    - They don’t always have internet access

    - They may have low digital literacy

    - Complexity reinforces mistrust, simplicity implies transparency

- UX should be **invisible** (not in the way) until it’s needed, and then **obvious**

- Use lots of help text - explain everything that is going on.

- **Mobile centric** design with offline access is often a good idea because data input from the field, with low or no connection can be important. Cold/rain/gloves all impact ability to navigate a touch screen. Using voice in creative ways or extra large buttons are two examples of ways to approach this.

- Data input and data analysis are two very different things and need different UI/UX. Input may happen in the field using a mobile app, but analysis more often at a computer.

- **Familiarity:**

    - The **vocabulary** should be “farmer words” not “techie words”

    - **Consistency**: use consistent labeling (terms) and ordering to maximize understanding. Things with the same functionality should be similar, e.g. form layouts, button appearance, etc.

    - Use a *map-centric UI* for activities - show where they occurred

    - Use a *timeline UI* for relating activities over time, e.g. field or animal history

    - Lots of farms use spreadsheets and Airtable, which are super flexible. How do we play the **edge between simplicity and flexibility**?

    - *But we also heard “spreadsheets do not match the way farmers think”*

    - If you want to collect data from producers, try to **use a tool they’re already familiar with**. e.g. in India this is text messaging with Telegram

    - If you want researchers to understand your UX, look to things like Google Drive, DropBox, etc. for cues

- **Use “roles” to help define “rights”**, e.g. what roles have the power to share which data

    - Different types of people often have to access/use Ag Tech, especially TAPs

    - Grouping people together and controlling sharing via group membership and group privileges eases management

    - Roles and responsibilities/rights can also be assigned to API connections to other systems

- Good UX patterns help **informed consent**:

    - Clarify the bargain: What is negotiable? What can the user opt in and out of?

    - **Once data is shared, it’s hard to control it or take it back**, make sure people know that.

    - Present a **reminder of obligations** when accessing data

    - An **audit trail** to track who accessed what data, when, can be valuable.

- What happens when someone is fired or quits? Remove their access, but not their data

- **Use of AI**? Some are bullish on it; it could provide hyper relevant Ag advisory. It can integrate multiple data sources and deliver on the promise of an Ag extension.

    - Lots of folks using Retrieval Augmented Generation. Curating content and receiving shared, vetted content in the correct format is difficult. Can explore a lean RAG model.

    - “I think AI should only be used for 'mundane' data processing and management tasks and nothing else because it can introduce even more privacy and agency concerns that we are already facing.”

### 5.3.6 Focus on TAPs

In many cases **most work, transactions, and relationships around ag data happen through Technical Assistance Providers. **The UI/UX for TAPs can and should be different from the UI/UX for producers, because they are generally more technically experienced. Also because anyone who spends a lot of time in a tool starts to prefer efficiency over ease-of- use, if a tradeoff must be made. Finally, they are more likely to understand privacy and data issues, so they provide a layer of protection, both for farmers, and for the services. 

### 5.3.7 Tech Stacks

While not the focus of this collabathon, we did spend some time asking about the tech stacks used by various tools, and the ways that the tech infrastructure can enable or support better data management.

- **There is a big role**** of government here**: eAuth, FedRAMP, India state protocols, and even government-run servers can have a big impact on which services producers interact with, in what ways. 

- **Authentication**** and authorization**: **most people are using oAuth right now**. Scopes provide granularity around permissions and a built-in consent model that people understand.

- **Revocation is hard**. There is no standard way to remove consent, as revocation does not explicitly exist in oAuth though providers can build interfaces for it. 

- **Simple formats: **Lots of folks provide ways to import/export data using simple standard formats, like PDF and CSV.

- **Web3**: Not much adoption in Ag Tech yet. The UX of interacting with web3 is still too complex and confusing for most folks. Some providers are exploring the use of [Solid](https://solidproject.org/about) for sovereign data stores. Others are considering [Decentralized Identifiers](https://www.w3.org/TR/did-core/) for auth and [Verifiable Credentials](https://www.w3.org/TR/vc-data-model-2.0/) verification.

- **A successful Ag Data wallet likely requires agreed upon standards, protocols, & schemas for data connection and sharing**

## 5.4 Summary and the implications for an ADW 

Ag Data remains very **siloed and disconnected** in various software tools/platforms for producers and TAPs. However, it has **substantial latent value**, and it seems worth our time to try to facilitate increased trust, informed consent, and more data sharing across the ecosystem. This could give producers more stake in their Ag Data, and the ability to gain more value from it. 

There’s a real **risk of power imbalance** between producers and Ag Tech providers, leading to implicit coercion. An Ag Data Wallet would need to offer at least the same value as other service providers, alongside more data sovereignty. Or perhaps it can offer more value, with features like data portability and interoperability.

Many folks see the value in supporting & promoting more consistent and functional user experiences for data management across Ag Tech. **A consistent, high quality data management UX** would: 

- Allow producers to move data between Ag Tech tools with greater confidence.

- Increase understanding of choices and implications when sharing data, **reducing implicit coercion.**

- Reduce time spent in data management decisions and processes.

UI/UX best practices from this Collabathon could be valuable for both existing Ag Tech tools which incorporate data sharing capabilities, and a potential standalone Ag Data Wallet which could hold producer data and manage data sharing in a variety of use cases.

Conceptually, a standalone Ag Data Wallet could increase producer sovereignty and increase the likelihood of data sharing, for the benefit of all.

Ag Tech providers are** excited about the idea of receiving data input from a standalone wallet**, but less interested in delivering data to such a wallet. An Ag Data Wallet could provide them with instant integrations with many other platforms, but it was less clear what value they would get from exporting to it. 

## 5.5 Unanswered Questions

- Data ownership is complex: Who owns the data? The entity who owns the ground? The equipment? The guy who paid for the service? Clear agreements are important here.

    - It can be tricky to even identify who is the person with control/permission to provide data e.g. rented land with a custom operator coordinated by a farmer. Who all has to sign off on providing access to data

- How relevant are emerging codes of conduct in facilitating trust?

- How does this project relate to the USDA? They have an outsized impact on all of this in the USA. If they suggest a standard they want to use then that takes a bunch of variables out

## 5.6 Challenges for a standalone Ag Data Wallet

- Some providers are not keen on sending their data to other Ag Data repositories, because managing data is their business model

- We are unlikely to get big companies like John Deere and Climate on board with an Ag Data Wallet, unless maybe we provide a compelling use of new tech for them?

- Trying to build standard software components for existing tools may not be very useful, because there are too many different tech stacks.
