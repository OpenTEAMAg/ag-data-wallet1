# 8. Summary Guidance & Next Steps 

## 8.1 Principles

In this section, we build upon all of the above work to propose a set of principles for improving human-data interactions, with downstream effects on usability, transparency, and trust. We hope these themes are of utility to those engaged in the design and development of Ag Tech.

### 1. Be human-centered:
#### Understand the diversity of data practices among your current and intended users

To truly understand the needs and challenges within Ag Tech, it's crucial to go beyond assumptions and engage directly with the individuals who use these tools daily. Farmers, agronomists, technical assistance providers, and other stakeholders each have unique perspectives and workflows that need to be considered. By actively involving users in the design process and conducting thorough research, you can gain valuable insights into their data practices, pain points, and desired outcomes. This understanding will enable you to develop solutions that are not only effective but also user-friendly and tailored to their specific needs. Remember, technology should empower users, not burden them with unnecessary complexity or irrelevant features. Furthermore, consider how your current or planned user research can contribute to our collective understanding around concrete ag data management challenges, barriers to adoption of domain-specific technologies, and the perceived value and usability of Ag Tech. This principle is a derivation of a broader first principle of human-centered design (Moran, K, 2024), but we encourage thinking specifically about agriculture as knowledge work that relies on identifying shared conceptual models among diverse user groups.

### 2. Speak the languages of your users:
#### Improve the match between your technology and both natural languages and domain-specific vocabularies of your users

Effective communication is key to building trust and fostering understanding. In the context of Ag Tech, this means using language that is clear, concise, and accessible to all users, regardless of their background or expertise. Avoid technical jargon and complex terminology that may alienate or confuse non-technical users. Instead, strive to use plain language and familiar terms that resonate with your target audience. Additionally, be mindful of the diverse linguistic and cultural backgrounds of your users. Providing multi-lingual support and incorporating culturally relevant content can significantly enhance the user experience and promote inclusivity. Remember, technology should facilitate communication and collaboration, not create barriers due to language or cultural differences.

Key questions to consider:

-   How can you simplify and clarify the language used in your technology?

-   What steps can you take to ensure that your solutions are accessible to users with varying levels of literacy and technical expertise?

-   How can you incorporate multi-lingual support and culturally relevant content?

### 3. Design for real life:
#### Prioritize features based on the context of tech use

People use Ag Tech in very different contexts: Farmers may track animal weights during the move between pastures. Conservation specialists may collect soil samples in the field and type the location of the sample on their phone. A certifier may sit with a farmer at a kitchen table to review the past year of records. It's essential to consider these diverse contexts when designing and developing solutions. A mobile app that works seamlessly in the field may not be suitable for desktop use, and a tool designed for large-scale operations may be overwhelming for small-scale farmers. This principle builds on situated cognition theory (Suchman, 1987) that can be used to describe how the social, physical, and infrastructural context influences how, if at all, a particular technology is used, including in agriculture. That said, two common trends in Ag Tech use indicate the need for (a) responsive design, since people use varying devices, often older phones and tablets, and (b) design for low- or no-network connectivity via making data/or features available offline, to support usage in the fields. 3 ) Consider alternate user input methods such as voice for when hands are not available.

Key questions to consider:

-   How can you ensure that your solutions are adaptable to different environments and scenarios?

-   What features and functionalities are most important for different user groups and contexts?

-   How can you prioritize user needs and challenges in the design process?
### 

### 4. Improve user agency
#### Provide data stakeholders with agency, visibility, and control over their data.

Producers and TAPs have expressed a clear need for greater agency and transparency in how their data is accessed and used. It's crucial to design solutions that give users clear visibility into who has access to their data, for what purpose, and under what terms. This includes providing intuitive controls for granting, revoking, and modifying data access permissions. Additionally, users should be informed about the potential implications of data sharing, including the possibility of data being copied, aggregated, or used in ways they may not have anticipated. By empowering users with knowledge and control, you can foster trust and confidence in your technology. Remember, data should be a tool for empowerment, not a source of anxiety or uncertainty.

Key questions:

-   How can you provide users with clear and concise information about data access and usage?

-   What tools and controls can you offer to empower users to manage their data sharing preferences?

-   How can you educate users about the potential implications of data sharing, including the limitations of data deletion and revocation?

### 5. Reduce data friction
#### Create seamless data collection and management workflows.

Producers and TAPs face significant challenges in efficiently collecting and managing farm data. These challenges are exacerbated by the diversity of farming practices, collaborative workflows, and the need for digitization. To improve stakeholder outcomes, Ag Tech solutions should prioritize seamless data collection and management workflows. This includes:

-   **Understanding on-farm data collection:** It would benefit the Ag Tech community to work towards a shared community-wide understanding of the needs and challenges of on-farm or in-field data collection, as well as how digitization processes may motivate or influence the adoption (or not) of specific technologies or tools.

-   **Leveraging technology to reduce friction:** Implement technologies such as speech-to-text, image-to-text, and language translation to simplify data entry and improve accessibility for diverse users.

-   **Prioritizing early-stage data lifecycle:** Focus on optimizing the initial stages of data collection and management, as this is where many pain points and inefficiencies occur.

-   **Considering the collaborative context:** Design solutions that support collaboration among farmers, farmworkers, and TAPs, enabling seamless data sharing and communication.

By addressing these key areas, AgTech solutions can significantly reduce data friction, empowering producers and TAPs to collect, manage, and utilize farm data more effectively. This will ultimately lead to improved decision-making, enhanced productivity, and greater overall success in agricultural operations.

Key questions to consider:

-   How can you streamline and simplify the on-farm data collection process?

-   What technologies and tools can you leverage to reduce data friction and improve accessibility?

-   How can you design solutions that support collaboration and data sharing among stakeholders?

### 6. Don't be (too) original
#### Implement consistent and familiar interaction paradigms in technology.

Producers and TAPs are often juggling multiple tools and platforms, and the cognitive load of learning new interaction paradigms for each one can be overwhelming. By prioritizing consistency and familiarity in your design, you can reduce friction and improve the overall user experience. This means using established design patterns from existing tools and platforms, standard terminology, and intuitive navigation structures whenever possible. While innovation is important, it should never come at the expense of usability. Remember, the goal is to empower users to accomplish their tasks efficiently and effectively, not to impress them with flashy but unfamiliar features. *Example:* A confirmation dialog to delete a data element should have the same text and button placement each time. However, if the user chooses to delete *all* their data, the confirmation dialog should be visually distinct, highlighting the significance of the action.

Key considerations:

-   How can you leverage established design patterns and conventions to create a familiar and intuitive user experience?

-   What steps can you take to ensure consistency in terminology, navigation, and visual elements across your platform?

-   How can you balance innovation with usability, ensuring that new features are easy to learn and use?

### 7a. Actively engage in standards development
#### Participate and/or take leadership in data standardization efforts across sectors and disciplines.

There are many standards across sectors and disciplines that have the potential to improve interoperability and the overhead of Ag Data management. Invested participation from Ag Tech developers, users, and other stakeholders, is critical to ensure that such standards are designed to honor producer sovereignty, transparency, and reciprocity.

Examples of community-led data standards development in Ag Tech include work by [Ag Data Transparent](https://www.agdatatransparent.com/) and [Ag Gateway](https://aggateway.atlassian.net/wiki/spaces/AG/overview?mode=global), where Ag Gateway's [MODUS Agricultural Lab Test Data Standard](https://aggateway.atlassian.net/wiki/spaces/MOD/overview) serves as an exemplar of an exemplary user-friendly and implementable standard. Increased and active collaboration of common Ag Tech standards to govern data in Ag Tech can improve software qualities such as interoperability, data portability, and application portability. However, the language and guidance around how to measure and improve such software qualities has historically been standardized by general technology organizations such as the ISO, IEC, and IEEE, e.g., ISO/IEC 19941:2017 standard provides shared terminology on cloud computing interoperability and portability.

Example: Without standards, the data requirements for certification are variable and create lots of barriers for farmer data collection collecting all that data. Certifiers ask for all kinds of data, which drives a backwards process whereby specific data is collected only for the specific needs of that certifier. It would be much easier on producers if they knew they only had to collect a standard set of data, which could be shipped out to various certifiers. Efforts to bring together the ecosystem of certifiers to work a set of standards would be beneficial to all stakeholders.

Key questions to consider:

-   How can you contribute to the Ag Tech ecosystem committing to the use of existing standards more?

-   How can you contribute to the development of new standards where necessary?

-   Whose responsibility is it to drive development and adoption of standards?

### 7b. Make data machine-translatable
#### Identify and develop shared utilities for automatable data flows, from data schemas to common APIs.

The future of AgTech hinges on the ability to seamlessly exchange data between different tools and platforms. Producers and TAPs currently face significant challenges due to the lack of standardized data formats and the difficulty of manually transferring information. By actively participating in the development and adoption of shared data schemas and common APIs, AgTech developers can unlock the potential for greater interoperability and significantly reduce data friction. This will not only streamline workflows and improve efficiency but also empower users to leverage their data more effectively. Embracing standardization and open data exchange mechanisms is not just a technical consideration; it's a strategic imperative for fostering innovation, collaboration, and ultimately, a more connected and user-centric AgTech ecosystem.

Key Considerations:

-   How can AgTech stakeholders collaborate to establish and promote shared data schemas and APIs?

-   What are the key barriers to data interoperability in the current AgTech landscape?

-   How can we ensure that data exchange mechanisms are designed with security, privacy, and user control in mind?

### 8. Make policies human-readable
#### Improve understandability, consistency, and accessibility in data policies. 

Starting with Terms of Service, License Agreements, and Privacy Policies, the legal language which governs the relationship between tech developers and their users needs to be clear, well-organized, and must not attempt to hide undesirable deal elements or reserve unneeded rights. Ideally, we move toward shared policy structures and standardized documents, with clear rationale for deviations, to reduce time, money, and effort on behalf of all parties - technology providers AND technology users.

Key questions to consider:

-   How can you simplify and clarify the language used in your policies, avoiding legal jargon and complex terminology, to ensure that your policies are accessible to users with varying levels of literacy and legal expertise?

-   In what ways can you involve users in the development and review of your data policies, ensuring their voices are heard and their concerns are addressed?

-   How can you contribute to industry-wide efforts to standardize data policies and promote greater transparency and consistency across the AgTech ecosystem?

### 10. Responsibly prioritize open knowledge
#### Consider how to communicate your use of and contribution to open knowledge, including open source software and open datasets.

Those in the OpenTEAM community are generally predisposed to creating open source technology and seeking to support and leverage open source assets. *Even if your situation seems to prevent working in the open*, look for pieces of your technology that do not provide a critical competitive advantage to test open development within your organization. Contribute to open source projects which provide you with critical functionality.

As you collect data with your users, consider whether that data can be anonymized and distributed safely for the benefit of broader research efforts and to advance our understanding of agricultural practices and their impact on productivity and ecosystems.

Key questions to consider:

-   What aspects of your technology or data could be shared openly without compromising your competitive advantage?

-   How can you contribute to existing open source projects that align with your goals and values?

-   What steps can you take to anonymize and share data responsibly, while protecting user privacy and ensuring data integrity?

-   How can you clearly communicate your commitment to open knowledge and the benefits it brings to the broader community?

