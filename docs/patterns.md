# 6. UX Design Patterns

## 6.1. OVERVIEW

UX patterns are reusable solutions to common design challenges. They offer generalized frameworks that can be implemented as is, or customized to improve user experiences in digital products. In the agricultural technology (Ag Tech) context, these patterns provide practical recommendations to address recurring usability issues encountered by product teams.

The Ag Data Wallet (ADW) UI/UX Collabathon developed the Ag Tech UX Pattern Catalog to guide anyone tackling usability challenges or building new digital solutions for the agricultural sector. For this initial release, we’ve selected five key patterns that we believe will be most valuable to our primary audience: Ag Tech implementers. We envision this as a format that OpenTEAM’s community can iterate on, adding patterns as we discover or encounter them in our research and development efforts.

### 6.1.1. How to use this

- Nonlinear Navigation: Feel free to explore topics in any order. The guide is not meant to be read sequentially.

- User-Centered Design Reference: The selected UX patterns are based on widely accepted UX design principles but may need to be adapted to specific user contexts

### 6.1.2. Goals & audience

- Primary: communicate design recommendations to **Ag Tech implementers,** such as software engineers, product managers, and UI/UX designers.

- Secondary: communicate software best practices to Ag Tech end-users, such as farm managers and technical assistance providers.

### 6.1.3. Background (ADW UI/UX Collabathon)

The ADW UI/UX Collabathon operates on the belief that a human-centered, consistent approach to tech design can build trust between Ag Tech users, developers, and stakeholders. Drawing from research in sectors like e-commerce and financial technology, we identified key themes that influence technology adoption: ***privacy, security, transparency, agency, usability, usefulness, and familiarity.***

This initial Ag Tech UX catalog was informed by user research and interviews, where farmers and technical assistance providers emphasized the need for transparency and user agency throughout the data lifecycle. The patterns aim to guide the development of the ADW UX and serve as a resource for other Ag Tech implementers.

### 6.1.4. Terms

| Terms  | DEFINITIONS  |
|---|---|
| UX pattern | UX patterns, or UX design patterns, are reusable solutions to common usability problems encountered in user experience and user interface design. They serve as universal building blocks that help product teams address recurring design challenges and create a sense of familiarity for users navigating digital products or web pages. |
| Users | Refers to end-users of technical tools. E.g. farmers, TAPs, etc. |
| Data/Resource or online resources | Refers to data, dataset, or other digital resource (webpage, code, etc) |
| Data/Resource owner | A person who created, collected, and owned farm data. E.g. farmers who collected farm data for their operation.   |
| Farm data manager | A farm staff/worker who is responsible for managing data about any aspect of a farm or ranch. |

### 6.1.5. Design Pattern Presentation

Each design pattern consists of the following elements:

- Title: an illustrative name.

- Problem Statement: a summary of the common elements of the problem found across multiple Ag Tech implementations.

- Use Cases: scenarios and use cases in the agricultural technology context.

- Description: overview of the design pattern which can address the identified problem.

- Examples: screenshots from existing tech tools which implement the design pattern.

- Themes: corresponding UX themes from the collabathon’s landscape review report.

## 6.2. UX PATTERNS:

### 6.2.1. Make connected services explicit

#### Problem Statement

Applications often make it challenging for users to track and monitor who has access to their data and how their data is flowing across a set of connected software applications. This is further complicated as terms of service evolve without users’ explicit awareness or engaged consent.

The phrase “connected services” refers to the paradigm of “integrations”, where software developers build connections among tools to enable data sharing, shared authentication, and other shared services across a suite of apps. These types of integrations can improve interoperability among software, or reduce the number of accounts a user needs to access multiple software systems. However, when implemented silently, that is, without explicit user interface components to communicate connections among services, this paradigm can also confuse users concerning which software has access to which data.

This challenge is made more difficult by the lack of a clear, centralized view within the account settings of each connected service. Users often struggle to identify which services are accessing their data through APIs (automated data connections), and the information is often scattered or buried in complex interfaces. Without an intuitive way to view and manage these connections, it is difficult to monitor, control, or revoke access to a user’s data. As a result, users remain unaware of how their data is shared, often leaving them without the power to manage or consent to ongoing data use by third parties.

##### Ag tech use cases 

- **Farmers** who adopt multiple farm data management tech tools can streamline their operations and insight gathering by setting them to share data via APIs.

- **Technical assistance providers (TAPs)** use multiple tech tools for day-to-day operations and client management. A data flow between their Customer Relationship Management (CRM) software and other related tech tools can make them more effective and transparent.

#### Description

Displaying all connected services (via API or other means) in a dedicated section of the settings interface enables users to easily monitor and manage their data-sharing activities. By providing clear details about which services have access to their data and what specific information is being shared, users gain transparency and control over their data.

Additionally, offering users the ability to revoke or modify data access directly from this list simplifies data-sharing permission management, enhancing security and boosting user empowerment.

#### Examples

1. **John Deere Operations Center**

Users can view the list of connected services (via API) from Access UI at a glance. They can view connection details by opening the item from the list.

[John Deere Operations Center - API access list in Access page]

![Enter image alt description](img/UX1.png)

[John Deere Operations Center- API Connection details revealed]

![Enter image alt description](img/UX2.png)

2. **Google Account Data & Privacy**

Users can view the list of connected services at a glance.

[Google’s connected third-party apps & services/apps list]

![Enter image alt description](img/u89_Image_5.png)

[Google’s connected third-party apps - The connected service details view shows how it works and what data the third party can access.]

![Enter image alt description](img/AAY_Image_6.png)

[Google’s connected third-party apps - Additional details about access and the way to revoke action]

![Enter image alt description](img/aM4_Image_7.png)

#### Themes

* Transparency ,  Privacy ,    Agency


### 6.2.2. Provide user-friendly data access controls

#### Problem Statement

Data owners should have a simple and direct way to grant trusted individuals access to their data or resources. Requiring authentication for new platforms or sending access requests through unfamiliar channels can create barriers for recipients, making it harder to grant or receive access.

##### Ag tech use cases

Sometimes a technical assistant provider (TAP) collects farm data on behalf of a farmer. The TAP can create a private access link to a spreadsheet containing the farm data and share it with the farmer. This allows the farmer to easily verify the data without needing to navigate complex tools or authentication processes.

#### Description

Granting access to one's private data or resources can be managed in several ways. A common UX pattern involves generating a private access link (also known as an unguessable URL) and sharing it with trusted individuals who have permission to view or use the resource. This method allows for quick and convenient access without requiring formal authentication, making it simple and user-friendly. The link can be shared through existing communication channels like email or messaging apps, minimizing friction for both the data owner and recipient.

To mitigate the risk of unintended sharing, the data or resource owner can modify permissions at any time. Additional security measures, such as setting expiration dates for the link or requiring a password for access, can further enhance control and protect the resource.

#### Examples

1. **Dropbox’s private link-sharing setting**

Resource owners can create a public and/or private link to share it with others. Dropbox also offers access time constraints and password protection features.

![Enter image alt description](img/0sC_Image_8.png)

1. **Google Sheets**

Users can grant access to their online spreadsheet from the share setting dialog and copy the link.

![Enter image alt description](img/WcV_Image_9.png)

#### Themes

*  Agency ,  Security


### 6.2.3. Manage roles and permissions

#### Problem Statement

Data owners require a structured approach to collaborate with various stakeholders, each with distinct roles and responsibilities in managing and sharing data. A data management system that provides clearly defined roles and permissions enables more effective collaboration, ensuring that tasks are carried out efficiently while preventing unauthorized actions or overstepping of boundaries. This clarity fosters trust and smoother workflows, allowing everyone to focus on their specific responsibilities within the system.

##### Ag tech use cases

Data owners can assign different roles to farm workers for data collection and management within a centralized system. By assigning specific roles, the manager can prevent unauthorized data modification while enabling broader collaboration. For instance, a farm viability adviser (a type of technical assistance provider) can be given Viewer or Commenter access to review financial data and provide tailored advice without altering the data itself.

#### Description

When sharing a file or resource with others, the data owner can assign specific roles that define how users can interact with the shared content. Common roles include:

- Viewer: Can only view the file without making any changes.

- Commenter: Can view the file, suggest changes, and add comments but cannot edit the content.

- Editor: Has full editing rights, allowing changes to be made.

- Owner: The original creator or owner, with the ability to manage access, change roles, and control permissions for other users.

The level of role granularity varies depending on the product or platform and may have context-specific role names. Data owners can revoke access or adjust roles at any time, ensuring flexibility in managing collaborations. Access and roles are typically assigned by entering the recipient’s email and selecting their permissions. Once assigned, the individual receives a notification and can begin fulfilling their designated responsibilities.

#### Examples

1. **Google Sheets  - File Share**

Data owners can share and govern data with assigned roles in Google Workspace. Limiting the edit privilege can allow the owner to share the data more broadly when needed.

![Enter img alt description](img/3F5_Image_10.png)

**B. Terraso Story Maps**

The owner of a story map can invite others to collaborate on their story with the editor role. Invited collaborators accept the role and can add chapters, text, photos, and videos to the story. But they cannot delete the story map itself.

![Enter img alt description](img/7Xz_Image_11.png)

#### Themes

* Transparency  ,  Privacy ,  Agency


### 6.2.4. Export resources

#### Problem Statement

Data owners often need to export their data from one SaaS (Software as a Service) tool to another to meet their specific needs. When this process is difficult or unavailable, users can feel trapped within the tool, diminishing their sense of control and flexibility over their own data.

##### Ag tech use cases

Farm collaborators collect data from the field using mobile devices and the farm data manager exports the data for further processing and analysis to gain insights.

A farm data manager exports a subset of the dataset and forwards it to external stakeholders, such as financial institutions or government agencies, for reporting or compliance purposes.

#### Description

The ability to export or download one’s own data is a common user expectation. This functionality allows users to process their data using different tools, enabling greater flexibility in how they manage and analyze it. Downloading data also provides users with a local copy, which is useful for offline work or backup purposes.

**Supported file formats**

The file formats supported for export depend on the tool's context and the type of data involved. Users typically expect to export tabular data like spreadsheets as CSV files and images as JPEG files, at a minimum. Offering multiple file format options enhances the user experience by accommodating diverse use cases and technical needs.

**Permission to export/download**

For data or resources shared by multiple collaborators, the data owner or administrator should have the ability to control export/download permissions based on user roles. This ensures proper data governance and security while enabling collaborative work.

#### Examples

**A.Google Maps**

Users can export custom geospatial data (points, polygons, etc.) as KML/KMZ or CSV.

![Enter img alt description](img/EFo_Image_12.png)

**B. Google Sheets**

Users have 6 options to export the spreadsheet in Google Sheets. Google apps don’t differentiate between export and download functionalities.

![Enter img alt description](img/ZyW_Image_13.png)

**C. Microsoft 365 Excel**

** **Users can export their Excel spreadsheet as CSV or PDF.  

![Enter img alt description](img/kwm_Image_14.png)

**D. Miro: Frame**

Users can export the selected frame as PDF, JPG img, or CSV file from the virtual whiteboard SaaS tool.

![Enter img alt description](img/KwK_Image_15.png)

#### Themes

* Transparency ,  Privacy ,   Agency

### 6.2.5. Convert resource format to meet the destination’s requirements

#### Problem Statement

When sending data or files to other software, users often face the cumbersome task of manually verifying and adjusting the file format to match the requirements.

Mismatched file formats often result in data transfer failures, causing delays and additional work for users.

##### Ag tech use cases

Farmers need a simple way to forward their data to external parties, such as certifiers, in the correct format for each recipient. Automating the packaging and formatting of data for repeated submissions significantly reduces the effort involved in applying for certifications and ensures successful transfers without manual intervention.

#### Description

When sending data to external entities, ensuring it is in the correct format is essential for successful transfers. Users need a way to package and send their data in the appropriate format for third-party requirements. By automatically setting the outgoing data format to match the destination's specifications, the system removes the need for users to manually adjust settings for recurring or ongoing transfers. This automation simplifies the process, reduces errors, and enhances efficiency, allowing users to dedicate less time to data formatting.

#### Examples

**A. Apple Photo**

"Automatic" image file format option is designed to choose the best file format when transferring or sharing photos, based on the compatibility and capabilities of the receiving device or platform.

[Apple Photo - Picture format can be determined by the destination ]

![Enter image alt description](img/pij_Image_16.png)

**B. TurboTax**

TurboTax stores entered personal, financial, and tax information in a proprietary format (e.g. “.tax2023”), but as befits its intended purpose, it includes the ability to generate a PDF file that formats the tax information and associated calculations in a form acceptable to federal and state tax authorities (File -> Save to PDF…). It also provides the ability to deliver tax information to many tax authorities electronically, sometimes for an extra fee (File -> Electronic Filing -> File Electronically). Lastly, it provides the ability to generate a special file for sending to product help agents which keeps the original file structure but anonymizes all personal information (Online -> Send Tax File to Agent).

NOTE: Although TurboTax makes data sharing explicit, and has a privacy policy that appropriately limits their use of your personal data, it’s worth noting that TurboTax does *not* provide user data export in a portable or standard form, *deliberately* attempting to lock users into a pattern of annual fees and purchases. Indeed, it has been [reported ](https://www.propublica.org/article/inside-turbotax-20-year-fight-to-stop-americans-from-filing-their-taxes-for-free)that the company has actively lobbied against tax simplification which would remove the need for its product entirely.

[TurboTax - Saving proprietary format to PDF to match the tax authorities' specifications]

![Enter image alt description](img/1ls_Image_17.png)

[TurboTax - Filing tax electronically from stored tax information]

![Enter image alt description](img/wY0_Image_18.png)

[TurboTax - Generating a special file to send agents]

![Enter image alt description](img/k0N_Image_19.png)

#### Themes

* Transparency  ,    Agency 