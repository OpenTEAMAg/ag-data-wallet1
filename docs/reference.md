# 9. References

> Bhimani, A. (1996). Securing the commercial Internet. *Commun. ACM*, *39*(6), 29--35. [https://doi.org/10.1145/228503.228509](https://doi.org/10.1145/228503.228509)
>
> Bodó, B. (2020). Mediated trust: A theoretical framework to address the trustworthiness of technological trust mediators. *New Media & Society*, 1461444820939922. [https://doi.org/10.1177/1461444820939922](https://doi.org/10.1177/1461444820939922)
>
> Boehner, K., Vertesi, J., Sengers, P., & Dourish, P. (2007). How HCI interprets the probes. *Proceedings of the SIGCHI Conference on Human Factors in Computing Systems*, 1077--1086. [https://doi.org/10.1145/1240624.1240789](https://doi.org/10.1145/1240624.1240789)
>
> Carroll, S. R., Garba, I., Figueroa-Rodríguez, O. L., Holbrook, J., Lovett, R., Materechera, S., Parsons, M., Raseroka, K., Rodriguez-Lonebear, D., Rowe, R., Sara, R., Walker, J. D., Anderson, J., & Hudson, M. (2020). The CARE Principles for Indigenous Data Governance. *Data Science Journal*, *19*, 43. [https://doi.org/10.5334/dsj-2020-043](https://doi.org/10.5334/dsj-2020-043)
>
> Cogeca, C. (2018). *EU Code of conduct on agricultural data sharing by contractual agreement* (p. 11) \[Guidelines\]. [https://www.copa-cogeca.eu/img/user/files/EU%20CODE/EU_Code_2018_web_version.pdf](https://www.copa-cogeca.eu/img/user/files/EU%20CODE/EU_Code_2018_web_version.pdf)
>
> Davis, F. D. (1989). Perceived Usefulness, Perceived Ease of Use, and User Acceptance of Information Technology. *MIS Quarterly*, *13*(3), 319--340. [https://doi.org/10.2307/249008](https://doi.org/10.2307/249008)
>
> de Beer, J. (2017). *Ownership of Open Data: Governance Options for Agriculture and Nutrition* (p. 13). Wallingford: Global Open Data for Agricultural and Nutrition \| University of Ottawa. [https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3015958](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3015958)
>
> Duncan, E. (2018). *An Exploration of how the Relationship between Farmers and Retailers influences Precision Agriculture Adoption* \[University of Guelph\]. [http://hdl.handle.net/10214/13546](http://hdl.handle.net/10214/13546)
>
> Falcone, R., & Castelfranchi, C. (2001). Social Trust: A Cognitive Approach. In C. Castelfranchi & Y.-H. Tan (Eds.), *Trust and Deception in Virtual Societies* (pp. 55--90). Springer Netherlands. [https://doi.org/10.1007/978-94-017-3614-5_3](https://doi.org/10.1007/978-94-017-3614-5_3)
>
> Fleming, A., Jakku, E., Lim-Camacho, L., Taylor, B., & Thorburn, P. (2018). Is big data for big farming or for everyone? Perceptions in the Australian grains industry. *Agronomy for Sustainable Development*, *38*(3), 24. [https://doi.org/10.1007/s13593-018-0501-y](https://doi.org/10.1007/s13593-018-0501-y)
>
> Furnell, S. M., & Karweni, T. (1999). Security implications of electronic commerce: A survey of consumers and businesses. *Internet Research*, *9*(5), 372--382. [https://doi.org/10.1108/10662249910297778](https://doi.org/10.1108/10662249910297778)
>
> Gambetta, D. (Ed.). (1988). *Trust. Making and Breaking Cooperative Relations*. Basil Blackwell Ltd.
>
> Gefen, D., Karahanna, E., & Straub, D. W. (2003). Trust and TAM in Online Shopping: An Integrated Model. *MIS Quarterly*, *27*(1), 51--90. [https://doi.org/10.2307/30036519](https://doi.org/10.2307/30036519)
>
> Goodwin, C. (1991). Privacy: Recognition of a Consumer Right. *Journal of Public Policy & Marketing*, *10*(1), 149--166. [https://doi.org/10.1177/074391569101000111](https://doi.org/10.1177/074391569101000111)
>
> Herder, E., & van Maaren, O. (2020). Privacy Dashboards: The Impact of the Type of Personal Data and User Control on Trust and Perceived Risk. *Adjunct Publication of the 28th ACM Conference on User Modeling, Adaptation and Personalization*, 169--174. [https://doi.org/10.1145/3386392.3399557](https://doi.org/10.1145/3386392.3399557)
>
> Hou, F., & Jansen, S. (2022). A systematic literature review on trust in the software ecosystem. *Empirical Software Engineering*, *28*(1), 8. [https://doi.org/10.1007/s10664-022-10238-y](https://doi.org/10.1007/s10664-022-10238-y)
>
> Jakku, E., Taylor, B., Fleming, A., Mason, C., Fielke, S., Sounness, C., & Thorburn, P. (2019). "If they don't tell us what they do with it, why would we trust them?" Trust, transparency and benefit-sharing in Smart Farming. *NJAS - Wageningen Journal of Life Sciences*, *90--91*, 13. [https://doi.org/10.1016/j.njas.2018.11.002](https://doi.org/10.1016/j.njas.2018.11.002)
>
> Jakob, N. N. (n.d.). *Usability 101: Introduction to Usability*. Nielsen Norman Group. Retrieved July 15, 2024, from [https://www.nngroup.com/articles/usability-101-introduction-to-usability/](https://www.nngroup.com/articles/usability-101-introduction-to-usability/)
>
> Kim, D. J., Ferrin, D. L., & Rao, H. R. (2008). A trust-based consumer decision-making model in electronic commerce: The role of trust, perceived risk, and their antecedents. *Decision Support Systems*, *44*(2), 544--564. [https://doi.org/10.1016/j.dss.2007.07.001](https://doi.org/10.1016/j.dss.2007.07.001)
>
> Lewis, J. D., & Weigert, A. (1985). Trust as a Social Reality. *Social Forces*, *63*(4), 967--985. [https://doi.org/10.1093/sf/63.4.967](https://doi.org/10.1093/sf/63.4.967)
>
> Lin, D., Crabtree, J., Dillo, I., Downs, R. R., Edmunds, R., Giaretta, D., De Giusti, M., L'Hours, H., Hugo, W., Jenkyns, R., Khodiyar, V., Martone, M. E., Mokrane, M., Navale, V., Petters, J., Sierman, B., Sokolova, D. V., Stockhause, M., & Westbrook, J. (2020). The TRUST Principles for digital repositories. *Scientific Data*, *7*(1), 144. [https://doi.org/10.1038/s41597-020-0486-7](https://doi.org/10.1038/s41597-020-0486-7)
>
> Luhmann, N. (2018). *Trust and Power*. John Wiley & Sons.
>
> Marangunić, N., & Granić, A. (2015). Technology acceptance model: A literature review from 1986 to 2013. *Universal Access in the Information Society*, *14*(1), 81--95. [https://doi.org/10.1007/s10209-014-0348-1](https://doi.org/10.1007/s10209-014-0348-1)
>
> Mayer, R. C., Davis, J. H., & Schoorman, F. D. (1995). An Integrative Model of Organizational Trust. *The Academy of Management Review*, *20*(3), 709--734. [https://doi.org/10.2307/258792](https://doi.org/10.2307/258792)
>
> Moran, K. (n.d.). *The Aesthetic-Usability Effect*. Nielsen Norman Group. Retrieved July 15, 2024, from [https://www.nngroup.com/articles/aesthetic-usability-effect/](https://www.nngroup.com/articles/aesthetic-usability-effect/)
>
> OpenTEAM Working Group Team. (2023, July). *OpenTEAM Ag Data Glossary*. [https://openteam-agreements.community/TermsandDefinitions/](https://openteam-agreements.community/TermsandDefinitions/)
>
> Raturi, A., Thompson, J. J., Ackroyd, V., Chase, C. A., Davis, B. W., Myers, R., Poncet, A., Ramos-Giraldo, P., Reberg-Horton, C., Rejesus, R., Robertson, A., Ruark, M. D., Seehaver-Eagen, S., & Mirsky, S. (2022a). Cultivating trust in technology-mediated sustainable agricultural research. *Agronomy Journal*, *114*(5), 2669--2680. [https://doi.org/10.1002/agj2.20974](https://doi.org/10.1002/agj2.20974)
>
> Raturi, A., Thompson, J. J., Ackroyd, V., Chase, C. A., Davis, B. W., Myers, R., Poncet, A., Ramos-Giraldo, P., Reberg-Horton, C., Rejesus, R., Robertson, A., Ruark, M. D., Seehaver-Eagen, S., & Mirsky, S. (2022b). Cultivating trust in technology-mediated sustainable agricultural research. *Agronomy Journal*, *114*(5), 2669--2680. [https://doi.org/10.1002/agj2.20974](https://doi.org/10.1002/agj2.20974)
>
> Rotz, S., Duncan, E., Small, M., Botschner, J., Dara, R., Mosby, I., Reed, M., & Fraser, E. D. G. (2019). The Politics of Digital Agricultural Technologies: A Preliminary Review. *Sociologia Ruralis*, *59*(2), 203--229. [https://doi.org/10.1111/soru.12233](https://doi.org/10.1111/soru.12233)
>
> Rousseau, D. M., Sitkin, S. B., Burt, R. S., & Camerer, C. (1998). Not so Different after All: A Cross-Discipline View of Trust. *The Academy of Management Review*, *23*(3), 393--404. [https://doi.org/10.5465/amr.1998.926617](https://doi.org/10.5465/amr.1998.926617)
>
> Russel Hardin. (2002). *Trust and Trustworthiness*. Russell Sage Foundation. [https://www.russellsage.org/publications/trust-and-trustworthiness-1](https://www.russellsage.org/publications/trust-and-trustworthiness-1)
>
> Singh, S., Sahni, M. M., & Kovid, R. K. (2020). What drives FinTech adoption? A multi-method evaluation using an adapted technology acceptance model. *Management Decision*, *58*(8), 1675--1697. [https://doi.org/10.1108/MD-09-2019-1318](https://doi.org/10.1108/MD-09-2019-1318)
>
> Slattery, D., Rayburn, K., & Slay, C. M. (2021). *Farmer Perspectives on Data*. Trust in Food, a Farm Journal Initiative & The Sustainability Consortium. [https://www.trustinfood.com/wp-content/uploads/2020/05/Farmer-Data-Perspectives-Research_final.pdf](https://www.trustinfood.com/wp-content/uploads/2020/05/Farmer-Data-Perspectives-Research_final.pdf)
>
> Sykuta, M. E. (2016). Big Data in Agriculture: Property Rights, Privacy and Competition in Ag Data Services. *International Food and Agribusiness Management Review*, *19*(A), 18. [https://doi.org/10.22004/ag.econ.240696](https://doi.org/10.22004/ag.econ.240696)
>
> Twyman, M., Harvey, N., & Harries, C. (2008). Trust in motives, trust in competence: Separate factors determining the effectiveness of risk communication. *Judgement and Decision Making*, *3*(1), 111--120.
>
> Vasquez, O., & San-Jose, L. (2022). Ethics in Fintech through users' confidence: Determinants that affect trust. *Ramon Llull Journal of Applied Ethics*, *13*, Article 13. [https://doi.org/10.34810/rljaev1n13id398681](https://doi.org/10.34810/rljaev1n13id398681)
>
> Venkatesh, V., Morris, M. G., Davis, G. B., & Davis, F. D. (2003). User Acceptance of Information Technology: Toward a Unified View. *MIS Quarterly*, *27*(3), 425--478. [https://doi.org/10.2307/30036540](https://doi.org/10.2307/30036540)
>
> *What are Cultural Probes? --- Updated 2024*. (n.d.). The Interaction Design Foundation. Retrieved July 12, 2024, from [https://www.interaction-design.org/literature/topics/cultural-probes](https://www.interaction-design.org/literature/topics/cultural-probes)
>
> Wilgenbusch, J., Lynch, B., Hospodarsky, N., & Pardey, P. (2020). *Dealing with data privacy and security to support agricultural R&D: Technical practices and operating procedures for responsible agroinformatics data management.* \[Report\]. CGIAR Big Data Platform. [https://cgspace.cgiar.org/handle/10568/108095](https://cgspace.cgiar.org/handle/10568/108095)
>
> Wilkinson, M. D., Dumontier, M., Aalbersberg, Ij. J., Appleton, G., Axton, M., Baak, A., Blomberg, N., Boiten, J.-W., da Silva Santos, L. B., Bourne, P. E., Bouwman, J., Brookes, A. J., Clark, T., Crosas, M., Dillo, I., Dumon, O., Edmunds, S., Evelo, C. T., Finkers, R., ... Mons, B. (2016). The FAIR Guiding Principles for scientific data management and stewardship. *Scientific Data*, *3*(1), 160018. [https://doi.org/10.1038/sdata.2016.18](https://doi.org/10.1038/sdata.2016.18)
>
> Williams, R., & Kitchen, P. J. (1 C.E., January 1). *Involvement, Elaboration and the Sources of Online Trust* (involvement-elaboration-sources-online-trust) \[Chapter\]. Https://Services.Igi-Global.Com/Resolvedoi/Resolve.Aspx?Doi=10.4018/978-1-60960-575-9.Ch001; IGI Global. [https://www.igi-global.com/gateway/chapter/www.igi-global.com/gateway/chapter/54130](https://www.igi-global.com/gateway/chapter/www.igi-global.com/gateway/chapter/54130)
>
> Wiseman, L., Sanderson, J., Zhang, A., & Jakku, E. (2019). Farmers and their data: An examination of farmers' reluctance to share their data through the lens of the laws impacting smart farming. *NJAS - Wageningen Journal of Life Sciences*, *90--91*, 10. [https://doi.org/10.1016/j.njas.2019.04.007](https://doi.org/10.1016/j.njas.2019.04.007)
