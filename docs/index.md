# Introduction

## Ag Data Wallet UI/UX Collabathon, 2024

Agriculture entails a variety of knowledge work activities as people grapple with the intermingled management of data about farms, people, land, and natural resources. As such, there are many agricultural data stakeholders, from people that directly produce and use ag data such as land stewards (e.g., farmers) and their advisors (e.g., service providers, extension agents) to people that review and communicate ag data such as certifiers, marketing groups, and government entities. Each of these data stakeholders has different goals, desires, and needs with respect to data management. **There is, in theory, alignment across ag data stakeholder groups with respect to the need for interoperable, streamlined, and responsible ag data management and sharing, as well as consensus that the status quo must be improved to reduce the cognitive overhead of managing information.**

**This collabathon was prompted with a vision for an "Agricultural Data Wallet" (ADW): a digital concept that enables farmers, ranchers, and land stewards to securely store, access, and manage their data while exercising full control over who can see and use it.** The hypothesis underlying this vision is that by collaboratively designing and implementing the ADW, agricultural data management will be streamlined - reducing administrative burdens, and increasing privacy and control over data. This, in turn, will lead to improved ecological outcomes, greater data sovereignty, and increased economic opportunities for land stewards.

To pursue this vision, OpenTEAM convened a group of experienced organizations, Terran Collective, Tech Matters, and Purdue's Agricultural Informatics Lab, i.e., the "convening team", to engage in a year-long collaborative effort, i.e., a "collabathon". **Our collabathon goal was to take a more human-centered approach to the design of an ADW, focusing on identifying User Interface and User Experience (UX/UI) design considerations, contextualized in opportunities across the technology landscape, and motivated by real ag data stakeholder needs.**

Over the last year, we have have researched, interviewed, debated, and refined our thinking around (a) how to conceptualize trust in Ag Tech, (b) how different ag data stakeholders think about trust, data, and technology in an agricultural context, and (c) what design patterns might enable digitally-mediated trust in Ag Tech. We brought together diverse stakeholders, including producers, technologists, designers, researchers, and government agencies, to identify key data-sharing challenges and collaboratively design user experiences (UX) that meet the agricultural community\'s needs. The focus was on exploring system architectures and UX design patterns that are intuitive, functional, and adaptable across platforms (e.g., mobile, desktop). These patterns are intended to provide a familiar and seamless experience for users, ensuring that the ADW becomes a trusted tool for producers.

The following materials document the outcomes of the 2024 ADW UX/UI Collabathon.

Specifically, we...

-   **Developed a landscape review** that explored the state of research and practice on concepts related to trust, sovereignty, data sharing, privacy, and related topics in human-computer interaction, in the context of the agricultural community

-   **Interviewed technical subject matter experts** on the topic of agricultural data management, including coalitions focused on ag data education, policy, and interoperability

-   **Interviewed producers and Technical Assistance Providers (TAPs)** on common challenges encountered with agricultural data technologies

-   **Hosted community sessions** to share findings and get feedback along the way.

This resulted in:

-   The development of a UX Design Patterns catalog (Section 6) to provide practical recommendations to address recurring usability issues encountered by product teams

-   The development of suggested user interface elements to address the use cases that emerged from the interviews and community sessions

-   General recommendations for Ag Tech under a set of key themes: Data, Trust, UX Design, and Open Source/Open Data.

These outputs capture the insights, challenges, and recommendations that emerged during the Collabathon. This collabathon is part of a greater body of work created by the OpenTEAM community and will continue to be built upon. OpenTEAM's collaborative efforts are focused on enabling a more interoperable ag data ecosystem, built on farmer consent and trusted data transaction to create a more data-driven, farmer-benefitting, and regenerative ag sector. The outputs of this project are focused on the UI/UX components needed for trusted data storage and transaction -- this is part of our area of work focused on farmer data stewardship and consent. Other projects within our community have focused on common user roles and credentials, recommendations for [[Agricultural Data Use Agreements]{.underline}](https://openteam-agreements.community/), and the needed data formats / conventions to allow for ease of data transfer across tools in a manner that is understandable for land stewards. Each of these components contributes to an envisioned data space in which farmers can manage their own information, or grant rights and permissions to others in their network to manage their information for the purpose and value that they would like to achieve.

In future phases of work, the OpenTEAM community will build connectivity between these previous and ongoing areas of work, including outputs of this collabathon, through the creation of reference implementations, which could be tested by land stewards in our network and will serve as well-documented technical examples which could be scaled. We anticipate near-term projects focused on prototyping user-facing consent management (data use notifications and agreements) to test both the recommended UI/UX patterns discovered in this collabathon, as well as the content of our community-designed data use agreements in existing agricultural data management tools for rapid user feedback and iteration.


**Authors, ADW Convening Team**

[OpenTEAM](https://openteam.community/)

*Anna Lynton, Eliza Baker-Wacks*

OpenTEAM, or Open Technology Ecosystem for Agricultural Management, is a farmer-driven, collaborative community of food system stakeholders (farmers, ranchers, scientists, researchers, engineers, farm service providers, programmers and food companies) who are committed to improving soil health, expanding regenerative agriculture, and advancing agriculture's ability to become a solution to climate change.

[Tech Matters](https://techmatters.org/)

*Steve Francis, Hana Lee, Katy McKinney-Bock*

A non-profit open source developer which creates software solutions for communities whose needs are not being met by commercial software products. Tech Matters creates in a co-design process and seeks input from potential users around the world. They bring their methods, experiences, and technical and design expertise to this collabathon.

[Terran Collective / Hylo](https://www.terran.io/)

*Clare Brodeur, Tibet Sprague*

Terran is a non-profit collective, working to amplify cooperation among people working to regenerate our communities and our planet. We are technologists and community organizers building systems and tools for more cooperative coordination. Our main project is Hylo - the prosocial coordination platform for purpose driven groups. We are also passionate about bioregional organizing and the role the food systems play in healing our local ecological and cultural landscapes.

[Agricultural Informatics Lab @ Purdue University](https://aginformaticslab.org/)

*Ankita Raturi, Laila Dodhy, Autumn Denny*

A university-based informatics research lab working across food, agriculture, ecology, and technology in service of resilient food futures. We build a range of products, from farmer-facing decision support tools and community-oriented local food system toolkits to developer-facing APIs that serve a range of agricultural data. We conduct human-centered design research, translate our findings into usable and useful software prototypes, think critically about the role of technology in local and global food systems, and build real tools for real users.

**Disclaimer**

This report is a snapshot of our assumptions, ideas, and understanding of trust, as part of the work for the OpenTEAM Ag Data Wallet project. This report is not intended to be a comprehensive review of literature, but instead captures our current process, open questions, and needs, as well as the resources we gathered towards an effort to bridge the gap between trust theory and practice, with emphasis on agricultural technologies.

**Contact**
Please contact anna@openteam.community with feedback and questions